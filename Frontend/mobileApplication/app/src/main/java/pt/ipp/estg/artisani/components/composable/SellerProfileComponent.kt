package pt.ipp.estg.artisani.components.composable

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.LocationOn
import androidx.compose.material.icons.outlined.Lock
import androidx.compose.material.icons.outlined.Place
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import pt.ipp.estg.artisani.components.data.AppViewModel
import pt.ipp.estg.artisani.components.models.SellerInformation

@Composable
fun SellerProfile(
    token: String,
    navController: NavHostController,
    updateCurrentUser: (SellerInformation) -> Unit
) {
    val appViewModel: AppViewModel = viewModel()

    val seller = appViewModel.getSellerInformation(token).observeAsState()

    Log.d("SellerProfile", "Seller: $seller")

    seller.value?.let { sellerInformation ->
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            item {
                ProfileHeader(sellerInformation)
            }

            item {
                Spacer(modifier = Modifier.height(16.dp))
            }

            item {
                ProfileItem(icon = Icons.Default.Person, label = "Name", value = sellerInformation.name)
            }

            item {
                ProfileItem(
                    icon = Icons.Default.Phone,
                    label = "Contact",
                    value = sellerInformation.contact.toString()
                )
            }

            item {
                ProfileItem(
                    icon = Icons.Outlined.Info,
                    label = "NIF",
                    value = sellerInformation.nif.toString()
                )
            }

            item {
                ProfileItem(
                    icon = Icons.Outlined.Lock,
                    label = "NIB",
                    value = sellerInformation.nib.toString()
                )
            }

            item {
                ProfileItem(
                    icon = Icons.Outlined.Place,
                    label = "Country",
                    value = sellerInformation.country
                )
            }

            item {
                ProfileItem(
                    icon = Icons.Outlined.Home,
                    label = "Address",
                    value = sellerInformation.address
                )
            }

            item {
                ProfileItem(
                    icon = Icons.Outlined.LocationOn,
                    label = "Postal Code",
                    value = sellerInformation.postalCode
                )
            }

            item {
                ProfileItem(
                    icon = Icons.Default.Email,
                    label = "Email",
                    value = sellerInformation.email
                )
            }

            item {
                Spacer(modifier = Modifier.height(16.dp))
            }

            item {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 16.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Button(
                        onClick = {
                            updateCurrentUser(sellerInformation)
                            navController.navigate("editProfile")
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                            .weight(1f)

                    ) {
                        Text(text = "Editar")
                    }
                    Button(
                        onClick = {
                            navController.navigateUp()
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .padding(8.dp)
                    ) {
                        Text(text = "Sair da conta")
                    }
                }
            }
        }
    }
}

@Composable
fun ProfileHeader(seller: SellerInformation) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            imageVector = Icons.Default.Person,
            contentDescription = null,
            modifier = Modifier
                .size(80.dp)
                .padding(end = 8.dp)
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column {
            Text(
                text = seller.name,
                style = MaterialTheme.typography.titleLarge,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}

@Composable
fun ProfileItem(icon: ImageVector, label: String, value: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
    ) {

        Icon(imageVector = icon, contentDescription = null, modifier = Modifier.size(24.dp))
        Spacer(modifier = Modifier.width(16.dp))
        Column {
            Text(
                text = label,
                style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
            )
            Text(text = value, style = MaterialTheme.typography.bodyMedium)
        }
    }
}


