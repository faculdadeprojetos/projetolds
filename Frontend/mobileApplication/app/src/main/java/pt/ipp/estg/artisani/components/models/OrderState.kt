package pt.ipp.estg.artisani.components.models

enum class OrderState(val displayName: String) {
    CONFIRMADA("Confirmada"),
    RECUSADA("Recusada"),
    EM_PROCESSAMENTO("Em Processamento"),
    ENVIADA("Enviada"),
    PENDENTE ("Pendente")
}