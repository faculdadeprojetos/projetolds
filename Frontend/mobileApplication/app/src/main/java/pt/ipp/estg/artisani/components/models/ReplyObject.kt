package pt.ipp.estg.artisani.components.models

data class ReplyObject(
    val wasSuccessful: Boolean,
    val message: String
)

