package pt.ipp.estg.artisani.components.composable

import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import pt.ipp.estg.artisani.R
import pt.ipp.estg.artisani.components.controllers.filterToXDigits
import pt.ipp.estg.artisani.components.data.AppViewModel
import pt.ipp.estg.artisani.components.models.ReplyObject
import pt.ipp.estg.artisani.components.models.SellerInformation
import pt.ipp.estg.artisani.components.retrofit.dtos.SellerInformationDto

/**
 * This is a helper function to create an annotated string for the login screen.
 */
@Composable
fun loginAnnotatedString(labelText: String): AnnotatedString {
    return buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                color = MaterialTheme.colorScheme.primary,
                fontWeight = FontWeight.Bold,
                fontSize = 12.sp,
                fontFamily = FontFamily.SansSerif,
            )
        ) {
            append(labelText)
        }
    }
}

/**
 * Function to display the error dialog.
 *
 * @param onDismissRequest Function that is called when the dialog is dismissed.
 * @param title Title of the dialog.
 * @param message Message of the dialog.
 * @param buttonText Text of the button of the dialog.
 */
@Composable
fun ErrorDialog(
    onDismissRequest: (Boolean) -> Unit,
    title: String,
    message: String,
    notValidFields: String,
    buttonText: String,
) {
    Box(
        modifier = Modifier.fillMaxSize(),
    ) {
        Dialog(
            onDismissRequest = {
                onDismissRequest(false)
            },
            properties = DialogProperties(
                dismissOnBackPress = true,
                dismissOnClickOutside = true,
            )
        ) {
            Card(
                colors = CardDefaults.cardColors(
                    containerColor = Color.White,
                    contentColor = Color.White,
                ),
                modifier = Modifier
                    .padding(10.dp),
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 6.dp
                )
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = title,
                        fontSize = 30.sp,
                        fontFamily = FontFamily.SansSerif,
                        modifier = Modifier.padding(bottom = 20.dp),
                        color = Color.Black
                    )

                    Text(
                        text = message,
                        fontSize = 18.sp,
                        fontFamily = FontFamily.SansSerif,
                        color = Color.Black,
                        modifier = Modifier.padding(start = 15.dp, bottom = 20.dp, end = 15.dp)
                    )

                    Text(
                        text = notValidFields,
                        fontSize = 18.sp,
                        fontFamily = FontFamily.SansSerif,
                        color = Color.Black
                    )

                    TextButton(
                        onClick = {
                            onDismissRequest(true)
                        },
                        content = {
                            Text(
                                text = buttonText,
                                fontFamily = FontFamily.SansSerif,
                                fontSize = 18.sp,
                                color = Color.Black
                            )
                        },
                    )
                }
            }
        }
    }
}

@Composable
fun DropdownMenuContent(
    options: List<String>,
    selectedOption: (String) -> Unit
) {
    options.forEach { option ->
        DropdownMenuItem(
            onClick = { selectedOption(option) },
            modifier = Modifier
                .fillMaxWidth()
                .clickable { selectedOption(option) },
            text =
            {
                Text(
                    text = option,
                    color = Color.Black,
                    modifier = Modifier.padding(10.dp)
                )
            }
        )

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FormOutlinedTextField(
    value: String,
    onValueChange: (String) -> Unit,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    label: String,
    applyVisualTransformation: Boolean = false,
    isInvalid: Boolean = false,
    errorMessage: String = "",
    leadingIcon: @Composable (() -> Unit)? = null,
) {
    OutlinedTextField(
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .background(Color.White),
        value = value,
        onValueChange = { onValueChange(it) },
        label = {
            Text(
                label,
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = Color.Black,
            focusedBorderColor = if (isInvalid) Color.Red else Color.Gray,
            unfocusedBorderColor = if (isInvalid) Color.Red else Color.Gray
        ),
        keyboardOptions = keyboardOptions,
        visualTransformation = if (applyVisualTransformation) PasswordVisualTransformation() else VisualTransformation.None,
        supportingText = {
            if (isInvalid) {
                Text(
                    text = errorMessage,
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 12.sp,
                    color = Color.Red
                )
            }
        },
        leadingIcon = leadingIcon
    )
}

/**
 * This is the form to edit the user profile.
 */
@Composable
fun EditProfileForm(
    navController: NavHostController,
    seller: SellerInformation,
    changeCurrentSeller: (SellerInformation) -> Unit
) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        EditProfileFormColumn(
            navController = navController,
            seller = seller,
            changeCurrentSeller = changeCurrentSeller
        )
    }
}

/**
 * This is the column for the edit profile form.
 */
@Composable
fun EditProfileFormColumn(
    navController: NavHostController,
    seller: SellerInformation,
    changeCurrentSeller: (SellerInformation) -> Unit
) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Editar Perfil",
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )

        EditProfileFormFields(
            navController = navController,
            seller = seller,
            changeCurrentSeller = changeCurrentSeller
        )
    }
}

/**
 * This is the fields of the form to edit the user profile.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditProfileFormFields(
    navController: NavHostController,
    seller: SellerInformation,
    changeCurrentSeller: (SellerInformation) -> Unit
) {
    val scope = rememberCoroutineScope()

    val ctx = LocalContext.current

    // user variables
    var username by rememberSaveable { mutableStateOf(seller.name) }
    var contact by rememberSaveable { mutableIntStateOf(seller.contact) }
    var nif by rememberSaveable { mutableIntStateOf(seller.nif) }
    var nib by rememberSaveable { mutableIntStateOf(seller.nib) }
    var country by rememberSaveable { mutableStateOf(seller.country) }
    var address by rememberSaveable { mutableStateOf(seller.address) }
    var postalCode by rememberSaveable { mutableStateOf(seller.postalCode) }
    var password by rememberSaveable { mutableStateOf("") }
    var confirmPassword by rememberSaveable { mutableStateOf("") }
    var email by rememberSaveable { mutableStateOf(seller.email) }

    // These are going to be the variables that will hold the values of the dropdown menu.
    var expanded by rememberSaveable { mutableStateOf(false) }
    val listOfOptions by remember { mutableStateOf(listOf("Portugal", "Espanha", "França")) }

    var hasEditProfileButtonBeenClicked by remember { mutableStateOf(false) }

    var notValidFormFields by remember { mutableStateOf("") }
    var openAlertDialog by remember { mutableStateOf(false) }

    if (hasEditProfileButtonBeenClicked) {
        // Validate the user information.
        if (notValidFormFields.isEmpty()) {
            if (username.length < 5 || username.length > 15) {
                notValidFormFields += "Nome de utilizador\n"
            }

            if (Patterns.EMAIL_ADDRESS.matcher(email).matches().not()) {
                notValidFormFields += "Email\n"
            }

            if (password.length > 16) {
                notValidFormFields += "Password\n"
            }
        }

        if (notValidFormFields.isNotEmpty()) {
            if (!openAlertDialog) {
                ErrorDialog(
                    onDismissRequest = {
                        openAlertDialog = it
                        hasEditProfileButtonBeenClicked = false
                        notValidFormFields = ""
                    },
                    title = "Editar Perfil",
                    message = "Os seguintes campos não são válidos:",
                    notValidFields = notValidFormFields,
                    buttonText = "Ok",
                )
            }
        } else {
            // Verify if the user already exists before inserting it in the database.
            val appViewModel: AppViewModel = viewModel()

            val sellerInformation = SellerInformationDto(
                seller.id,
                username,
                contact,
                nif,
                nib,
                country,
                address,
                postalCode,
                email,
                password,
                seller.totalProductsSold,
                seller.totalValueEarnedFromSales
            )

            // Update the user in the database.
            if (hasEditProfileButtonBeenClicked) {
                Log.d("SellerInformation", "Entered hasEditProfileButtonBeenClicked")
                LaunchedEffect(Unit) {
                    Log.d("SellerInformation", "Entered LaunchedEffect")

                    // Swapped logic to not use .observeAsState() because it kept calling the insertPlayer() method multiple times.
                    appViewModel.updateSellerInformation(seller.id, sellerInformation).asFlow()
                        .collect { result: ReplyObject ->
                            Log.d("SellerInformation", "Entered collect")
                            Log.d("SellerInformation", result.toString())

                            if (result.wasSuccessful) {
                                scope.launch {
                                    // Display a Toast indicating success.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    val sellerInformationToUpdate = SellerInformation(
                                        sellerInformation.id,
                                        sellerInformation.name,
                                        sellerInformation.contact,
                                        sellerInformation.nif,
                                        sellerInformation.nib,
                                        sellerInformation.country,
                                        sellerInformation.address,
                                        sellerInformation.postalCode,
                                        sellerInformation.email,
                                        sellerInformation.password,
                                        sellerInformation.totalProductsSold,
                                        sellerInformation.totalValueEarnedFromSales,
                                    )

                                    // Update information of the current user for the rest of the app.
                                    changeCurrentSeller(sellerInformationToUpdate)

                                    // Change the screen to the profile screen.
                                    navController.navigate("profile")
                                }
                            } else {
                                scope.launch {
                                    // Display a Toast indicating failure.
                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                }
            }

        }
    }

    /**
     * This is the text field for the username.
     */
    FormOutlinedTextField(
        value = username,
        onValueChange = { username = it },
        label = "Nome do utilizador",
        leadingIcon = {
            Text(
                text = "@",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
    )

    Row(
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .background(Color.White),
    ) {
        /**
         * This is the text field for the contact.
         */
        OutlinedTextField(
            modifier = Modifier
                .weight(1f),
            value = contact.toString(),
            onValueChange = {
                contact = filterToXDigits(it, 9).toInt()
            },
            label = {
                Text(
                    "Contacto",
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 20.sp,
                    color = Color.Black
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black,
                focusedBorderColor = Color.Gray,
                unfocusedBorderColor = Color.Gray
            ),
        )

        /**
         * This is the text field for the NIF.
         */
        OutlinedTextField(
            modifier = Modifier
                .weight(1f),
            value = nif.toString(),
            onValueChange = { nif = filterToXDigits(it, 9).toInt() },
            label = {
                Text(
                    "NIF",
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 20.sp,
                    color = Color.Black
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black,
                focusedBorderColor = Color.Gray,
                unfocusedBorderColor = Color.Gray
            ),

            )
    }

    /**
     * This is the text field for the NIB.
     */
    FormOutlinedTextField(
        value = nib.toString(),
        onValueChange = { nib = filterToXDigits(it, 9).toInt() },
        label = "NIB",
    )

    /**
     * This is the dropdown menu for the country.
     */
    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = { expanded = it },
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .background(Color.White)
    ) {
        OutlinedTextField(
            modifier = Modifier.menuAnchor(),
            readOnly = true,
            value = country,
            onValueChange = {},
            label = {
                Text(
                    "País",
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 20.sp,
                    color = Color.Black
                )
            },
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.LocationOn,
                    contentDescription = "Location Icon",
                    tint = Color.Black
                )
            }

        )

        ExposedDropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
            DropdownMenuContent(listOfOptions, selectedOption = {
                country = it
                expanded = false
            })
        }

    }

    /**
     * This is the text field for the address.
     */
    FormOutlinedTextField(
        value = address,
        onValueChange = { address = it },
        label = "Morada",
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Home,
                contentDescription = "Location Icon",
                tint = Color.Black
            )
        }
    )

    /**
     * This is the text field for the postal code.
     */
    FormOutlinedTextField(
        value = postalCode,
        onValueChange = { postalCode = it },
        label = "Código Postal",
    )

    /**
     * This is the text field for the email.
     */
    FormOutlinedTextField(
        value = email,
        onValueChange = { email = it },
        label = "Email",
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Email,
            imeAction = ImeAction.Next
        ),
        errorMessage = "Email inválido",
        isInvalid = !Patterns.EMAIL_ADDRESS.matcher(email).matches(),
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Email,
                contentDescription = "Location Icon",
                tint = Color.Black
            )
        }
    )

    /**
     * This is the text field for the password.
     */
    FormOutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = "Palavra-passe",
        applyVisualTransformation = true,
    )

    /**
     * This is the text field for the password confirmation.
     */
    FormOutlinedTextField(
        value = confirmPassword,
        onValueChange = { confirmPassword = it },
        label = "Confirmar palavra-passe",
        applyVisualTransformation = true,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Next
        )
    )

    /**
     * This is the button to edit the user profile.
     */
    Button(
        onClick = {
            hasEditProfileButtonBeenClicked = true
            openAlertDialog = false
        },
        content = {
            Text(
                text = "Editar",
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
    )
}

@Composable
fun LoginMenu(
    navController: NavHostController,
    onLoginClick: (String) -> Unit
) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        LoginFormColumn(onLoginClick = onLoginClick, navController = navController)
    }
}

@Composable
fun LoginFormColumn(
    onLoginClick: (String) -> Unit,
    modifier: Modifier = Modifier,
    navController: NavHostController
) {
    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Login",
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )
        LoginFormFields(onLoginClick = onLoginClick, navController = navController)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginFormFields(
    navController: NavHostController,
    onLoginClick: (String) -> Unit
) {
    // Context of app
    val ctx = LocalContext.current

    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var passwordVisibility by remember { mutableStateOf(false) }

    val appViewModel: AppViewModel = viewModel()

    var wasLoginButtonClicked by remember { mutableStateOf(false) }

    if (wasLoginButtonClicked) {
        LaunchedEffect(ctx) {
            Log.d("Login", "Login button clicked")
            appViewModel.login(email, password).asFlow().collect { result ->
                Log.d("Login", "Login result: $result")
                result?.let {
                    Toast.makeText(
                        ctx,
                        "Login successful",
                        Toast.LENGTH_SHORT
                    ).show()

                    onLoginClick(it)
                }
            }
        }
    }

    OutlinedTextField(
        modifier = Modifier
            .padding(horizontal = 30.dp)
            .background(Color.White),
        value = email,
        onValueChange = { email = it },
        label = {
            Text(
                "Email",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = Color.Black,
        ),
    )

    OutlinedTextField(
        modifier = Modifier
            .padding(horizontal = 30.dp)
            .background(Color.White)
            .padding(top = 16.dp),
        value = password,
        onValueChange = { password = it },
        label = {
            Text(
                "Password",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            textColor = Color.Black,
        ),
        visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
        trailingIcon = {
            IconButton(
                onClick = { passwordVisibility = !passwordVisibility },
                modifier = Modifier.padding(8.dp)

            ) {
                if (passwordVisibility) {
                    Icon(
                        painterResource(id = R.drawable.baseline_visibility_24),
                        contentDescription = "Localized"
                    )
                } else

                    Icon(
                        painterResource(id = R.drawable.baseline_visibility_off_24),
                        contentDescription = "Localized"
                    )

            }
        },
        keyboardOptions = KeyboardOptions(
            autoCorrect = false,
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Password
        )
    )

    Button(
        onClick = {
            wasLoginButtonClicked = true
        },
        modifier = Modifier
            .padding(top = 16.dp)
            .height(48.dp)
    ) {
        Text("Login")
    }

    AppClickableText(
        text = loginAnnotatedString(labelText = "Registar"),
        navController = navController,
        verticalPadding = 20.dp,
    )
}

@Composable
fun RegisterMenu(navController: NavHostController) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        RegisterFormColumn(navController = navController)
    }
}

@Composable
fun RegisterFormColumn(navController: NavHostController) {
    // This is the scroll state for the column.
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .verticalScroll(scrollState)
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Registo",
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )
        RegisterFormFields(navController = navController)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterFormFields(navController: NavHostController) {
    val ctx = LocalContext.current

    // These are going to be the variables that will hold the values of the text fields.
    var username by rememberSaveable { mutableStateOf("") }
    var contact by rememberSaveable { mutableIntStateOf(0) }
    var nif by rememberSaveable { mutableIntStateOf(0) }
    var nib by rememberSaveable { mutableIntStateOf(0) }
    var country by rememberSaveable { mutableStateOf("Portugal") }
    var address by rememberSaveable { mutableStateOf("") }
    var postalCode by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    var confirmPassword by rememberSaveable { mutableStateOf("") }
    var email by rememberSaveable { mutableStateOf("") }

    // These are going to be the variables that will hold the values of the dropdown menu.
    var expanded by rememberSaveable { mutableStateOf(false) }
    val listOfOptions by remember { mutableStateOf(listOf("Portugal", "Espanha", "França")) }

    val appViewModel: AppViewModel = viewModel()
    var wasRegisterButtonClicked by remember { mutableStateOf(false) }

    if (wasRegisterButtonClicked) {
        LaunchedEffect(ctx) {
            val seller = appViewModel.register(
                SellerInformationDto(
                    id = 0,
                    name = username,
                    contact = contact,
                    nif = nif,
                    nib = nib,
                    country = country,
                    address = address,
                    postalCode = postalCode,
                    password = password,
                    email = email,
                    totalProductsSold = 0,
                    totalValueEarnedFromSales = 0f
                )
            )

            seller.let {
                Toast.makeText(
                    ctx,
                    "Registado com sucesso!",
                    Toast.LENGTH_SHORT
                ).show()

                navController.navigate("login")
            }
        }
    }

    /**
     * This is the text field for the username.
     */
    FormOutlinedTextField(
        value = username,
        onValueChange = { username = it },
        label = "Nome do utilizador",
        leadingIcon = {
            Text(
                text = "@",
                fontFamily = FontFamily.SansSerif,
                fontSize = 20.sp,
                color = Color.Black
            )
        },
    )

    Row(
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .background(Color.White),
    ) {
        /**
         * This is the text field for the contact.
         */
        OutlinedTextField(
            modifier = Modifier
                .weight(1f),
            value = contact.toString(),
            onValueChange = {
                contact = filterToXDigits(it, 9).toInt()
            },
            label = {
                Text(
                    "Contacto",
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 20.sp,
                    color = Color.Black
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black,
                focusedBorderColor = Color.Gray,
                unfocusedBorderColor = Color.Gray
            ),
        )

        /**
         * This is the text field for the NIF.
         */
        OutlinedTextField(
            modifier = Modifier
                .weight(1f),
            value = nif.toString(),
            onValueChange = { nif = filterToXDigits(it, 9).toInt() },
            label = {
                Text(
                    "NIF",
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 20.sp,
                    color = Color.Black
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black,
                focusedBorderColor = Color.Gray,
                unfocusedBorderColor = Color.Gray
            ),

            )
    }

    /**
     * This is the text field for the NIB.
     */
    FormOutlinedTextField(
        value = nib.toString(),
        onValueChange = { nib = filterToXDigits(it, 9).toInt() },
        label = "NIB",
    )

    /**
     * This is the dropdown menu for the country.
     */
    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = { expanded = it },
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .background(Color.White)
    ) {
        OutlinedTextField(
            modifier = Modifier.menuAnchor(),
            readOnly = true,
            value = country,
            onValueChange = {},
            label = {
                Text(
                    "País",
                    fontFamily = FontFamily.SansSerif,
                    fontSize = 20.sp,
                    color = Color.Black
                )
            },
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.LocationOn,
                    contentDescription = "Location Icon",
                    tint = Color.Black
                )
            }

        )

        ExposedDropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
            DropdownMenuContent(listOfOptions, selectedOption = {
                country = it
                expanded = false
            })
        }

    }

    /**
     * This is the text field for the address.
     */
    FormOutlinedTextField(
        value = address,
        onValueChange = { address = it },
        label = "Morada",
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Home,
                contentDescription = "Location Icon",
                tint = Color.Black
            )
        }
    )

    /**
     * This is the text field for the postal code.
     */
    FormOutlinedTextField(
        value = postalCode,
        onValueChange = { postalCode = it },
        label = "Código Postal",
    )

    /**
     * This is the text field for the email.
     */
    FormOutlinedTextField(
        value = email,
        onValueChange = { email = it },
        label = "Email",
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Email,
            imeAction = ImeAction.Next
        ),
        errorMessage = "Email inválido",
        isInvalid = !Patterns.EMAIL_ADDRESS.matcher(email).matches(),
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Email,
                contentDescription = "Location Icon",
                tint = Color.Black
            )
        }
    )

    /**
     * This is the text field for the password.
     */
    FormOutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = "Palavra-passe",
        applyVisualTransformation = true,
    )

    /**
     * This is the text field for the password confirmation.
     */
    FormOutlinedTextField(
        value = confirmPassword,
        onValueChange = { confirmPassword = it },
        label = "Confirmar palavra-passe",
        applyVisualTransformation = true,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Next
        )
    )

    /**
     * This is the button for the register.
     */
    Button(
        onClick = {
            wasRegisterButtonClicked = true
        },
        modifier = Modifier
            .padding(top = 10.dp, start = 30.dp, end = 30.dp, bottom = 20.dp)
            .fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Black,
            contentColor = Color.White
        )
    ) {
        Text(
            text = "Registar",
            fontFamily = FontFamily.SansSerif,
            fontSize = 20.sp,
            color = Color.White
        )
    }
}


