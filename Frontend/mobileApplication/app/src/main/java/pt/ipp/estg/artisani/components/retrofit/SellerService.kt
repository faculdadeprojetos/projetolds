package pt.ipp.estg.artisani.components.retrofit

import pt.ipp.estg.artisani.components.retrofit.dtos.SellerLogin
import pt.ipp.estg.artisani.components.retrofit.dtos.SellerInformationDto
import pt.ipp.estg.artisani.components.retrofit.dtos.TokenDto
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface SellerService {

    @Headers("Content-Type: application/json")
    @GET("getSeller")
    fun getSeller(@Header("Authorization") token: String): Call<SellerInformationDto>

    @Headers("Content-Type: application/json")
    @POST("login")
    //suspend fun login(@Body seller: String): Response<String>
    suspend fun login(@Body seller: SellerLogin): Response<TokenDto>

    @Headers("Content-Type: application/json")
    @POST("register")
    suspend fun register(seller: SellerInformationDto): Response<SellerInformationDto>

    @Headers("Content-Type: application/json")
    @PUT("{id}")
    fun updateSellerInformation(@Path("id") id: Int, @Body seller: SellerInformationDto): Call<Void>
}