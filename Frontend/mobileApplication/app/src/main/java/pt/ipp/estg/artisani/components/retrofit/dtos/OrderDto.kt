package pt.ipp.estg.artisani.components.retrofit.dtos

import pt.ipp.estg.artisani.components.retrofit.dtos.CartProductDto

class OrderDto (
    val id: Int,
    val products: List<CartProductDto>,
    val totalProductQuantity : Int,
    val totalValue : Float,
    val totalValueWithDiscount: Float,
    val date: String,
    val clientId: Int,
    val state: String
)