package pt.ipp.estg.artisani.components.composable

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import pt.ipp.estg.artisani.components.data.AppViewModel
import pt.ipp.estg.artisani.components.models.OrderInformation
import pt.ipp.estg.artisani.components.models.OrderState
import pt.ipp.estg.artisani.components.retrofit.dtos.CartProductDto

@Composable
fun OrderList(orderList: List<OrderInformation> = listOf(), navController: NavController,) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "Lista de Encomendas",
            style = MaterialTheme.typography.headlineLarge.copy(color = Color.DarkGray),
            modifier = Modifier.padding(bottom = 16.dp)
        )

        // Table Header
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Text(
                text = "ID",
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(0.6f)
            )
            Text(
                text = "Produto",
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = "Valor",
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(0.7f)
            )
            Text(
                text = "Data",
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.weight(1.7f)
            )

        }

        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp), color = Color.Gray
        )

        if (orderList.isEmpty()) {
            Text(
                text = "Sem encomendas disponíveis",
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(top = 16.dp)
            )
        } else {
            OrderListTable(orderList, navController = navController)
        }
    }
}

@Composable
fun OrderListTable(
    orderList: List<OrderInformation> = listOf(),  navController: NavController,
) {
    LazyColumn {
        items(orderList) { order ->
            // Send the order information to the composable
            OrderItemRow(order,navController = navController )
            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp), color = Color.Gray
            )
        }
    }
}

@Composable
fun OrderItemRow(order: OrderInformation,  navController: NavController,) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(text = "${order.id}", style = MaterialTheme.typography.bodySmall, modifier = Modifier.weight(0.5f))
        Text(text = order.products.joinToString(", ") { it.product.name }, style = MaterialTheme.typography.bodySmall, modifier = Modifier.weight(1f))
        Text(text = "$${order.totalValue}", style = MaterialTheme.typography.bodySmall, modifier = Modifier.weight(0.5f))
        Text(text = order.date, style = MaterialTheme.typography.bodySmall, modifier = Modifier.weight(1f))

        Text(
            text = "Ver Detalhes",
            style = MaterialTheme.typography.bodySmall.copy(color = Color.Blue),
            modifier = Modifier.clickable {
                navController.navigate("orders/${order.id}")
            }
        )
    }
}

@Composable
fun getAllOrders(token: String) : List<OrderInformation> {
    val orderList = mutableListOf<OrderInformation>()
    val appViewModel: AppViewModel = viewModel()
    val listOfOrders = appViewModel.getOrdersFromSeller(token).observeAsState()

    listOfOrders.value?.let { notNullOrders: List<OrderInformation?> ->
        notNullOrders.forEach { order: OrderInformation? ->
            order?.let {
                val newOrder = OrderInformation(
                    id = it.id,
                    products = it.products,
                    quantity = it.quantity,
                    totalValue = it.totalValue,
                    totalValueWithDiscount = it.totalValueWithDiscount,
                    date = it.date,
                    clientId = it.clientId,
                    state = it.state
                )

                orderList.add(newOrder)
            }
        }
    } ?: run {
        Log.d("Order Management", "No orders found")
    }

    return orderList
}



/**
 * Retorna uma encomenda específica
 */
@Composable
fun getOrderForId(token: String, id: Int): LiveData<OrderInformation?> {
    val appViewModel: AppViewModel = viewModel()
    val order = appViewModel.getSpecificOrderFromSeller(token = token, id = id).observeAsState()

    order.value?.let { possiblyNullOrder: OrderInformation? ->
        possiblyNullOrder?.let {
            val newOrder = OrderInformation(
                id = it.id,
                products = it.products,
                quantity = it.quantity,
                totalValue = it.totalValue,
                totalValueWithDiscount = it.totalValueWithDiscount,
                date = it.date,
                clientId = it.clientId,
                state = it.state
            )

            return MutableLiveData(newOrder)
        } ?: run {
            Log.d("Order Management", "No orders found")
        }
    } ?: run {
        Log.d("Order Management", "No orders found")
    }

    return MutableLiveData(null)
}

/**
 * Mostra os detalhes da encomenda
 */
@Composable
fun OrderDetailsScreen(token: String, order: OrderInformation, navController: NavController) {
    var showDialog by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "Detalhes da Encomenda",
            style = MaterialTheme.typography.headlineLarge.copy(color = Color.DarkGray),
            modifier = Modifier.padding(bottom = 16.dp)
        )

        // Mostrar detalhes da encomenda
        Text(text = "ID da Encomenda: ${order.id}", style = MaterialTheme.typography.bodyLarge)
        Text(text = "Data do pedido: ${order.date}", style = MaterialTheme.typography.bodyLarge)
        Text(text = "Quantidade: ${order.quantity}", style = MaterialTheme.typography.bodyLarge)
        Text(text = "Valor Total: ${order.totalValue}", style = MaterialTheme.typography.bodyLarge)
        Text(
            text = "Estado da Encomenda: ${order.state}",
            style = MaterialTheme.typography.bodyLarge
        )
        // Adicione mais informações da encomenda conforme necessário

        // Lista de produtos da encomenda
        Text(
            text = "Produtos na Encomenda:",
            style = MaterialTheme.typography.headlineMedium.copy(color = Color.DarkGray),
            modifier = Modifier.padding(top = 16.dp, bottom = 8.dp)
        )

        OrderProductsList(order.products)

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            TextButton(
                onClick = {
                    navController.navigate("orders")
                }
            ) {
                Text(text = "Voltar à Lista de Encomendas")
            }

            // Botão para mudar o estado da encomenda
            TextButton(
                onClick = {
                    showDialog = true
                }
            ) {
                Text(text = "Selecionar Estado")
            }

            if (showDialog) {
                Box(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    Dialog(
                        onDismissRequest = {
                            showDialog = false
                        },
                        properties = DialogProperties(
                            dismissOnBackPress = true,
                            dismissOnClickOutside = true,
                        ),

                        ) {
                        Card(
                            colors = CardDefaults.cardColors(
                                containerColor = Color.White,
                                contentColor = Color.White,
                            ),
                            modifier = Modifier
                                .padding(10.dp),
                            elevation = CardDefaults.cardElevation(
                                defaultElevation = 6.dp
                            )
                        ) {
                            ChooseStateDialog(
                                token = token,
                                order = order,
                            )
                        }
                    }
                }
            }
        }
    }
}

/**
 * Mostra o diálogo para selecionar o estado da encomenda
 */
@Composable
fun ChooseStateDialog(token: String, order: OrderInformation) {
    var currentState by rememberSaveable { mutableStateOf(order.state) }

    var hasUpdateStateButtonBeenClicked by remember { mutableStateOf(false) }

    val orderStatesString by remember {
        mutableStateOf(
            listOf(
                "Pendente",
                "Em Processamento",
                "Confirmada",
                "Enviada",
                "Recusada",
            ).filter { it != order.state }
        )
    }

    val ctx = LocalContext.current

    Log.d("Order Management25", "Current state: $currentState")
    if (hasUpdateStateButtonBeenClicked) {
        Log.d("Order Management25", "Updating order state")
        // Atualiza o estado da encomenda
        val appViewModel: AppViewModel = viewModel()

        LaunchedEffect(ctx) {
            Log.d("Order Management", "Updating order state")
            appViewModel.updateOrderState(token = token, id = order.id, state = currentState)
                .asFlow().collect {
                    Log.d("Order Management", "${it.wasSuccessful}")
                    Log.d("Order Management", "Order state updated")
                    if (it.wasSuccessful) {
                        Toast.makeText(
                            ctx,
                            "Estado da encomenda atualizado com sucesso",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        Text(
            text = "Selecione o estado da encomenda",
            style = MaterialTheme.typography.headlineMedium.copy(color = Color.DarkGray),
            modifier = Modifier.padding(bottom = 16.dp)
        )

        orderStatesString.forEach { state ->
            TextButton(
                onClick = {
                    currentState = state
                }
            ) {
                Text(text = state)
            }
        }

        Button(
            onClick = {
                hasUpdateStateButtonBeenClicked = true
            }
        ) {
            Text(text = "Atualizar Estado")
        }
    }
}

/**
 * Mostra a lista de produtos da encomenda
 */
@Composable
fun OrderProductsList(products: List<CartProductDto>) {
    LazyColumn {
        items(products) { product ->
            // Mostrar informações detalhadas do produto
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Text(
                    text = "ID: ${product.id}",
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Produto: ${product.product.name}",
                    style = MaterialTheme.typography.bodyMedium,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Categoria: ${product.product.category}",
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Stock: ${product.product.stock}",
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Valor Individual: $${product.product.individualPrice}",
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Quantidade: $${product.quantityBought}",
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
            }

            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp),
                color = Color.Gray
            )
        }
    }
}