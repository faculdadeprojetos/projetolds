package pt.ipp.estg.artisani.components.models

class CartProduct (
    val id: Int,
    val quantityBought: Int,
    val product: Product,
    val totalValue: Float,
)