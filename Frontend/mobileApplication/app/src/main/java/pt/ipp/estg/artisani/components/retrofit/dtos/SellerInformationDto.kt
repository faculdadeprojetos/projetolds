package pt.ipp.estg.artisani.components.retrofit.dtos

class SellerInformationDto (
    val id: Int,
    val name: String,
    val contact: Int,
    val nif: Int,
    val nib: Int,
    val country: String,
    val address: String,
    val postalCode: String,
    val email: String,
    val password: String,
    val totalProductsSold: Int,
    val totalValueEarnedFromSales: Float,
) {
    override fun toString(): String {
        return "{id: $id, name: $name, contact: $contact, nif: $nif, nib: $nib, country: $country, address: $address, postalCode: $postalCode, email: $email, password: $password, totalProductsSold: $totalProductsSold, totalValueEarnedFromSales: $totalValueEarnedFromSales}"
    }
}
