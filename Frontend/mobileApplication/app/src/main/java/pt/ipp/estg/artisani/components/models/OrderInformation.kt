package pt.ipp.estg.artisani.components.models

import pt.ipp.estg.artisani.components.retrofit.dtos.CartProductDto

data class OrderInformation(
    val id: Int,
    val products: List<CartProductDto>,
    val quantity : Int,
    val totalValue : Float,
    val totalValueWithDiscount: Float,
    val date: String,
    val clientId: Int,
    var state: String
)
