package pt.ipp.estg.artisani.components.composable
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import pt.ipp.estg.artisani.components.classes.Product


@Composable
fun AllProducts() {
    val products = remember {
        listOf(
            Product(
                id = "1",
                stock = 10,
                name = "Produto 1",
                category = "Categoria 1",
                individualValue = 19.99f,
                height = 5.0f,
                width = 3.0f,
                weight = 2.0f,
                typeOfMaterialMade = "Material 1",
                washingInstructions = "Lave com cuidado",
                sellerId = "",
                imagePath = "https://example.com/image1.jpg"
            ),
            Product(
                id = "2",
                stock = 5,
                name = "Produto 2",
                category = "Categoria 2",
                individualValue = 29.99f,
                height = 8.0f,
                width = 4.0f,
                weight = 3.0f,
                typeOfMaterialMade = "Material 2",
                washingInstructions = "190º na maquina",
                sellerId = "",
                imagePath = "https://example.com/image2.jpg"
            ),
            Product(
                id = "3",
                stock = 8,
                name = "Produto 3",
                category = "Categoria 3",
                individualValue = 39.99f,
                height = 6.0f,
                width = 5.0f,
                weight = 4.0f,
                typeOfMaterialMade = "Material 3",
                washingInstructions = "Lave com cuidado",
                sellerId = "",
                imagePath = "https://example.com/image3.jpg"
            )
        )
    }

    var selectedProduct by remember { mutableStateOf<Product?>(null) }



    if (selectedProduct != null) {

        ProductDetails(
            product = selectedProduct!!,
            onEditProduct = {

                //Funçao que da save no produto
            },
            onClose = { selectedProduct = null }
        )
    } else {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            Text(
                text = "Lista dos seus produtos:",
                color = Color.Blue,
                style = MaterialTheme.typography.headlineSmall,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(vertical = 8.dp, horizontal = 50.dp)
            )

            products.forEach { product ->
                ProductItem(product) { clickedProduct ->
                    selectedProduct = clickedProduct
                }
                Divider(modifier = Modifier.padding(horizontal = 16.dp))
            }
        }
    }
    }


@Composable
fun ProductItem(product: Product, onProductClick: (Product) -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { onProductClick(product) },
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = "${product.name}",
            fontWeight = FontWeight.Bold,
            color = Color.White
        )

        Text(text = "Quantidade em Stock: ${product.stock}", color = Color.White)

        Text(
            text = "Preço de Venda: ${product.individualValue}",
            color = Color.White
        )
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductDetails(product: Product, onEditProduct: () -> Unit, onClose: () -> Unit) {
    var isEditing by remember { mutableStateOf(false) }
    var editedProduct by remember { mutableStateOf(product.copy()) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(16.dp)
    ) {
        Text(text = "Detalhes do Produto", fontWeight = FontWeight.Bold, fontSize = 20.sp, color = Color.Green)

        Spacer(modifier = Modifier.height(8.dp))

        if (isEditing) {
            // Editing mode
            OutlinedTextField(
                value = editedProduct.name,
                onValueChange = { editedProduct = editedProduct.copy(name = it) },
                label = { Text("Nome do Produto", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.category,
                onValueChange = { editedProduct = editedProduct.copy(category = it) },
                label = { Text("Categoria", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.stock.toString(),
                onValueChange = { editedProduct = editedProduct.copy(stock = it.toIntOrNull() ?: 0) },
                label = { Text("Quantidade em Stock", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.individualValue.toString(),
                onValueChange = { editedProduct = editedProduct.copy(individualValue = it.toFloat()) },
                label = { Text("Preço de Venda", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.typeOfMaterialMade,
                onValueChange = { editedProduct = editedProduct.copy(typeOfMaterialMade = it) },
                label = { Text("Tipo de material", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.washingInstructions,
                onValueChange = { editedProduct = editedProduct.copy(typeOfMaterialMade = it) },
                label = { Text("Instruções de lavagem", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.weight.toString(),
                onValueChange = { editedProduct = editedProduct.copy(weight = it.toFloat())},
                label = { Text("Peso do produto", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.height.toString(),
                onValueChange = { editedProduct = editedProduct.copy(height = it.toFloat())},
                label = { Text("Altura do produto", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
            OutlinedTextField(
                value = editedProduct.width.toString(),
                onValueChange = { editedProduct = editedProduct.copy(width = it.toFloat())},
                label = { Text("Largura do produto", color = Color.White) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )

        } else {

            Text(text = "Nome: ${product.name}", color = Color.White)
            Text(text = "Categoria: ${product.category}", color = Color.White)
            Text(text = "Quantidade em Stock: ${product.stock}", color = Color.White)
            Text(text = "Preço de Venda: ${product.individualValue}", color = Color.White)
            Text(text = "Tipo de material: ${product.typeOfMaterialMade}", color = Color.White)
            Text(text = "Instruções de lavagem: ${product.washingInstructions}", color = Color.White)
            Text(text = "Peso do produto: ${product.weight}", color = Color.White)
            Text(text = "Altura do Produto: ${product.height}", color = Color.White)
            Text(text = "Largura do Droduto: ${product.width}", color = Color.White)
        }

        Spacer(modifier = Modifier.height(16.dp))


        Button(
            onClick = {
                if (isEditing) {

                    onEditProduct()
                }
                isEditing = !isEditing
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            if (isEditing) {
                Text("Guardar Produto")
            } else {
                Text("Editar Produto")
            }
        }

        Spacer(modifier = Modifier.height(16.dp))


        Button(
            onClick = onClose,
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Text("Fechar")
        }
    }
}
@Preview(showBackground = true)
@Composable
fun AllProductsPreview() {
    AllProducts()
}
