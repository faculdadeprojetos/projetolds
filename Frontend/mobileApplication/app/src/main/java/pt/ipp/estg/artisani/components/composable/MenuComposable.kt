package pt.ipp.estg.artisani.components.composable


import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import pt.ipp.estg.artisani.R


/**
 * Component that represents the menu of the application.
 *
 * It is the first screen that the user sees when he opens the application.
 *
 * It contains three texts that are clickable - Game, Leaderboard and Account.
 * These texts will take the user to the respective components
 * whilst changing the selected item in the navigation drawer.
 *
 * @param navController Navigation controller that allows the user to navigate between screens.
 * @param changeSelectedItem Function that changes the selected item in the navigation drawer.
 */
@Composable
fun MenuComponent(navController: NavHostController, changeSelectedItem : (String) -> Unit) {
    val verticalPadding = 60.dp

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(32.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val productsAnnotatedString = menuAnnotatedString(labelText = stringResource(R.string.products))
        val ordersAnnotatedString = menuAnnotatedString(labelText = stringResource(R.string.orders))
        val profileAnnotatedString = menuAnnotatedString(labelText = stringResource(R.string.profile))

        AppClickableText(
            text = productsAnnotatedString,
            navController = navController,
            verticalPadding = verticalPadding,
            changeSelectedItem = changeSelectedItem
        )

        AppClickableText(
            text = ordersAnnotatedString,
            navController = navController,
            verticalPadding = verticalPadding,
            changeSelectedItem = changeSelectedItem
        )

        AppClickableText(
            text = profileAnnotatedString,
            navController = navController,
            verticalPadding = verticalPadding,
            changeSelectedItem = changeSelectedItem
        )
    }
}

@Composable
fun menuAnnotatedString(labelText: String): AnnotatedString {
    return buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                color = MaterialTheme.colorScheme.primary,
                fontWeight = FontWeight.Bold,
                fontSize = 32.sp,
                fontFamily = FontFamily.SansSerif,
            )
        ) {
            append(labelText)
        }
    }
}



