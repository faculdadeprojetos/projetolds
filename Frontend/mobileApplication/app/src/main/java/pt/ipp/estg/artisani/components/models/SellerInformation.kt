package pt.ipp.estg.artisani.components.models

data class SellerInformation(
    val id: Int,
    val name: String,
    val contact: Int,
    val nif: Int,
    val nib: Int,
    val country: String,
    val address: String,
    val postalCode: String,
    val email: String,
    val password: String,
    val totalProductsSold: Int,
    val totalValueEarnedFromSales: Float,
)
