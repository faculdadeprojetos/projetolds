package pt.ipp.estg.artisani.components.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Product(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val stock: Int,
    val name: String,
    val category: String,
    val individualValue: Float,
    val height: Int,
    val width: Int,
    val weight: Float,
    val typeOfMaterialMade: String,
    val washingInstructions: String,
    val sellerId: Int,
    val imagePath: String?
) {

}
