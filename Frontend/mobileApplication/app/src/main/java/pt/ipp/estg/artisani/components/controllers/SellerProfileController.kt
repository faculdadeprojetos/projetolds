package pt.ipp.estg.artisani.components.controllers

import android.content.Context
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import pt.ipp.estg.artisani.components.data.AppViewModel
import pt.ipp.estg.artisani.components.models.SellerInformation

@Composable
fun getSellerInformation(
    token: String,
    context: Context
) : SellerInformation? {
    val appViewModel: AppViewModel = viewModel()

    var sellerInformation by remember { mutableStateOf<SellerInformation?>(null) }

    LaunchedEffect(context) {
        appViewModel.getSellerInformation(token).asFlow()
            .collect { possiblyNullSeller: SellerInformation? ->
                Log.d("SellerProfile possibly null", "SellerInformation: $possiblyNullSeller")
                sellerInformation?.let {
                    Log.d("SellerProfile", "SellerInformation: $it")
                    sellerInformation = it
                    return@collect
                }
            }
        return@LaunchedEffect
    }

    return sellerInformation
}