package pt.ipp.estg.artisani.components.retrofit.dtos

class ProductDto(
    val id: Int,
    val stock: Int,
    val name: String,
    val category: String,
    val individualPrice: Float,
    val height: Int,
    val width: Int,
    val weight: Float,
    val typeOfMaterialMade: String,
    val washingInstructions: String,
    val sellerId: Int,
    val packageId: Int?,
    val sellerPackageId: Int?,
    val imagePath: String?,
) {
constructor() : this(0,0,"","",0f, 0 ,0, 0f,  "", "", 0,0,0,"")

    override fun toString(): String {
        return "{\"id\":\"$id\",\"stock\":\"$stock\",\"name\":\"$name\",\"category\":\"$category\",\"individualPrice\":\"$individualPrice\"," +
                "\"height\":\"$height\",\"width\":\"$width\",\"weight\":\"$weight\", \"typeOfMaterialMade\":\"$typeOfMaterialMade\",\"washingInstructions\":\"$washingInstructions\"," +
                "\"sellerId\":\"$sellerId\",\"packageId\":\"$packageId\",\"sellerPackageId\":\"$sellerPackageId\",\"imagePath\":\"$imagePath\"}"


    }
}


