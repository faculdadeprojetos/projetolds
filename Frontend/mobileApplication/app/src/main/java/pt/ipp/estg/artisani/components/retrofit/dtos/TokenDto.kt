package pt.ipp.estg.artisani.components.retrofit.dtos

class TokenDto (
    val token: String,
)