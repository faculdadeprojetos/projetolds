package pt.ipp.estg.artisani.components.controllers

import java.math.BigInteger

fun filterToXDigits(value: String, x: Int): String {
    // Filter the value to only digits.
    val filteredValue = value.filter {
        it.isDigit()
    }

    // Create a string with x 9s.
    val numberOf9s = "9".repeat(x)

    // Convert the string to an integer.
    val integerNumberOf9s = BigInteger(numberOf9s)

    // Return the filtered value if it is empty
    // or if it is between 1 and the number of 9s.
    return if (filteredValue.isEmpty()) {
        filteredValue
    } else {
        // If the value is between 1 and the number of 9s,
        // return the value -
        // Example: x is 3 then the max is 999, so values between 1 and 999 are valid.
        // If the value is not between 1 and the number of 9s,
        // return the value without the last character.
        // Example: x is 3 then the max is 999, so values between 1000 and 9999 will simply return the first x digits.
        val filteredBigIntegerValue = BigInteger(filteredValue)
        val isValid =
            filteredBigIntegerValue >= BigInteger.ONE && filteredBigIntegerValue <= integerNumberOf9s

        if (isValid) {
            filteredValue
        } else {
            filteredValue.dropLast(1)
        }
    }
}
