package pt.ipp.estg.artisani.components.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson


import pt.ipp.estg.artisani.components.models.CartProduct
import pt.ipp.estg.artisani.components.models.OrderInformation
import pt.ipp.estg.artisani.components.models.OrderState
import pt.ipp.estg.artisani.components.models.Product
import pt.ipp.estg.artisani.components.models.ReplyObject
import pt.ipp.estg.artisani.components.models.SellerInformation
import pt.ipp.estg.artisani.components.retrofit.OrderService
import pt.ipp.estg.artisani.components.retrofit.dtos.ProductDto
import pt.ipp.estg.artisani.components.retrofit.ProductService
import pt.ipp.estg.artisani.components.retrofit.dtos.SellerInformationDto
import pt.ipp.estg.artisani.components.retrofit.SellerService
import pt.ipp.estg.artisani.components.retrofit.dtos.OrderDto
import pt.ipp.estg.artisani.components.retrofit.dtos.SellerLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

class AppRepository(
    private val orderApi: OrderService,
    private val productApi: ProductService,
    private val sellerApi: SellerService,
) {
    /**
     * Get orders from a seller, who is identified by the token returned from the login
     */
    /**
     * Get orders from a seller, who is identified by the token returned from the login
     */
    fun getOrdersFromSeller(token: String): LiveData<List<OrderInformation?>> {
        val call: Call<List<OrderDto>?> =
            orderApi.getSellerPackagesFromLoggedInSeller("Bearer $token")
        val data = MutableLiveData<List<OrderInformation?>>()

        call.enqueue(object : Callback<List<OrderDto>?> {
            override fun onResponse(
                call: Call<List<OrderDto>?>,
                response: Response<List<OrderDto>?>
            ) {
                if (response.isSuccessful) {
                    val content = response.body()

                    if (content != null) {
                        val newOrders = mutableListOf<OrderInformation?>()
                        for (order in content) {
                            Log.d("Order", Gson().toJson(order))

                            val newOrder = OrderInformation(
                                id = order.id,
                                quantity = order.totalProductQuantity,
                                products = order.products,
                                totalValue = order.totalValue,
                                totalValueWithDiscount = order.totalValueWithDiscount,
                                date = order.date,
                                clientId = order.clientId,
                                state = order.state,
                            )
                            newOrders.add(newOrder)
                        }

                        data.value = newOrders
                    }
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<List<OrderDto>?>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return data
    }


    /**
     * Get a specific order from a seller, who is identified by the token returned from the login
     *
     * The order is identified by the id parameter
     */
    fun getSpecificOrderFromSeller(token: String, id: Int): LiveData<OrderInformation> {
        val call: Call<OrderDto> =
            orderApi.getSellerPackageFromLoggedInSeller("Bearer $token", id)
        val data = MutableLiveData<OrderInformation>()

        call.enqueue(object : Callback<OrderDto> {
            override fun onResponse(
                call: Call<OrderDto>,
                response: Response<OrderDto>
            ) {
                if (response.isSuccessful) {
                    val content = response.body()

                    if (content != null) {
                        val newOrder = OrderInformation(
                            id = content.id,
                            quantity = content.totalProductQuantity,
                            products = content.products,
                            totalValue = content.totalValue,
                            totalValueWithDiscount = content.totalValueWithDiscount,
                            date = content.date,
                            clientId = content.clientId,
                            state = content.state,
                        )

                        data.value = newOrder
                    }
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<OrderDto>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return data
    }

    /**
     * Get products from a seller, who is identified by the token returned from the login
     */
    fun getProductsFromSeller(token: String): LiveData<List<Product?>> {
        val call: Call<List<ProductDto>> = productApi.getProductsFromSeller("Bearer $token")
        val data = MutableLiveData<List<Product?>>()

        call.enqueue(object : Callback<List<ProductDto>> {
            override fun onResponse(
                call: Call<List<ProductDto>>,
                response: Response<List<ProductDto>>
            ) {
                if (response.isSuccessful) {
                    val content = response.body()

                    if (content != null) {
                        val newProducts = mutableListOf<Product?>()
                        for (product in content) {
                            Log.d("Product", Gson().toJson(product))

                            val newProduct = Product(
                                id = product.id,
                                stock = product.stock,
                                name = product.name,
                                category = product.category,
                                height = product.height,
                                width = product.width,
                                weight = product.weight,
                                typeOfMaterialMade = product.typeOfMaterialMade,
                                washingInstructions = product.washingInstructions,
                                individualValue = product.individualPrice,
                                sellerId = product.sellerId,
                                imagePath = product.imagePath,
                            )
                            newProducts.add(newProduct)
                        }

                        data.value = newProducts
                    }
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<List<ProductDto>>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return data
    }
    fun getProductById(token: String, productId: Int): LiveData<Product?> {
        val call: Call<ProductDto> = productApi.getProductById("Bearer $token", productId)
        val data = MutableLiveData<Product?>()

        call.enqueue(object : Callback<ProductDto> {
            override fun onResponse(
                call: Call<ProductDto>,
                response: Response<ProductDto>
            ) {
                if (response.isSuccessful) {
                    val productDto = response.body()

                    if (productDto != null) {
                        val product = Product(
                            id = productDto.id,
                            stock = productDto.stock,
                            name = productDto.name,
                            category = productDto.category,
                            height = productDto.height,
                            width = productDto.width,
                            weight = productDto.weight,
                            typeOfMaterialMade = productDto.typeOfMaterialMade,
                            washingInstructions = productDto.washingInstructions,
                            individualValue = productDto.individualPrice,
                            sellerId = productDto.sellerId,
                            imagePath = productDto.imagePath
                        )

                        data.value = product
                    }
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<ProductDto>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return data
    }



    /**
     * Add a product from a seller, who is identified by the token returned from the login
     */
    fun addProductFromSeller(
        product: ProductDto, token: String
    ): LiveData<Product?> {
        val call: Call<ProductDto> = productApi.createProduct(product, "Bearer $token")
        val data = MutableLiveData<Product?>()
        call.enqueue(object : Callback<ProductDto> {
            override fun onResponse(call: Call<ProductDto>, response: Response<ProductDto>) {
                if (response.isSuccessful) {
                    val content = response.body()
                    Log.d("product", Gson().toJson(content))
                    content?.let {
                        val newProduct = Product(
                            id = it.id,
                            stock = it.stock,
                            name = it.name,
                            category = it.category,
                            height = it.height,
                            width = it.width,
                            weight = it.weight,
                            typeOfMaterialMade = it.typeOfMaterialMade,
                            washingInstructions = it.washingInstructions,
                            individualValue = it.individualPrice,
                            sellerId = it.sellerId,
                            imagePath = it.imagePath,
                        )

                        data.value = newProduct
                    }
                }


            }

            override fun onFailure(call: Call<ProductDto>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t}")
            }
        })
        return data
    }

    /**
     * Get information from a seller, who is identified by the token returned from the login
     */
    fun getSellerInformation(token: String): LiveData<SellerInformation?> {
        Log.d("Token", token)

        val call: Call<SellerInformationDto> = sellerApi.getSeller("Bearer $token")

        val data = MutableLiveData<SellerInformation?>()

        call.enqueue(object : Callback<SellerInformationDto> {
            override fun onResponse(
                call: Call<SellerInformationDto>,
                response: Response<SellerInformationDto>
            ) {
                if (response.isSuccessful) {
                    val content = response.body()
                    content?.let {
                        val newSeller = SellerInformation(
                            id = it.id,
                            name = it.name,
                            contact = it.contact,
                            nif = it.nif,
                            nib = it.nib,
                            country = it.country,
                            address = it.address,
                            postalCode = it.postalCode,
                            email = it.email,
                            password = it.password,
                            totalProductsSold = it.totalProductsSold,
                            totalValueEarnedFromSales = it.totalValueEarnedFromSales,
                        )

                        data.value = newSeller
                    }
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<SellerInformationDto>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return data
    }

    /**
     * Update information from a seller, who is identified by the token returned from the login
     */
    fun updateSellerInformation(id: Int, seller: SellerInformationDto): LiveData<ReplyObject> {
        val reply = MutableLiveData(ReplyObject(false, "Seller information not updated"))

        val response: Call<Void> = sellerApi.updateSellerInformation(id, seller)

        response.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    reply.value = ReplyObject(true, "Seller information updated successfully")
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return reply
    }
    fun updateProductInformation(token: String, productId: Int, product: ProductDto): LiveData<ReplyObject> {
        val reply = MutableLiveData(ReplyObject(false, "Product information not updated"))

        val response: Call<Void> = productApi.updateProductInformation(token, productId, product)

        response.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    reply.value = ReplyObject(true, "Product information updated successfully")
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return reply
    }
    /**
     * Update the state of an order, identified by it's id, from a seller, who is identified by the token returned from the login
     */
    fun updateOrderState(token: String, id: Int, state: String): LiveData<ReplyObject> {
        val reply = MutableLiveData(ReplyObject(false, "Order state not updated"))

        val response: Call<Void> = orderApi.updateSellerPackageState("Bearer $token", id, state)

        response.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    reply.value = ReplyObject(true, "Order state updated successfully")
                } else {
                    // Handle unsuccessful response
                    response.errorBody()?.string()?.let { Log.d("Response body", it) }
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                // Handle failure
                Log.e("Retrofit", "Error: ${t.message}")
            }
        })

        return reply
    }

    /**
     * Login a seller
     */
    suspend fun login(email: String, password: String): LiveData<String?> {
        val response = sellerApi.login(SellerLogin(email, password))
        Log.d("Response body", Gson().toJson(response.body()))
        response.errorBody()?.let { Log.d("Response body", it.string()) }

        val data = MutableLiveData<String?>()
        if (response.isSuccessful) {
            val content = response.body()
            content?.let {
                data.value = it.token
            }
        }

        return data
    }

    /**
     * Register a seller
     */
    suspend fun register(seller: SellerInformationDto): LiveData<SellerInformation> {
        val response = sellerApi.register(seller)
        val data = MutableLiveData<SellerInformation>()
        if (response.isSuccessful) {
            val content = response.body()
            content?.let {
                val newSeller = SellerInformation(
                    id = it.id,
                    name = it.name,
                    contact = it.contact,
                    nif = it.nif,
                    nib = it.nib,
                    country = it.country,
                    address = it.address,
                    postalCode = it.postalCode,
                    email = it.email,
                    password = it.password,
                    totalProductsSold = it.totalProductsSold,
                    totalValueEarnedFromSales = it.totalValueEarnedFromSales,
                )

                data.value = newSeller
            }
        }

        return data
    }
}