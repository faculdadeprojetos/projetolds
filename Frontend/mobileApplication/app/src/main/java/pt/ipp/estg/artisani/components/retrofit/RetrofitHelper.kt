package pt.ipp.estg.artisani.components.retrofit

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import pt.ipp.estg.artisani.R
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.io.InputStream
import java.security.GeneralSecurityException
import java.security.KeyManagementException
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.util.Arrays
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

object RetrofitHelper {
    fun getInstance(url: String, context: Context): Retrofit {
        val httpClientBuilder = OkHttpClient.Builder().hostnameVerifier { _, _ -> true }.addInterceptor { chain ->
            val request = chain.request()

            // Log request details here
            Log.d("RetrofitHelper", "Request: $request")

            val response = chain.proceed(request)

            // Log response details here
            Log.d("RetrofitHelper", "Response: $response")

            response
        }.readTimeout(5, TimeUnit.SECONDS)
        initSSL(context, httpClientBuilder)

        return Retrofit.Builder().baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create()).client(httpClientBuilder.build())
            .build()
    }

    private fun initSSL(context: Context, httpClientBuilder: OkHttpClient.Builder) {
        var sslContext: SSLContext? = null
        try {
            sslContext = createCertificate(context.resources.openRawResource(R.raw.client_certificate))
        } catch (e: CertificateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        if (sslContext != null) {
            httpClientBuilder.sslSocketFactory(
                sslContext.socketFactory,
                systemDefaultTrustManager()
            )
        }
    }

    @Throws(
        CertificateException::class,
        IOException::class,
        KeyStoreException::class,
        KeyManagementException::class,
        NoSuchAlgorithmException::class
    )
    private fun createCertificate(trustedCertificateIS: InputStream): SSLContext {
        val cf = CertificateFactory.getInstance("X.509")
        val ca: Certificate = trustedCertificateIS.use { trustedCertificate ->
            cf.generateCertificate(trustedCertificate)
        }

        // creating a KeyStore containing our trusted CAs
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", ca)

        // creating a TrustManager that trusts the CAs in our KeyStore
        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
        val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
        tmf.init(keyStore)

        // creating an SSLSocketFactory that uses our TrustManager
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, tmf.trustManagers, null)
        return sslContext
    }

    private fun systemDefaultTrustManager(): X509TrustManager {
        return try {
            val trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as KeyStore?)
            val trustManagers = trustManagerFactory.trustManagers
            check(!(trustManagers.size != 1 || trustManagers[0] !is X509TrustManager)) {
                "Unexpected default trust managers:" + Arrays.toString(
                    trustManagers
                )
            }
            trustManagers[0] as X509TrustManager
        } catch (e: GeneralSecurityException) {
            throw AssertionError() // The system has no TLS. Just give up.
        }
    }
}
