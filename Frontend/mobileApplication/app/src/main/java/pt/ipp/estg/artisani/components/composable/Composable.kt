package pt.ipp.estg.artisani.components.composable

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import pt.ipp.estg.artisani.R
import pt.ipp.estg.artisani.components.models.OrderInformation
import pt.ipp.estg.artisani.components.models.SellerInformation
import pt.ipp.estg.artisani.components.retrofit.dtos.ProductDto


/**
 * This is the app's navigation graph.
 *
 * Part of it is defined and displayed in [MenuComponent],
 * the composable function that represents the app's menu.
 *
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 */
@Composable
fun AppNavigationMenu(
    navController: NavHostController,
    changeSelectedItem: (String) -> Unit,
) {
    var token by remember { mutableStateOf<String?>(null) }
    var currentSeller by remember { mutableStateOf<SellerInformation?>(null) }
    var currentProduct by remember { mutableStateOf<ProductDto?> (null)}

    // Navigation menu options
    NavHost(
        navController = navController,
        startDestination = "menu"
    ) {
        composable("menu") {
            MenuComponent(navController = navController, changeSelectedItem = changeSelectedItem)
        }

        composable("login") {
            LoginMenu(
                navController = navController,
                onLoginClick = {
                    token = it
                    changeSelectedItem("profile")
                    navController.navigate("profile")
                }
            )
        }


        composable("registar") {
            RegisterMenu(navController = navController)
        }



        composable("profile") {
            if (token == null) {
                changeSelectedItem("profile")
                navController.navigate("login")
            } else {
                SellerProfile(token = token!!, navController = navController, updateCurrentUser = {
                    currentSeller = it


                })
            }
        }


        composable("editProfile") {
            if (currentSeller == null || token == null) {
                Toast.makeText(
                    navController.context,
                    "You need to log in to your account to edit your profile",
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("profile")
                navController.navigate("login")
            } else {
                EditProfileForm(
                    navController = navController,
                    seller = currentSeller!!,
                    changeCurrentSeller = {
                        currentSeller = it
                    }
                )
            }
        }

        composable("products") {
            if (token == null) {
                Toast.makeText(
                    navController.context,
                    "You need to log in to your account to see your products",
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("profile")
                navController.navigate("login")
            } else {
                val productList = getAllProducts(token = token!!)

                AllProducts(productList = productList, navController)
            }
        }
        composable("newProduct") {
            if (token == null ) {
                changeSelectedItem("profile")
                navController.navigate("login")
            } else {
                AddProduct(navController = navController, token = token!!)
            }
        }
        composable("productDetail/{id}") {
            if (token == null ) {
                changeSelectedItem("profile")
                navController.navigate("login")
            } else {

                viewProductDetail(it.arguments?.getString("id")!!.toInt(), token!!, navController, changeCurrentProduct={
                    currentProduct = it
                })
            }
        }
        composable("EditProduct"){
            if (token == null || currentProduct == null) {
                changeSelectedItem("profile")
                navController.navigate("login")
            } else {

                EditProduct(token!!,navController,currentProduct!! , changeCurrentProduct = {currentProduct = it})
            }
        }

        composable("orders") {
            if (token == null) {
                Toast.makeText(
                    navController.context,
                    "You need to log in to your account to see your orders",
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("profile")
                navController.navigate("login")
            } else {
                val orderList = getAllOrders(token = token!!)

                orderList.let {
                    if (it.isNotEmpty()) {
                        OrderList(orderList = it, navController = navController)
                    }
                }
            }
        }

        composable("orders/{id}") {
            if (token == null) {
                Toast.makeText(
                    navController.context,
                    "You need to log in to your account to see your orders",
                    Toast.LENGTH_SHORT
                ).show()

                changeSelectedItem("profile")
                navController.navigate("login")
            } else {
                val order = getOrderForId(
                    token = token!!,
                    id = it.arguments?.getString("id")!!.toInt()
                ).observeAsState().value

                order?.let { notNullOrder: OrderInformation ->
                    OrderDetailsScreen(
                        token = token!!,
                        order = notNullOrder,
                        navController = navController
                    )
                }
            }
        }
    }
}

/**
 * This is the app's top bar.
 * It is displayed in [AppScaffold], which is displayed in [AppModalNavigationDrawer].
 *
 * It contains the app's navigation icon, we opted for a hamburger icon,
 * the app's title and a settings icon to navigate to the settings screen.
 *
 * @param scope The coroutine scope used to launch coroutines.
 * @param drawerState The state of the navigation drawer.
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppTopBar(
    scope: CoroutineScope,
    drawerState: DrawerState,
    navController: NavHostController,
    changeSelectedItem: (String) -> Unit
) {
    TopAppBar(
        navigationIcon = {
            IconButton(
                onClick = {
                    scope.launch {
                        drawerState.open()
                    }
                }
            ) {
                Icon(
                    imageVector = Icons.Default.Menu,
                    contentDescription = "Localized description",
                )
            }
        },
        title = {
            Text(
                text = stringResource(R.string.app_name),
                fontFamily = FontFamily.SansSerif,
                fontSize = 30.sp,
                letterSpacing = 0.3.sp,
                modifier = Modifier.padding(start = 50.dp)
            )
        },
        actions = {
            IconButton(
                onClick = {


                },
                modifier = Modifier.padding(start = 50.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Settings,
                    contentDescription = "Localized description",
                )
            }
        }
    )
}

/**
 * This is the app's scaffold.
 * It is displayed in [AppModalNavigationDrawer].
 *
 * The app's scaffold contains the app's top bar and the app's navigation graph.
 *
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param drawerState The state of the navigation drawer.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppScaffold(
    navController: NavHostController,
    drawerState: DrawerState,
    changeSelectedItem: (String) -> Unit,
) {
    val scope = rememberCoroutineScope()

    Scaffold(
        topBar = {
            AppTopBar(
                scope = scope,
                drawerState = drawerState,
                navController = navController,
                changeSelectedItem = changeSelectedItem
            )
        }
    ) {
        Column(
            modifier = Modifier.padding(paddingValues = it)
        ) {
            AppNavigationMenu(
                navController = navController,
                changeSelectedItem = changeSelectedItem,
            )
        }
    }
}

/**
 * This is the app's navigation drawer.
 *
 * It displays the total navigation graph, which is defined in [AppNavigationMenu].
 * as well as the app's top bar, which is defined in [AppTopBar].
 *
 * The navigation drawer is triggered by a swipe gesture from the left edge of the screen,
 * or by clicking on the hamburger icon in the top bar.
 *
 * Clicking on an item in the navigation drawer will navigate to the corresponding screen.
 *
 * The list of items in the navigation drawer is defined according to the components that need to be displayed
 * in the app, in this case, the components are: account, game, leaderboard.
 *
 * @param navController The navigation controller used to navigate to the corresponding screen.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppModalNavigationDrawer(navController: NavHostController) {
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)

    val drawerItems = listOf(
        "profile",
        "orders",
        "products"

    )

    val selectedItem = remember { mutableStateOf("") }

    ModalNavigationDrawer(
        gesturesEnabled = true,
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                drawerItems.forEach {
                    val itemText = it.replaceFirst(it[0], it[0].uppercaseChar(), true)
                    NavigationDrawerItem(
                        label = {
                            Text(
                                text = itemText,
                                fontFamily = FontFamily.SansSerif,
                                fontSize = 20.sp,
                                letterSpacing = 0.3.sp
                            )
                        },
                        selected = selectedItem.value == it,
                        onClick = {
                            selectedItem.value = it
                            navController.navigate(it)
                        }
                    )
                }
            }
        },
        content = {
            AppScaffold(
                navController = navController,
                drawerState = drawerState,
                changeSelectedItem = {
                    selectedItem.value = it
                },
            )
        }
    )
}

/**
 * This is a function that displays a ClickableText with a line below it.
 *
 * The ClickableText is used to navigate to the corresponding screen.
 *
 * The line below the ClickableText is used to indicate that the text is clickable.
 *
 * @param text The text to be displayed.
 * @param navController The navigation controller used to navigate to the corresponding screen.
 * @param verticalPadding The vertical padding to be applied to the ClickableText.
 * @param changeSelectedItem The function used to change the selected item in the navigation drawer.
 */
@Composable
fun AppClickableText(
    text: AnnotatedString,
    navController: NavHostController,
    verticalPadding: Dp = 0.dp,
    changeSelectedItem: ((String) -> Unit?)? = null
) {
    ClickableText(
        text = text,
        onClick = {
            if (changeSelectedItem != null) {
                changeSelectedItem(text.text.lowercase())
            }
            navController.navigate(text.text.lowercase())
        },
        modifier = Modifier
            .padding(vertical = verticalPadding)
            .drawBehind {
                val strokeWidthPx = 1.dp.toPx()
                val verticalOffset = size.height - 2.sp.toPx()
                drawLine(
                    color = Color.Black,
                    strokeWidth = strokeWidthPx,
                    start = Offset(0f, verticalOffset),
                    end = Offset(size.width, verticalOffset)
                )
            },
    )
}