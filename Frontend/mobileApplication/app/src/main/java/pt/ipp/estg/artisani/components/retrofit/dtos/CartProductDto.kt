package pt.ipp.estg.artisani.components.retrofit.dtos

class CartProductDto (
    val id: Int,
    val quantityBought: Int,
    val product: ProductDto,
    val totalValue: Float,
)