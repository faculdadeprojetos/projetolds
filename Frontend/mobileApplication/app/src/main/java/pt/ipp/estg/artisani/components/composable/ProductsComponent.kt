package pt.ipp.estg.artisani.components.composable

import android.R
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pt.ipp.estg.artisani.components.controllers.filterToXDigits
import pt.ipp.estg.artisani.components.data.AppViewModel
import pt.ipp.estg.artisani.components.models.OrderInformation
import pt.ipp.estg.artisani.components.models.Product
import pt.ipp.estg.artisani.components.models.ReplyObject
import pt.ipp.estg.artisani.components.models.SellerInformation
import pt.ipp.estg.artisani.components.retrofit.dtos.ProductDto
import pt.ipp.estg.artisani.components.retrofit.dtos.SellerInformationDto
import java.io.ByteArrayOutputStream

@Composable
fun AllProducts(productList: List<Product>, navController: NavHostController) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {

        Text(
            text = "Lista dos seus produtos:",
            color = Color.Blue,
            style = MaterialTheme.typography.headlineSmall,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(vertical = 8.dp, horizontal = 50.dp),
        )
        Button(onClick = {

            navController.navigate("newProduct")
        }) {
            Text(text="Criar Produto")
        }

        productList.forEach { product ->
            ProductItem(product, navController)
            Divider(modifier = Modifier.padding(horizontal = 10.dp))
        }
    }
}

@Composable
fun ProductItem(product: Product, navController: NavHostController) {

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = product.name, fontWeight = FontWeight.Bold,
            color = Color.Black,
            modifier = Modifier.clickable {
                navController.navigate("productDetail/${product.id}")
            }
        )
        Text(text = "Quantidade em Stock: ${product.stock}", color = Color.Black)
        Text(
            text = "Preço de Venda: ${product.individualValue}",
            color = Color.Black
        )

    }

}

@Composable
fun getAllProducts(token: String): MutableList<Product> {
    val productList = mutableListOf<Product>()
    val appViewModel: AppViewModel = viewModel()
    val listOfProducts = appViewModel.getProductsFromSeller(token).observeAsState()

    listOfProducts.value?.let { notNullProducts: List<Product?> ->
        notNullProducts.forEach { product: Product? ->
            product?.let {
                val newProduct = Product(
                    id = it.id,
                    stock = it.stock,
                    name = it.name,
                    category = it.category,
                    individualValue = it.individualValue,
                    height = it.height,
                    width = it.width,
                    weight = it.weight,
                    typeOfMaterialMade = it.typeOfMaterialMade,
                    washingInstructions = it.washingInstructions,
                    sellerId = it.sellerId,
                    imagePath = it.imagePath
                )

                productList.add(newProduct)
            }
        }
    } ?: run {
        Log.d("Product Management", "No Products found")
    }

    return productList
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun viewProductDetail(
    productId: Int,
    token: String,
    navController: NavHostController,
    changeCurrentProduct: (ProductDto) -> Unit
) {

    val appViewModel: AppViewModel = viewModel()
    val seller = appViewModel.getSellerInformation(token).observeAsState()
    val product = appViewModel.getProductById(token, productId).observeAsState()

    var productName = product.value?.name.toString()
    var productIndividualPrice = product.value?.individualValue
    var productCategory = product.value?.category.toString()
    var productHeight = product.value?.height
    var productWidth = product.value?.width
    var productWeight = product.value?.weight
    var productTypeOfMaterialMade = product.value?.typeOfMaterialMade.toString()
    var productWashingInstructions = product.value?.washingInstructions.toString()
    var productImagePath = product.value?.imagePath.toString()
    var productDto : ProductDto

    productDto = ProductDto()
    if (product.value?.id != null && product.value?.stock != null && productIndividualPrice != null && productHeight != null && productWidth != null && productWeight != null && product.value?.sellerId != null) {
         productDto = ProductDto(
            product.value?.id!!,
            product.value?.stock!!,
            productName,
            productCategory,
            productIndividualPrice,
            productHeight,
            productWidth,
            productWeight,
            productTypeOfMaterialMade,
            productWashingInstructions,
            product.value?.sellerId!!,
            0,
            0,
            productImagePath
        )
    }

    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(scrollState)
    ) {
        OutlinedTextField(
            value = productName,
            onValueChange = {},
            label = { Text("Name") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = productCategory,
            onValueChange = {},
            label = { Text("Category") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = "$$productIndividualPrice",
            onValueChange = {},
            label = { Text("Individual Price") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = productHeight.toString(),
            onValueChange = {},
            label = { Text("Height") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = productWidth.toString(),
            onValueChange = {},
            label = { Text("Width") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = productWeight.toString(),
            onValueChange = {},
            label = { Text("Weight") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = productTypeOfMaterialMade,
            onValueChange = {},
            label = { Text("Material Made") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        OutlinedTextField(
            value = productWashingInstructions,
            onValueChange = {},
            label = { Text("Washing Instructions") },
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        )
        Button(onClick = {
            changeCurrentProduct(productDto)
            navController.navigate("EditProduct")
        }) {
        Text(text="Editar")
        }

    }





}

@Composable
fun EditProduct(
    token: String,
    navController: NavHostController,
    product: ProductDto,
    changeCurrentProduct: (ProductDto) -> Unit
) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
            contentColor = Color.White,
        ),
        modifier = Modifier
            .padding(50.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        )
    ) {
        EditProductColumn(
            token,
            navController = navController,
            product = product,
            changeCurrentProduct = changeCurrentProduct
        )
    }
}

@Composable
fun EditProductColumn(
    token: String,
    navController: NavHostController,
    product: ProductDto,
    changeCurrentProduct: (ProductDto) -> Unit
) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(5.dp)
            .background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Editar Produto",
            fontSize = 30.sp,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(bottom = 20.dp),
            color = Color.Black
        )

        EditProfileFormFields(
            token,
            navController = navController,
            product = product,
            changeCurrentProduct = changeCurrentProduct
        )
    }
}


@Composable
fun EditProfileFormFields(
    token: String,
    navController: NavHostController,
    product: ProductDto,
    changeCurrentProduct: (ProductDto) -> Unit
) {
    val scope = rememberCoroutineScope()

    val ctx = LocalContext.current


    var productName by rememberSaveable { mutableStateOf(product.name) }
    var productStock by rememberSaveable { mutableIntStateOf(product.stock) }
    var productIndividualPrice by rememberSaveable { mutableFloatStateOf(product.individualPrice) }
    var productHeight by rememberSaveable { mutableIntStateOf(product.height) }
    var productWidth by rememberSaveable { mutableIntStateOf(product.width) }
    var productCategory by rememberSaveable { mutableStateOf(product.category) }
    var productWeight by rememberSaveable { mutableFloatStateOf(product.weight) }
    var productTypeOfMaterialMade by rememberSaveable { mutableStateOf(product.typeOfMaterialMade) }
    var productWashingInstructions by rememberSaveable { mutableStateOf(product.washingInstructions) }
    var hasEditProfileButtonBeenClicked by remember { mutableStateOf(false) }
    var notValidFormFields by remember { mutableStateOf("") }
    var openAlertDialog by remember { mutableStateOf(false) }

    if (hasEditProfileButtonBeenClicked) {


        if (notValidFormFields.isNotEmpty()) {
            if (!openAlertDialog) {
                ErrorDialog(
                    onDismissRequest = {
                        openAlertDialog = it
                        hasEditProfileButtonBeenClicked = false
                        notValidFormFields = ""
                    },
                    title = "Editar Produto",
                    message = "Os seguintes campos não são válidos:",
                    notValidFields = notValidFormFields,
                    buttonText = "Ok",
                )
            }
        } else {

            val appViewModel: AppViewModel = viewModel()

            val product = ProductDto(
                product.id,
                productStock,
                productName,
                productCategory,
                productIndividualPrice,
                productHeight,
                productWidth,
                productWeight,
                productTypeOfMaterialMade,
                productWashingInstructions,
                product.sellerId,
                product.packageId,
                0,
                product.imagePath


            )

            // Update the user in the database.
            if (hasEditProfileButtonBeenClicked) {
                Log.d("Product Information", "Entered productButtonhasbeenClicked")
                LaunchedEffect(Unit) {
                    Log.d("Product Information", "Entered LaunchedEffect")

                    // Swapped logic to not use .observeAsState() because it kept calling the insertPlayer() method multiple times.
                    appViewModel.updateProductInformation(token, product.id, product).asFlow()
                        .collect { result: ReplyObject ->
                            Log.d("SellerInformation", "Entered collect")
                            Log.d("SellerInformation", result.toString())

                            if (result.wasSuccessful) {
                                scope.launch {

                                    Toast.makeText(
                                        ctx,
                                        result.message,
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    changeCurrentProduct(product)

                                    // Change the screen to the profile screen.
                                    navController.navigate("profile")
                                }
                            }

                        }
                }
            }

        }
    }

    FormOutlinedTextField(
        value = productName,
        onValueChange = { productName = it },
        label = "Nome do Produto",

        )

    FormOutlinedTextField(
        value = productIndividualPrice.toString(),
        onValueChange = { productIndividualPrice = it.toFloatOrNull() ?: 0f },
        label = "Preço Individual",
    )
    FormOutlinedTextField(
        value = productHeight.toString(),
        onValueChange = { productHeight = it.toIntOrNull() ?: 0 },
        label = "Altura",
    )
    FormOutlinedTextField(
        value = productWidth.toString(),
        onValueChange = { productWidth = it.toIntOrNull() ?: 0 },
        label = "Largura",
    )
    FormOutlinedTextField(
        value = productStock.toString(),
        onValueChange = { productStock = it.toIntOrNull() ?: 0 },
        label = "Stock",
    )
    FormOutlinedTextField(
        value = productWeight.toString(),
        onValueChange = { productWeight = it.toFloatOrNull() ?: 0f },
        label = "Peso",
    )
    FormOutlinedTextField(
        value = productCategory,
        onValueChange = { productCategory = it },
        label = "Categoria do Produto",
    )
    FormOutlinedTextField(
        value = productTypeOfMaterialMade,
        onValueChange = { productTypeOfMaterialMade = it },
        label = "Tipo de material do Produto",
    )
    FormOutlinedTextField(
        value = productWashingInstructions,
        onValueChange = { productWashingInstructions = it },
        label = "Instruções de lavagem do produto",
    )



    Button(
        onClick = {
            hasEditProfileButtonBeenClicked = true
            openAlertDialog = false
        },
        content = {
            Text(
                text = "Editar",
                fontFamily = FontFamily.SansSerif,
                fontSize = 18.sp,
                color = Color.Black
            )
        },
    )
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddProduct(navController: NavHostController, token: String) {
    val appViewModel: AppViewModel = viewModel()
    var productButtonWasClicked by remember { mutableStateOf(false) }

    val seller = appViewModel.getSellerInformation(token).observeAsState()

    val previewProduct = Product(
        id = 123,
        stock = 0,
        name = "",
        category = "",
        individualValue = 0f,
        height = 0,
        width = 0,
        weight = 0f,
        typeOfMaterialMade = "",
        washingInstructions = "",
        sellerId = 0,
        imagePath = ""
    )

    // Actual product being edited
    var product by remember { mutableStateOf(previewProduct) }
    val context = LocalContext.current

    var wasAddProductButtonClicked by remember { mutableStateOf(false) }

    var selectedImage: Bitmap? by remember { mutableStateOf(null) }
    var selectedImageName by remember { mutableStateOf<String?>(null) }
    var selectedImagePath by remember { mutableStateOf<String?>(null) }

    val singlePhotoPicker = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent(),
        onResult = { uri ->
            if (uri != null) {
                // Load the selected image and pass it to the callback
                val inputStream = context.contentResolver.openInputStream(uri)
                selectedImage = BitmapFactory.decodeStream(inputStream)

                // Update the selectedImageName with the file name
                selectedImageName = uri.lastPathSegment

                // Convert bitmap to Base64
                selectedImage?.let { image ->
                    val imageBase64 = convertBitmapToBase64(image)
                    // Update the selectedImagePath with the Base64 string
                    selectedImagePath = imageBase64
                    Log.d("BASE64", selectedImagePath!!)
                }
            }
        }
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(rememberScrollState())
    ) {
        OutlinedTextField(
            value = product.name,
            onValueChange = { product = product.copy(name = it) },
            label = { Text("Name", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Text
            )
        )

        OutlinedTextField(
            value = product.stock.toString(),
            onValueChange = { product = product.copy(stock = it.toIntOrNull() ?: 0) },
            label = { Text("Stock", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number
            )
        )

        OutlinedTextField(
            value = product.category,
            onValueChange = { product = product.copy(category = it) },
            label = { Text("Category", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Text
            )
        )

        OutlinedTextField(
            value = product.individualValue.toString(),
            onValueChange = { product = product.copy(individualValue = it.toFloatOrNull() ?: 0f) },
            label = { Text("Individual Value", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number
            )
        )

        OutlinedTextField(
            value = product.height.toString(),
            onValueChange = { product = product.copy(height = it.toIntOrNull() ?: 0) },
            label = { Text("Height", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number
            )
        )

        OutlinedTextField(
            value = product.width.toString(),
            onValueChange = { product = product.copy(width = it.toIntOrNull() ?: 0) },
            label = { Text("Width", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number
            )
        )

        OutlinedTextField(
            value = product.weight.toString(),
            onValueChange = { product = product.copy(weight = it.toFloatOrNull() ?: 0f) },
            label = { Text("Weight", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = Color.Black
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Number
            )
        )

        OutlinedTextField(
            value = product.typeOfMaterialMade,
            onValueChange = { product = product.copy(typeOfMaterialMade = it) },
            label = { Text("Material Made", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Text
            )
        )

        OutlinedTextField(
            value = product.washingInstructions,
            onValueChange = { product = product.copy(washingInstructions = it) },
            label = { Text("Washing Instructions", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            keyboardOptions = KeyboardOptions.Default.copy(
                keyboardType = KeyboardType.Text
            )
        )
        OutlinedTextField(
            value = seller.value?.id.toString(),
            onValueChange = { /* Não faz nada ao mudar o valor do sellerId */ },
            label = { Text("Seller ID", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )


        Button(
            onClick = {
                // Call the image picker when the button is clicked
                singlePhotoPicker.launch("image/*")
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp, bottom = 20.dp)
        ) {
            Text("Add Image")
        }


        OutlinedTextField(
            value = selectedImageName.orEmpty(),
            onValueChange = {},
            label = { Text("Selected Image", color = Color.Black) },
            colors = TextFieldDefaults.outlinedTextFieldColors(textColor = Color.Black),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )
        if (productButtonWasClicked) {
            if (selectedImage != null) {
                // Convert bitmap to Base64
                val imageBase64 = convertBitmapToBase64(selectedImage!!)
                // Update the selectedImagePath with the Base64 string
                selectedImagePath = imageBase64

                // Crie um ProductDto com os dados preenchidos
                val productDto = ProductDto(
                    id = product.id,
                    stock = product.stock,
                    name = product.name,
                    category = product.category,
                    individualPrice = product.individualValue,
                    height = product.height,
                    width = product.width,
                    weight = product.weight,
                    typeOfMaterialMade = product.typeOfMaterialMade,
                    washingInstructions = product.washingInstructions,
                    sellerId = product.sellerId,
                    packageId = 0,
                    sellerPackageId = 0,
                    imagePath = selectedImagePath

                )
                LaunchedEffect(Unit) {
                    appViewModel.addProductFromSeller(productDto, token)
                }

            }
        }

        Button(
            onClick = {

                if (validateProduct(product)) {
                    productButtonWasClicked = true
                    Toast.makeText(context, "Product created succesfully !", Toast.LENGTH_SHORT)
                        .show()
                    navController.navigate("products")
                } else {
                    Toast.makeText(
                        context,
                        "Please fill in all required fields",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp)
        ) {
            Text("Add Product")
        }
    }
}

fun convertBitmapToBase64(bitmap: Bitmap): String {
    val byteArrayOutputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
    val byteArray = byteArrayOutputStream.toByteArray()
    return Base64.encodeToString(byteArray, Base64.DEFAULT)
}


// Function to validate the product before submission
private fun validateProduct(product: Product): Boolean {
    return product.id.toString().isNotBlank() &&
            product.stock >= 0 &&
            product.name.isNotBlank() &&
            product.category.isNotBlank() &&
            product.individualValue > 0f &&
            product.height > 0f &&
            product.width > 0f &&
            product.weight > 0f &&
            product.typeOfMaterialMade.isNotBlank() &&
            product.washingInstructions.isNotBlank()
}

