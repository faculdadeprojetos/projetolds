package pt.ipp.estg.artisani.components.retrofit


import pt.ipp.estg.artisani.components.retrofit.dtos.ProductDto
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path


interface ProductService {


    @POST("create")
    fun createProduct(
        @Body product: ProductDto,
        @Header("Authorization") token: String
    ): Call<ProductDto>

    @GET("getProductsFromSeller")
    fun getProductsFromSeller(@Header("Authorization") token: String): Call<List<ProductDto>>

    @GET("{id}")
    fun getProductById(@Header("Authorization") token: String, @Path("id") productId: Int): Call<ProductDto>
    @PUT("{id}")
    fun updateProductInformation(@Header("Authorization")token: String,@Path("id") productId: Int, @Body product : ProductDto): Call<Void>
}