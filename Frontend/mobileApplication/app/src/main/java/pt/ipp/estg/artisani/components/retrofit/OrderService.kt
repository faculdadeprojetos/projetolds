package pt.ipp.estg.artisani.components.retrofit

import pt.ipp.estg.artisani.components.retrofit.dtos.OrderDto
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.PUT
import retrofit2.http.Path

interface OrderService {

    @Headers("Content-Type: application/json")
    @GET("getSellerPackageFromLoggedInSeller")
    fun getSellerPackagesFromLoggedInSeller(@Header("Authorization") token: String): Call<List<OrderDto>?>

    @Headers("Content-Type: application/json")
    @GET("getSellerPackageFromLoggedInSeller/{id}")
    fun getSellerPackageFromLoggedInSeller(@Header("Authorization") token: String, @Path("id") id: Int): Call<OrderDto>

    @Headers("Content-Type: application/json")
    @PUT("updateSellerPackageState/{id}")
    fun updateSellerPackageState(@Header("Authorization") token: String, @Path("id") id: Int, @Body state: String): Call<Void>
}