package pt.ipp.estg.artisani.components.data

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import okhttp3.MultipartBody
import pt.ipp.estg.artisani.components.models.OrderInformation
import pt.ipp.estg.artisani.components.models.Product
import pt.ipp.estg.artisani.components.models.ReplyObject
import pt.ipp.estg.artisani.components.models.SellerInformation
import pt.ipp.estg.artisani.components.retrofit.OrderService
import pt.ipp.estg.artisani.components.retrofit.dtos.ProductDto
import pt.ipp.estg.artisani.components.retrofit.ProductService
import pt.ipp.estg.artisani.components.retrofit.RetrofitHelper
import pt.ipp.estg.artisani.components.retrofit.dtos.SellerInformationDto
import pt.ipp.estg.artisani.components.retrofit.SellerService

class AppViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: AppRepository
    init {
        val orderEndpoint = "https://10.0.2.2:7204/api/sellerpackage/"
        val productEndpoint = "https://10.0.2.2:7204/api/product/"
        val sellerEndpoint = "https://10.0.2.2:7204/api/seller/"

        val ctx = getApplication<Application>().applicationContext

        val orderApi = RetrofitHelper.getInstance(orderEndpoint, ctx).create(OrderService::class.java)
        val productApi = RetrofitHelper.getInstance(productEndpoint, ctx).create(ProductService::class.java)
        val sellerApi = RetrofitHelper.getInstance(sellerEndpoint, ctx).create(SellerService::class.java)

        repository = AppRepository(orderApi, productApi, sellerApi)
    }

    /**
     * Get orders from a seller, who is identified by the token returned from the login
     */
    fun getOrdersFromSeller(token: String) : LiveData<List<OrderInformation?>> {
        return repository.getOrdersFromSeller(token)
    }

    /**
     * Get a specific order from a seller, who is identified by the token returned from the login
     *
     * @param id The id of the order
     */
    fun getSpecificOrderFromSeller(token: String, id: Int) : LiveData<OrderInformation> {
        return repository.getSpecificOrderFromSeller(token, id)
    }

    /**
     * Get products from a seller, who is identified by the token returned from the login
     */
    fun getProductsFromSeller(token: String) : LiveData<List<Product?>> {
        return repository.getProductsFromSeller(token)
    }
    fun getProductById(token: String, productId:Int): LiveData<Product?> {
        return repository.getProductById(token, productId)
    }

    /**
     * Add a product from a seller, who is identified by the token returned from the login
     */
    fun addProductFromSeller( product: ProductDto, token: String): LiveData<Product?> {
        return repository.addProductFromSeller( product, token)
    }

    /**
     * Get information from a seller, who is identified by the token returned from the login
     */
    fun getSellerInformation(token: String): LiveData<SellerInformation?> {
        return repository.getSellerInformation(token)
    }

    /**
     * Update information from a seller, who is identified by the token returned from the login
     */
    fun updateSellerInformation(id: Int, seller: SellerInformationDto): LiveData<ReplyObject> {
        return repository.updateSellerInformation(id, seller)
    }
    fun updateProductInformation(token: String, id: Int, product: ProductDto): LiveData<ReplyObject> {
        return repository.updateProductInformation(token, id, product)
    }
    /**
     * Update the state of an order, identified by it's id, from a seller, who is identified by the token returned from the login
     */
    fun updateOrderState(token: String, id: Int, state: String): LiveData<ReplyObject> {
        return repository.updateOrderState(token, id, state)
    }

    /**
     * Login a seller
     */
    suspend fun login(email: String, password: String): LiveData<String?> {
        return repository.login(email, password)
    }

    /**
     * Register a seller
     */
    suspend fun register(seller: SellerInformationDto): LiveData<SellerInformation> {
        return repository.register(seller)
    }
}