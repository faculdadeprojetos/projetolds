import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination'; // Importing NgxPaginationModule for pagination
import { FormsModule } from '@angular/forms';
import { MatProgressBar } from '@angular/material/progress-bar';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JwtModule } from '@auth0/angular-jwt';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ReactiveFormsModule } from '@angular/forms';

import { HeaderComponent } from './components/partials/header/header.component';
import { FooterComponent } from './components/partials/footer/footer.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailsComponent } from './components/products/product-details/product-details.component';
import { SearchBarFilterPipe } from './components/shared/searchBarFilter.pipe';
import { LoginComponent } from './components/client-profile/login/login.component';
//import { JwtInterceptor } from './shared/jwt.interceptor'; // Importing JwtInterceptor for JWT Interceptor functionality
import { ReviewComponent } from './components/review/review.component';
import { CartComponent } from './components/cart/cart.component';
import { RegistrationComponent } from './components/client-profile/registration/registration.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { EditProfileComponent } from './components/client-profile/edit-profile/edit-profile.component';
import { PersonalInformationComponent } from './components/client-profile/personal-information/personal-information.component';
import { PurchaseHistoryComponent } from './components/client-profile/purchase-history/purchase-history.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { HomeComponent } from './components/home/home.component';
import { ExclusiveProductsComponent } from './components/exclusive-products/exclusive-products.component';
import { ClientComponent } from './components/client-profile/client/client.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductsComponent,
    ProductDetailsComponent,
    SearchBarFilterPipe,
    LoginComponent,
    ReviewComponent,
    PurchaseHistoryComponent,
    CartComponent,
    RegistrationComponent,
    CheckoutComponent,
    EditProfileComponent,
    PersonalInformationComponent,
    AboutUsComponent,
    HomeComponent,
    ExclusiveProductsComponent,
    ClientComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule,
    FormsModule,
    MatProgressBarModule,
    HttpClientModule,
    JwtModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
