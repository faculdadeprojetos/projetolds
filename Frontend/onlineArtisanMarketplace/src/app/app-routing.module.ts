import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailsComponent } from './components/products/product-details/product-details.component';
import { LoginComponent } from './components/client-profile/login/login.component';
import { PurchaseHistoryComponent } from './components/client-profile/purchase-history/purchase-history.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { AuthGuard } from './services/auth/auth.guard';
import { ReviewComponent } from './components/review/review.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { HomeComponent } from './components/home/home.component';
import { ExclusiveProductsComponent } from './components/exclusive-products/exclusive-products.component';

//import { AuthGuard } from './services/auth/auth.guard';
import { RegistrationComponent } from './components/client-profile/registration/registration.component';
import { PersonalInformationComponent } from './components/client-profile/personal-information/personal-information.component';
import { ClientComponent } from './components/client-profile/client/client.component';

const routes: Routes = [
  { path: 'products', component: ProductsComponent },
  { path: 'product/:id', component: ProductDetailsComponent },
  {
    path: 'client/view',
    component: PersonalInformationComponent,
    canActivate: [AuthGuard],
  },
  { path: 'client/registration', component: RegistrationComponent },
  { path: 'reviews/:productId', component: ReviewComponent },
  { path: 'client/login', component: LoginComponent },
  { path: 'client/login/:showErrorMessage', component: LoginComponent }, // Define a route for the login component
  { path: 'cart', component: CartComponent }, // Define a route for the cart component
  {
    path: 'client/view/purchaseHistory',
    component: PurchaseHistoryComponent,
    canActivate: [AuthGuard],
  },
  { path: 'cart/checkout', component: CheckoutComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: '', component: HomeComponent },
  { path: 'exclusive-products', component: ExclusiveProductsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
