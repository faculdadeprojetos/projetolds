import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from 'src/app/models/Cart';
import { CartProduct } from 'src/app/models/CartProduct';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { CheckMobileService } from 'src/app/services/checkMobile/check-mobile.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent {
  cart!: Cart;

  // Quantity of each product in the cart
  quantities: { [productId: number]: number } = {};

  // Error messages
  quantityInvalid: boolean = false;
  insufficientStock: boolean = false;

  // Check if mobile
  isMobile: boolean = false;

  // Preferred currency symbol to display on the product price
  preferredCurrencySymbol: string = '$';

  constructor(
    private cartService: CartService,
    private currencyService: CurrencyService,
    private router: Router,
    private checkMobile: CheckMobileService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    // Get the cart from the cart service
    this.cartService.getCart().subscribe((cart: Cart) => {
      this.cart = cart;
      this.cart.products.forEach((cartProduct) => {
        this.quantities[cartProduct.product.id] =
          cartProduct.quantityBought;
      });
    });

    // Check if mobile device to display the correct view
    this.checkMobile.checkIfIsMobile().subscribe((isMobile: boolean) => {
      this.isMobile = isMobile;
    });

    // Get the preferred currency symbol to display the correct currency
    this.currencyService
      .getPreferredCurrencySymbol()
      .subscribe((preferredCurrencySymbol: string) => {
        this.preferredCurrencySymbol = preferredCurrencySymbol;
      });
  }

  /**
   * Remove a product from the cart
   * @param cartProduct The cart product to remove from the cart
   */
  removeProductFromCart(cartProduct: CartProduct) {
    this.cartService.removeFromCart(cartProduct);
  }

  /**
   * Navigate to the given url
   *
   * If an object is passed, navigate to the url with the objectId appended to the url
   *
   * Store the current url in local storage to be able to navigate back to
   * the previous url when the user refreshes the page or comes back to website after closing the tab
   *
   * @param url The url to navigate to
   * @param objectId The object to pass to the url - May be undefined
   */
  navigateTo(url: string, objectId?: any) {
    if (objectId) {
      this.router.navigate(['' + url + '/' + objectId]);
    } else {
      if (url == 'cart/checkout') {
        if (this.authService.isLoggedIn()) {
          this.router.navigate(['' + url]);
        } else {
          this.router.navigate(['/client/login/true'], {
            queryParams: { returnUrl: url },
          });
          url = "client/login/true?returnUrl=cart/checkout";
        }
      }
    }

    localStorage.setItem('previousUrl', url);
  }

  /**
   * Update the quantity of a product in the cart
   *
   * If the quantity is invalid, set the quantityInvalid variable to true - Used to display an error message
   * If the quantity is greater than the stock of the product, set the insufficientStock variable to true
   *
   * If the quantity is valid, update the quantity of the product in the cart
   *
   * @param quantity The quantity to update the product to
   * @param productId The product id of the product to update the quantity of
   * @returns Nothing, the returns are used here to stop the function from executing
   */
  updateQuantity(quantity: number, productId: number) {
    if (quantity < 1 || quantity > 10) {
      this.quantityInvalid = true;
      return;
    }

    let productStock = this.cart.products.find(
      (product) => product.product.id == productId
    )?.product.stock;

    if (productStock != undefined && quantity > productStock) {
      this.insufficientStock = true;
      return;
    }

    this.cartService.updateQuantity(quantity, productId);
  }
}
