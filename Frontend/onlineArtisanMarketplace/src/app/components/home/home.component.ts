import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  constructor(private router: Router) {}

  navigateTo(url: string, object?: any) {
    if (object) {
      localStorage.setItem('previousUrl', url + '/' + object);
      document.getElementById('homeLi')?.classList.remove('active');
      document.getElementById('products')?.classList.add('active');
      this.router.navigate(['/' + url + '/' + object]);
    } else {
      localStorage.setItem('previousUrl', url);
      document.getElementById('homeLi')?.classList.remove('active');
      document.getElementById('products')?.classList.add('active');
      this.router.navigate(['/' + url]);
    }
  }
}
