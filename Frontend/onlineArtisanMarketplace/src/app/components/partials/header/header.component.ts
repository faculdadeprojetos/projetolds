import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';

import { Router } from '@angular/router';
import { SearchBarService } from 'src/app/services/searchBar/search-bar.service';
import { Cart } from 'src/app/models/Cart';
import { CartService } from 'src/app/services/cart/cart.service';
import { CartProduct } from 'src/app/models/CartProduct';
import { CheckMobileService } from 'src/app/services/checkMobile/check-mobile.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [
    './header.component.css',
    '../../../../assets/css/style.css',
    '../../../../assets/css/responsive.css',
    '../../../../assets/css/bootstrap.min.css',
    '../../../../assets/css/bootsnav.css',
  ],
})
export class HeaderComponent implements OnInit {
  cart!: Cart;
  searchTerm!: string;

  isMobile: boolean = false;

  productArray!: Product[];

  preferredCurrencySymbol: string = '$';

  constructor(
    private router: Router,
    private el: ElementRef,
    private renderer: Renderer2,
    private searchBarService: SearchBarService,
    private productService: ProductService,
    private cartService: CartService,
    private currencyService: CurrencyService,
    private checkMobile: CheckMobileService
  ) {
    this.cartService.getCart().subscribe((cart: Cart) => {
      if (cart) {
        this.cart = cart;
      }
    });
  }

  ngOnInit(): void {
    this.checkIfSearchBarIsOpen();
    this.checkIfWindowIsMobile();
    this.checkLastClickedTab();

    this.checkMobile.checkIfIsMobile().subscribe((isMobile) => {
      this.isMobile = isMobile;
    });

    this.productService.getProducts().subscribe((products: Product[]) => {
      this.productArray = products;
      this.changeCurrencyForProducts();
    });

    this.currencyService
      .getPreferredCurrencySymbol()
      .subscribe((preferredCurrencySymbol: string) => {
        this.preferredCurrencySymbol = preferredCurrencySymbol;
      });

    window.addEventListener('resize', () => this.checkIfWindowIsMobile());
  }

  ngAfterViewInit() {
    const liElement = this.el.nativeElement.querySelector('li.nav-setting');
    const spanElement = this.el.nativeElement.querySelector('span.lnr-user');

    const observerUserIconClicks = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.attributeName === 'class') {
          if (liElement.classList.contains('active')) {
            this.renderer.addClass(spanElement, 'active');
          } else {
            this.renderer.removeClass(spanElement, 'active');
          }
        }
      });
    });

    observerUserIconClicks.observe(liElement, { attributes: true });
  }

  onEnter(event: Event) {
    this.searchTerm = (event.target as HTMLInputElement).value;
    this.searchBarService.searchText.next(this.searchTerm);
    this.searchTerm = '';
  }

  checkIfSearchBarIsOpen() {
    // Get the search bar and content elements
    var searchBar = document.getElementsByClassName(
      'top-search'
    )[0] as HTMLElement;
    var content = document.getElementsByClassName('content')[0] as HTMLElement;

    // Check if the search bar is open
    let isSearchBarNotOpen = searchBar?.style.display == 'none';

    if (isSearchBarNotOpen) {
      // If the search bar is not open, check if content exists
      if (content) {
        // If content exists, set the margin top to 0px
        // since the search bar wouldn't overlap the content of the page
        content.style.marginTop = '0px';
      }
    } else {
      // If the search bar is open, check if content exists
      if (content) {
        // If content exists, set the margin top to 60px
        // since the search bar would overlap the content of the page
        // if we didn't set the margin top of the content div to 60px
        content.style.marginTop = '60px';
      }
    }
  }

  navigateTo(url: string, object?: any): void {
    // Check if the url is null
    // If it is null, set the previousUrl to an empty string
    // and navigate to the home page
    if (!url) {
      // Get all elements with the active class
      let activeElements = document.getElementsByClassName('active');

      // Remove the active class from all elements
      // This will make the glow of the tab go away
      for (let i = 0; i < activeElements.length; i++) {
        activeElements[i].classList.remove('active');
      }

      // After removing the active class from all elements,
      // set the previousUrl to an empty string
      // make the home tab glow golden
      // and navigate to the home page
      localStorage.setItem('previousUrl', '');
      document.getElementById('homeLi')?.classList.add('active');
      this.router.navigate(['/']);
    }

    // Get the current url and store it in local storage
    // Afterwards, navigate to the url that is passed in
    if (object) {
      localStorage.setItem('previousUrl', url + '/' + object);
      this.router.navigate(['/' + url + '/' + object]);
    } else {
      localStorage.setItem('previousUrl', url);
      this.router.navigate(['/' + url]);
    }
  }

  checkLastClickedTab() {
    // Get the last clicked tab from local storage
    let url = localStorage.getItem('previousUrl');

    // Check if the url is not null
    if (url) {
      switch (url) {
        case 'products':
          // If the url is not null, and products was the last clicked tab,
          // add the active class to the products li element --> this will make the products tab glow golden
          // and navigate to the products page
          document.getElementById('products')?.classList.add('active');
          this.router.navigate(['/products']);
          break;
        case 'exclusive-products':
          // If the url is not null, and exclusive-products was the last clicked tab,
          // add the active class to the exclusive-products li element --> this will make the exclusive-products tab glow golden
          // and navigate to the exclusive-products page
          document.getElementById('featured')?.classList.add('active');
          this.router.navigate(['/exclusive-products']);
          break;
        case 'aboutUs':
          // If the url is not null, and aboutUs was the last clicked tab,
          // add the active class to the aboutUs li element --> this will make the aboutUs tab glow golden
          // and navigate to the aboutUs page
          document.getElementById('aboutUs')?.classList.add('active');
          this.router.navigate(['/about-us']);
          break;
        case 'cart':
          // If the url is not null, and cart was the last clicked tab,
          // add the active class to the cart icon element --> this will make the cart icon glow golden
          // and navigate to the cart page
          document.getElementById('cart')?.classList.add('active');
          this.router.navigate(['/cart']);
          break;
        case 'cart/checkout':
          // If the url is not null, and cart/checkout was the last clicked tab,
          // add the active class to the cart icon element --> this will make the cart icon glow golden
          // and navigate to the cart/checkout page
          document.getElementById('cart')?.classList.add('active');
          this.router.navigate(['/cart/checkout']);
          break;
        case 'client':
          // If the url is not null, and client was the last clicked tab,
          // add the active class to the client icon element --> this will make the client icon glow golden
          // and navigate to the client page
          document.getElementById('client')?.classList.add('active');
          this.router.navigate(['/client']);
          break;
        case 'client/view':
          // If the url is not null, and client/view was the last clicked tab,
          // add the active class to the client icon element --> this will make the client icon glow golden
          // and navigate to the client/view page
          document.getElementById('client')?.classList.add('active');
          this.router.navigate(['/client/view']);
          break;
        case 'client/login/true?returnUrl=cart/checkout':
          // If the url is not null, and client/login/true?returnUrl=cart/checkout was the last clicked tab,
          // add the active class to the client icon element --> this will make the client icon glow golden
          // and navigate to the client/login/true?returnUrl=cart/checkout page
          console.log('here');
          document.getElementById('client')?.classList.add('active');
          this.router.navigate(['/client/login/true'], {
            queryParams: { returnUrl: 'cart/checkout' },
          });
          break;
      }
    } else {
      // If the url is null, then send the user to the home page
      document.getElementById('homeLi')?.classList.add('active');
      this.router.navigate(['/']);
    }
  }

  checkIfWindowIsMobile() {
    // If the window is mobile, put the glass, user and cart icons on the left side of the screen
    var attrNav = document.getElementsByClassName('attr-nav')[0] as HTMLElement;

    var cartDropdown = document.getElementById('cartDropdown') as HTMLElement;

    if (window.innerWidth <= 991) {
      if (attrNav) {
        attrNav.style.left = '10px';
      }

      // Move the cart dropdown to the left side of the screen
      if (cartDropdown) {
        cartDropdown.style.left = '-30px';
        cartDropdown.style.borderWidth = '4.8px 4.8px 0.8px 4.8px';
        cartDropdown.style.borderColor =
          'rgb(33, 37, 41) rgb(33, 37, 41) rgb(33, 37, 41) rgb(33, 37, 41)';
        cartDropdown.style.marginTop = '0px';
      }

      this.checkMobile.checkIfIsMobile().next(true);
    } else {
      if (attrNav) {
        attrNav.style.left = '';
      }

      // Maintain the cart dropdown on the right side of the screen
      // In case they have phone in landscape mode and switch to portrait mode
      // and vice versa
      if (cartDropdown) {
        cartDropdown.style.left = '';
        // border-top 4.8px, border-right 0.8px, border-bottom 0.8px, border-left 0.8px
        cartDropdown.style.borderWidth = '4.8px 4.8px 0.8px 4.8px';
        cartDropdown.style.marginTop = '0px';
      }

      this.checkMobile.isMobile.next(false);
    }
  }

  /**
   * Remove a product from the cart
   * @param product The product to add to the cart
   */
  removeProductFromCart(product: CartProduct) {
    this.cartService.removeFromCart(product);
  }

  /**
   * Change the currency for the products
   *
   * Get the preferred currency from the currency service
   * and change the currency for the products
   * as well as update the product price of the products in the cart
   */
  changeCurrencyForProducts() {
    let updateProducts: Product[] = [];

    this.currencyService
      .getPreferredCurrency()
      .subscribe((preferredCurrency) => {
        updateProducts = [];
        if (preferredCurrency) {
          // Instead of changing the currency for each product individually,
          // Map the result of that to a new variable and call the productService to update the products array
          // using the .next() method
          this.productArray.forEach((product) => {
            this.currencyService
              .changeCurrencyForProducts(product.individualPrice, preferredCurrency)
              .subscribe((price: number) => {
                // Limit the price to 2 decimal places
                let updatedPrice = parseFloat(price.toFixed(2));

                product.individualPrice = updatedPrice;

                this.cartService.updateProductPrice(
                  product.id,
                  product.individualPrice
                );

                updateProducts.push(product);
              });
          });
        }

        this.productService.updateProducts(updateProducts);
      });

      // Update current productArray
      this.productArray = updateProducts;
  }
}
