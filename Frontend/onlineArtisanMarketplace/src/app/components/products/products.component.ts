import { Component } from '@angular/core';
import { Product } from '../../models/Product';
import { Router } from '@angular/router';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';
import { SearchBarService } from 'src/app/services/searchBar/search-bar.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent {
  productArray: Product[] = [];
  showProductDetails = false;
  selectedProduct!: Product;

  p: number = 0;
  searchText!: string;
  checkedFilters!: boolean[];

  constructor(
    private router: Router,
    private searchBarService: SearchBarService,
    private productService: ProductService

  ) {}

  /**
   * This method is called when the component is initialized
   * 
   * It tracks the changes in the search bar text and updates the searchText variable
   * so that the products can be filtered based on the search text
   */
  ngOnInit(): void {
    this.searchBarService.searchText.subscribe((text: string) => {
      this.searchText = text;
    });

    this.productService.products.subscribe((products) => {
      this.productArray = products;
    });
  }

  /**
   * This method is called after the view is initialized
   * 
   * It is used to remove the padding from the ngx-pagination control
   * element. It is used to center the pagination control of the products
   */
  ngAfterViewInit(): void {
    let paginationControl = document.querySelector('.ngx-pagination');

    if (paginationControl) {
      paginationControl.setAttribute('style', 'padding-left: 0px !important;');
    }
  }

  /**
   * This method is called when the user clicks on a card in the products page
   * @param productId The id of the product that was clicked
   */
  productDetails(productId: number) {
    const product = this.productArray.find(
      (product) => product.id == productId
    );

    this.selectedProduct = product!;
    
    this.showProductDetails = true;
  }

  /**
   * This method is called when the user clicks on the close button in the product details page
   */
  closeProductDetails() {
    this.showProductDetails = false;
    // Refresh the page to request the products to the server again
    this.router
      .navigateByUrl('/products', { skipLocationChange: true })
  }

  /**
   * This method is called when the user clicks on the filter texts on the top of the products page
   * @param category Filter to removed
   * @param isChecked Whether the filter is checked or not
   */
  addRemoveFilter(category: string, isChecked: boolean) {
    let currentFilters = this.searchBarService.selectedFilters.getValue();

    if (isChecked) {
      currentFilters.push(category);

      if (category === 'stock') {
        this.sendSearchBarRequest(1);
      }
    } else {
      currentFilters.splice(currentFilters.indexOf(category), 1);

      if (category === 'stock') {
        this.sendSearchBarRequest(2);
      }
    }

    this.searchBarService.selectedFilters.next(currentFilters);
  }

  /**
   * Method called when the user presses the enter key while selecting the text bar
   * @param type The type of request to be sent to the search bar service
   */
  sendSearchBarRequest(type: number) {
    switch (type) {
      case 1:
        this.searchBarService.searchText.next('stock');
        break;
      case 2:
        this.searchBarService.searchText.next('');
        break;
    }
  }
}
