import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartProduct } from 'src/app/models/CartProduct';
import { Product } from 'src/app/models/Product';
import { CartService } from 'src/app/services/cart/cart.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent {
  // Get the data from the parent component
  @Input() product!: Product;

  // Close the product details component
  @Output() closeProductDetails = new EventEmitter<void>();

  // Check if the product details component was accessed from the parent component
  wasNotAccessedFromParentComponent: boolean = true;

  // Quantity of the product to add to the cart
  quantity: number = 1;

  // Error messages
  quantityInvalid: boolean = false;
  insufficientStock: boolean = false;

  // Preferred currency symbol to display on the product price
  preferredCurrencySymbol: string = '$';

  constructor(
    private cartService: CartService,
    private currencyService: CurrencyService,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService
  ) {}

  /**
   * Initialize the product details component
   *
   * Get the product from the product service if the product details component was not
   * accessed from the parent component
   *  - Example: If the user clicks on a product's image from inside the cart menu
   *   they will be redirected to the product details component because they're sending the objectId
   *   in the url.
   *
   * Get the preferred currency symbol to display the correct currency
   * through the currency service
   */
  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const productId = params['id'];
      const possibleProduct = this.productService.getProductById(productId);

      // Check if the product exists in case the user manually enters the url
      if (possibleProduct) {
        this.wasNotAccessedFromParentComponent = false;
        this.product = possibleProduct;
      }
    });

    this.currencyService
      .getPreferredCurrencySymbol()
      .subscribe((preferredCurrency: string) => {
        this.preferredCurrencySymbol = preferredCurrency;
      });
  }

  /**
   * Close the product details component
   *
   * If the product details component was not accessed from the parent component
   * then redirect to the products page
   *
   * Otherwise, close the product details component
   * through the closeProductDetails event emitter
   * that is handled in the parent component
   */
  onClose(): void {
    if (!this.wasNotAccessedFromParentComponent) {
      this.router.navigate(['products']);
    } else {
      this.closeProductDetails.emit();
    }
  }

  /**
   * Add the product to the cart
   *
   * If the quantity is invalid, set the quantityInvalid flag to true
   * Example: If the user doesn't set a quantity and clicks on the add to cart button
   *
   * If the cart does not have enough stock, set the insufficientStock flag to true
   *
   * Otherwise, add the product to the cart
   * @param product The product to add to the cart
   */
  onAddToCart(product: Product): void {
    if (!this.quantity) {
      this.quantityInvalid = true;
    }

    let doesCartHaveEnoughStock = this.quantity <= product.stock;

    if (doesCartHaveEnoughStock) {
      const cartProduct = new CartProduct(
        product.id,
        product,
        this.quantity
      );
      this.cartService.addToCart(cartProduct);
    } else {
      this.insufficientStock = true;
    }
  }

  /**
   * Update the quantity shown on the product details component
   * This function is called when the user types anything in the quantity input
   * Since it is called with the ngModelChange event
   *
   * If the quantity is invalid, set the quantityInvalid flag to true
   * Example: If the user doesn't sets a quantity less than 1 or greater than 10
   *
   * If the cart does not have enough stock, set the insufficientStock flag to true
   *
   * @returns Nothing, the returns are used here to stop the function from executing
   */
  updateQuantityInvalidStatus(): void {
    if (!this.quantity) {
      this.quantityInvalid = false;
      return;
    }

    this.quantityInvalid = this.quantity < 1 || this.quantity > 10;
  }
}
