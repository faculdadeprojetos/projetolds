import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/models/Client';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ClientRestService } from 'src/app/services/clientRest/client-rest.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css'],
})
export class PersonalInformationComponent implements OnInit {
  client!: Client
  isEditing?: boolean = false;
  isWatchingSales?: boolean = false;

  constructor(
    private authService: AuthService,
    private clientService: ClientRestService,
    private router: Router,
    private currencyService: CurrencyService,
  ) {}

  ngOnInit(): void {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/clients/login']);
    } else {
      this.getCurrentClient();
    }
  }

  editProfile() {
    this.isEditing = true;
  }

  seeSales() {
    this.isWatchingSales = true;
  }

  closeSeeSales() {
    this.isWatchingSales = false;
  }

  /**
   * Gets the current client
   */
  getCurrentClient(): void {
    this.clientService.getClient().subscribe((client: Client) => {
      if (client == null) {
        this.router.navigate(['/client/login']);
      } else {
        this.client = client;
      }
    });
  }

  /**
     * Logs the user out upon being called
     * 
     * Is called when the logout button is clicked
     */
  logout() {
    this.authService.logout();

    this.currencyService.setPreferredCurrency('USD');
  }
}
