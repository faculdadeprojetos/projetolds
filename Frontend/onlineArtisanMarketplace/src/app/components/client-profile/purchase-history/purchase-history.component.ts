import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CartProduct } from 'src/app/models/CartProduct';
import { Client } from 'src/app/models/Client';
import { Package } from 'src/app/models/Package';
import { Product } from 'src/app/models/Product';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.css'],
})
export class PurchaseHistoryComponent {
  @Input() client: Client | undefined;

  @Output() closeIsWatchingSales = new EventEmitter<void>();

  // Array to store purchase history
  purchaseHistoryArray: Package[] = [];
  
  // Search term for filtering purchase history
  searchTerm: string = '';

  constructor(private router: Router, private checkoutService: CheckoutService) {}

  ngOnInit(): void {
    // Get purchase history for the current user
    this.checkoutService.getPurchaseHistory().subscribe((packages: Package[]) => {
      this.purchaseHistoryArray = packages;
    });
  }

  // Get products for a given package ID
  getProductsByPackageId(packageId: number): CartProduct[] | undefined {
    let findPackage = this.purchaseHistoryArray.find(
      (purchase) => purchase.id === packageId
    );

    return findPackage ? findPackage.products : undefined;
  }

  // Show products for a given package ID
  showProductsByPackageId(purchase: Package): void {
    purchase.showProducts = true;
  }

  // Close product details for a given package ID
  closeProductDetails(purchase: Package): void {
    purchase.showProducts = false;
  }

  // Navigate back to the home page
  goBack(): void {
    this.closeIsWatchingSales.emit();
  }

  // Get filtered purchase history based on search term
  get filteredPurchaseHistory(): Package[] {
    return this.purchaseHistoryArray.filter((purchase) =>
      purchase.products.some((product) =>
        product.product.name.toLowerCase().includes(this.searchTerm.toLowerCase())
      )
    );
  }
}
