import { Component, Input, OnInit } from '@angular/core';
import { Client } from 'src/app/models/Client';
import { Router } from '@angular/router';
import { ClientRestService } from 'src/app/services/clientRest/client-rest.service';
import { PersonalInformationComponent } from '../personal-information/personal-information.component';
import { NgToastService } from 'ng-angular-popup';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormClient } from 'src/app/models/ClientForm';
import { ReactiveFormsModule } from '@angular/forms';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
})
export class EditProfileComponent implements OnInit {
  @Input() client!: Client;

  clientForm!: FormGroup;

  error1: boolean = false;
  error2: boolean = false;

  constructor(
    private rest: ClientRestService,
    private router: Router,
    private personal: PersonalInformationComponent,
    private toast: NgToastService
  ) {}

  ngOnInit(): void {
    this.clientForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(60),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      // xxxx-xxx
      postalCode: new FormControl('', [
        Validators.required,
        Validators.pattern(/^\d{4}-\d{3}$/)
      ]),
      contact: new FormControl('', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      preferredCurrency: new FormControl('', [Validators.required]),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(30),
      ]),
      nif: new FormControl('', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      country: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(40),
      ]),
      password: new FormControl('', [
        Validators.maxLength(20)
      ]),
    });
  }

  get name() {
    return this.clientForm.get('name')!;
  }
  get email() {
    return this.clientForm.get('email')!;
  }
  get postalCode() {
    return this.clientForm.get('postalCode')!;
  }
  get contact() {
    return this.clientForm.get('contact')!;
  }
  get address() {
    return this.clientForm.get('address')!;
  }
  get country() {
    return this.clientForm.get('country')!;
  }
  get password() {
    return this.clientForm.get('password')!;
  }
  get nif() {
    return this.clientForm.get('nif')!;
  }
  get preferredCurrency(){
    return this.clientForm.get('preferredCurrency')!;
  }

  editProfile() {
    if (this.clientForm.invalid) {
      this.error1 = true;
      return;
    }

    this.rest.editClient(this.clientForm.value as FormClient).subscribe({
      next: (res: any) => {
        let local = JSON.parse(localStorage.getItem('currentUser')!);
        local.name = this.client.name;
        localStorage.setItem('currentUser', JSON.stringify(local));
        localStorage.setItem('preferredCurrency', this.client.preferredCurrency);

        this.toast.success({
          detail: 'Sucesso!',
          summary: 'Perfil do utilizador editado!',
          duration: 5000,
        });
        this.personal.isEditing = false;
      },
      error: (error: any) => {
        if (error.status == 422) {
          this.error1 = true;
        } else if (error.status == 500) {
          this.error2 = true;
        }
      },
    });
  }

  closeEditInformation() {
    this.personal.isEditing = false;
  }

  closeError(errorNumber: number) {
    if (errorNumber == 1) {
      this.error1 = false;
    } else {
      this.error2 = false;
    }
  }
}

