import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/models/Client';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ClientRestService } from 'src/app/services/clientRest/client-rest.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    //private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService,
    private currencyService: CurrencyService,
    private clientService: ClientRestService
  ) {}

  loading: boolean = false;
  errorLoginFailed: boolean = false;

  showNeedLoginMessage: boolean = false;

  private failedAttempts: {
    [email: string]: { counter: number; firstAttempt: Date };
  } = {};

  private blockedEmails: string[] = [];

  private blockedEmailsTimer: {
    [email: string]: { timer: number };
  } = {};

  blockedEmailError: boolean = false;

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

      if (returnUrl) {
        if (returnUrl.includes('?')) {
          location.href = returnUrl;
        }
        this.router.navigateByUrl(returnUrl);
      }
    }

    // Check the params to see if the user was redirected from a page that requires authentication
    this.route.params.subscribe((params) => {
      const needLogin = params['showErrorMessage'];

      if (needLogin) {
        this.showNeedLoginMessage = true;
      } else {
        this.loadLoginAttemptsInfoFromLocalStorage();
      }
    });
  }

  @HostListener('window:beforeunload')
  unloadHandler() {
    this.storeLoginAttemptsInfoInLocalStorage();
  }

  /**
   * Function that stores the login attempts info in the local storage
   *
   * It contains the following info:
   * - failedAttempts: Object that contains the number of failed attempts for each email
   * - blockedEmails: Array that contains the emails that are blocked
   * - blockedEmailError: Boolean that indicates if the user tried to login with a blocked email
   * - blockedEmailsTimer: Object that contains the timer for each blocked email
   */
  private storeLoginAttemptsInfoInLocalStorage() {
    const failedAttempts = JSON.stringify(this.failedAttempts);
    const blockedEmails = JSON.stringify(this.blockedEmails);
    const blockedEmailsTimer = JSON.stringify(this.blockedEmailsTimer);

    const loginAttemptsInfo = {
      failedAttempts,
      blockedEmails,
      blockedEmailsTimer,
    };

    localStorage.setItem(
      'loginAttemptsInfo',
      JSON.stringify(loginAttemptsInfo)
    );
  }

  /**
   * Function that loads the login attempts info from the local storage
   *
   * It contains the following info:
   * - failedAttempts: Object that will contain the number of failed attempts for each email
   * - blockedEmails: Array that will contain the emails that are blocked
   * - blockedEmailError: Boolean that will indicate if the user tried to login with a blocked email
   * - blockedEmailsTimer: Object that will contain the timer for each blocked email
   */
  private loadLoginAttemptsInfoFromLocalStorage() {
    const localStorageObject = localStorage.getItem('loginAttemptsInfo');

    const loginAttemptsInfo = JSON.parse(
      localStorageObject ? localStorageObject : '{}'
    );

    if (loginAttemptsInfo) {
      let failedAttempts = JSON.parse(loginAttemptsInfo.failedAttempts);

      this.failedAttempts = {};
      Object.entries(failedAttempts).forEach(([email, e]) => {
        let attempt = e as { counter: number; firstAttempt: string };
        this.failedAttempts[email] = {
          counter: attempt.counter,
          firstAttempt: new Date(attempt.firstAttempt),
        };
      });

      this.blockedEmails = JSON.parse(loginAttemptsInfo.blockedEmails);
      this.blockedEmailsTimer = JSON.parse(
        loginAttemptsInfo.blockedEmailsTimer
      );

      if (this.blockedEmailsTimer) {
        for (const email in this.blockedEmailsTimer) {
          this.restartTimerOnBlockedEmail(email);
        }
      }
    }
  }

  private startTimerOnBlockedEmail(email: string) {
    if (this.blockedEmailsTimer[email]) {
      return;
    }

    this.blockedEmailsTimer[email] = { timer: 0 };

    let timerId = setInterval(() => {
      this.blockedEmailsTimer[email].timer += 1000;

      this.displayBlockedEmailError(email);

      if (this.blockedEmailsTimer[email].timer >= 120000) {
        // Stop the timer
        clearInterval(timerId);

        // Delete the timer from the blockedEmailsTimer object
        // and remove the email from the blockedEmails array
        delete this.blockedEmailsTimer[email];
        delete this.failedAttempts[email];
        this.blockedEmails = this.blockedEmails.filter((e) => e != email);

        this.blockedEmailError = false;
      }
    }, 1000);
  }

  private restartTimerOnBlockedEmail(email: string) {
    let timerId = setInterval(() => {
      this.blockedEmailsTimer[email].timer += 1000;

      this.displayBlockedEmailError(email);

      if (this.blockedEmailsTimer[email].timer >= 120000) {
        // Stop the timer
        clearInterval(timerId);

        // Delete the timer from the blockedEmailsTimer object
        // and remove the email from the blockedEmails array
        delete this.blockedEmailsTimer[email];
        delete this.failedAttempts[email];
        this.blockedEmails = this.blockedEmails.filter((e) => e != email);

        this.blockedEmailError = false;
      }
    }, 1000);
  }

  /**
   * Function that displays the blocked email error
   */
  private displayBlockedEmailError(email: string) {
    var blockedEmailErrorMessage = document.getElementById('blockedEmailError');

    var minutes = Math.floor(
      (120 - this.blockedEmailsTimer[email].timer / 1000) / 60
    );

    var seconds = Math.round(
      (120 - this.blockedEmailsTimer[email].timer / 1000) % 60
    );

    if (blockedEmailErrorMessage) {
      if (minutes != 0) {
        blockedEmailErrorMessage.innerHTML = `<strong>You tried to login too many times.</strong> Please wait ${minutes} minutes and ${seconds} seconds until you can try to log with the email ${email} again.`;
      } else {
        blockedEmailErrorMessage.innerHTML = `<strong>You tried to login too many times.</strong> Please wait ${seconds} seconds until you can try to log with the email ${email} again.`;
      }

      this.blockedEmailError = true;
    }
  }

  /**
   * Attempts to login
   * @param email
   * @param pass
   * @returns void
   * @memberof LoginComponent
   */
  login(email: string, pass: string) {
    this.loading = true;

    if (this.blockedEmails.includes(email)) {
      this.loading = false;
      this.displayBlockedEmailError(email);
      return;
    }

    this.auth.login(email, pass).subscribe({
      next: (res) => {
        localStorage.setItem('currentUser', JSON.stringify(res));
        this.auth.loggedIn();

        this.clientService.getClient().subscribe((client: Client) => {
          if (client) {
            this.currencyService.setPreferredCurrency(client.preferredCurrency);
          }
        });

        if (this.failedAttempts[email]) {
          delete this.failedAttempts[email];
        }

        let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

        if (returnUrl) {
          if (returnUrl.includes('?')) {
            location.href = returnUrl;
          }
          this.router.navigateByUrl(returnUrl);
        }
      },
      error: () => {
        this.loading = false;
        this.errorLoginFailed = true;

        if (!this.failedAttempts[email]) {
          this.failedAttempts[email] = { counter: 1, firstAttempt: new Date() };
        } else {
          // If the user has tried to login more than 5 times in the last 2 minutes, block the login and send an email to the user's email
          this.failedAttempts[email].counter++;

          if (
            this.failedAttempts[email].counter >= 5 &&
            new Date().getTime() -
              this.failedAttempts[email].firstAttempt.getTime() <=
              120000
          ) {
            this.clientService.sendTooManyFailedAttemptsEmail(email).subscribe({
              next: () => {
                console.log('Email sent successfully');
              },
              error: () => {
                console.log('Error sending email');
              },
            });

            // Reset the failed attempts for this email
            this.failedAttempts[email] = {
              counter: 0,
              firstAttempt: new Date(),
            };

            this.blockedEmails.push(email);

            this.startTimerOnBlockedEmail(email);
          } else if (
            new Date().getTime() -
              this.failedAttempts[email].firstAttempt.getTime() >
            120000
          ) {
            this.failedAttempts[email].firstAttempt = new Date();
          }
        }
      },
    });
  }

  closeErrorLoginFailed() {
    this.errorLoginFailed = false; // Define para false apenas quando o usuário clicar no botão de fechar
  }

  register() {
    var returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

    this.router.navigate(['/client/registration'], {
      queryParams: { returnUrl: returnUrl },
    });
  }
}
