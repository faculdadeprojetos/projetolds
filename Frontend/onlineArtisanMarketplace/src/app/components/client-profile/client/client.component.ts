import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent {
  
    constructor(private authService: AuthService, private currencyService: CurrencyService) { }

    /**
     * Logs the user out upon being called
     * 
     * Is called when the logout button is clicked
     */
    logout() {
      this.authService.logout();

      this.currencyService.setPreferredCurrency('USD');
    }
}
