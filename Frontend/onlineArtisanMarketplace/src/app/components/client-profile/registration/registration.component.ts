import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { ClientRestService } from 'src/app/services/clientRest/client-rest.service';
import { FormClient } from 'src/app/models/ClientForm';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  @Input() client: FormClient;

  //flags to components
  error1 = false;
  error2 = false; 
  error3 = false;

  clientForm!: FormGroup;

  constructor(
    private clientService: ClientRestService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: NgToastService
  ) {
    this.client = new FormClient();
  }

  ngOnInit(): void {
    this.clientForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(60),
      ]),
      email: new FormControl('', [Validators.required]),
      postalCode: new FormControl('', [Validators.required]),
      contact: new FormControl('', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      preferredCurrency : new FormControl('', [Validators.required]),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(30),
      ]),
      nif: new FormControl('', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      country: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }
  get name() {
    return this.clientForm.get('name')!;
  }
  get email() {
    return this.clientForm.get('email')!;
  }
  get postalCode() {
    return this.clientForm.get('postalCode')!;
  }
  get contact() {
    return this.clientForm.get('contact')!;
  }
  get address() {
    return this.clientForm.get('address')!;
  }
  get country() {
    return this.clientForm.get('country')!;
  }
  get password() {
    return this.clientForm.get('password')!;
  }
  get nif() {
    return this.clientForm.get('nif')!;
  }
  get preferredCurrency(){
    return this.clientForm.get('preferredCurrency')!;
  }

  /*
   * Registration of a client
   */
  createClient() {
    this.clientService
      .createClient(this.clientForm.value as FormClient)
      .subscribe({
        next: () => {
          this.toast.success({
            detail: 'Sucesso!',
            summary: 'Perfil do utilizador registado!',
            duration: 5000,
          });

          localStorage.setItem('preferredCurrency', this.clientForm.value.preferredCurrency);

          var returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

          if (returnUrl) {
            this.router.navigate(['/client/login'], {
              queryParams: { returnUrl: returnUrl },
            });
          } else {
            this.router.navigate(['/client/login']);
          }
        },
        error: (error: any) => {
          if (error.status == 422) {
            this.error1 = true;
          } else if (error.status == 409) {
            this.error2 = true;
          } else {
            this.error3 = true;
          }
        },
      });
  }

  //errors alert management
  closeError(num: number) {
    if (num == 1) {
      this.error1 = false;
    } else if (num == 2) {
      this.error2 = false;
    } else if (num == 3) {
      this.error3 = false;
    }
  }
}
