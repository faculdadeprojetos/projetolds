import { Component } from '@angular/core';
import { Review } from '../../models/Review';
import { ActivatedRoute } from '@angular/router';
import { ReviewsService } from 'src/app/services/reviews/reviews.service';
import { ClientRestService } from 'src/app/services/clientRest/client-rest.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { EMPTY, catchError } from 'rxjs';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
})
export class ReviewComponent {
  productId?: number;
  reviews: Review[] = [];

  novaReviewTexto: string = '';
  novaReviewRating: number = 1;

  editReviews: boolean = false;
  reviewToEdit?: Review | null = null;

  showFilterOptions: boolean = false;
  filterType: 'asc' | 'desc' | 'normal' = 'normal';

  errorOccured: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private reviewService: ReviewsService,
    private clientService: ClientRestService,
    private authService: AuthService
  ) {
    this.route.params.subscribe((params) => {
      this.productId = +params['productId'];

      this.reviewService
        .getReviewsForProduct(this.productId!)
        .subscribe((reviews: Review[]) => {
          this.reviews = reviews;
        });
    });
  }

  // Get reviews for the current product
  getReviewsForProduct(): Review[] {
    return this.reviews.filter((review) => review.productId === this.productId);
  }

  // Add or edit a review
  adicionarReview(): void {
    if (this.novaReviewTexto.trim() !== '') {
      if (this.editReviews && this.reviewToEdit) {
        var reviewToEdit = { ...this.reviewToEdit };
        var newReviewText = this.novaReviewTexto;
        var newReviewRating = this.novaReviewRating;

        if (this.authService.isLoggedIn()) {
          this.clientService.getClient().subscribe((client) => {
            let clientId = client.id;

            // Editing an existing review
            reviewToEdit.comment = newReviewText;
            reviewToEdit.rating = newReviewRating;

            let newEditedReview = new Review(
              reviewToEdit.id,
              reviewToEdit.productId,
              reviewToEdit.comment,
              reviewToEdit.rating,
              clientId
            );

            this.reviewService
              .editReview(newEditedReview, newEditedReview.id)
              .pipe(
                catchError((error) => {
                  console.log(error);
                  this.errorOccured = true;
                  return EMPTY;
                })
              )
              .subscribe({
                next: (updatedReview) => {
                  this.reviews = this.reviews.map((review) =>
                    review.id == updatedReview.id ? updatedReview : review
                  );
                },
              });
          });
        } else {
          this.errorOccured = true;
        }

        this.resetReviewInputs();
      } else {
        if (this.authService.isLoggedIn()) {
          this.clientService.getClient().subscribe((client) => {
            let clientId = client.id;

            // Adding a new review
            let newReview = new Review(
              this.reviews.length + 1,
              this.productId!,
              this.novaReviewTexto,
              this.novaReviewRating,
              clientId
            );

            this.reviewService.createReview(newReview).subscribe({
              next: (review) => {
                this.reviews.push(review);
              },
              error: (error) => {
                console.log(error);
                this.errorOccured = true;
              },
            });

            this.resetReviewInputs();
          });
        } else {
          this.errorOccured = true;
        }
      }
    } else {
      alert('Please enter text to create the review.');
    }
  }

  // Edit an existing review
  editReview(reviewId: number): void {
    this.editReviews = true;

    this.reviewToEdit = this.reviews.find((review) => review.id == reviewId);

    if (this.reviewToEdit) {
      this.novaReviewTexto = this.reviewToEdit.comment;
      this.novaReviewRating = this.reviewToEdit.rating;
    }
  }

  // Reset review form inputs
  resetReviewInputs(): void {
    this.editReviews = false;
    this.reviewToEdit = null;
    this.novaReviewTexto = '';
    this.novaReviewRating = 1;
  }

  // Toggle filter options visibility
  toggleFilterOptions(): void {
    this.showFilterOptions = !this.showFilterOptions;
  }

  // Apply review filter based on filter type
  applyFilter(filterType: 'asc' | 'desc' | 'normal'): void {
    this.filterType = filterType;
    this.showFilterOptions = false;
  }

  // Get filtered reviews based on filter type
  getFilteredReviews(): Review[] {
    const reviews = this.getReviewsForProduct();

    if (this.filterType === 'asc') {
      return reviews.slice().sort((a, b) => a.rating - b.rating);
    } else if (this.filterType === 'desc') {
      return reviews.slice().sort((a, b) => b.rating - a.rating);
    } else {
      return reviews;
    }
  }
}
