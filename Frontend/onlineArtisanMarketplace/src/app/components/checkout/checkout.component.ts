import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
} from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, debounceTime, of, switchMap, take } from 'rxjs';
import { Cart } from 'src/app/models/Cart';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { ClientRestService } from 'src/app/services/clientRest/client-rest.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent {
  cart!: Cart;

  constructor(
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
    private cartService: CartService,
    private currencyService: CurrencyService,
    private authService: AuthService,
    private clientService: ClientRestService,
    private checkoutService: CheckoutService,
    private route: Router
  ) {
    window.addEventListener('resize', () => this.onWindowResize());
  }

  // Variables used to show the checkout pages
  page1: boolean = true;
  page2: boolean = false;
  page3: boolean = false;
  page4: boolean = false;

  // Variables used to show the checkout progress
  activeCircle1: boolean = true;
  activeCircle2: boolean = false;
  activeCircle3: boolean = false;
  activeCircle4: boolean = false;

  currentPage: number = 1;

  reservedProductsTimer!: number;
  intervalId: any;

  // Variables used during the checkout process
  private _appliedDiscount: boolean = false;

  // Variable used to stop the calculateTotalPrice() method from
  // being called when the values are loaded from the local storage
  gotValueFromLocalStorage: boolean = false;

  currencyPer10Points: number = 1;

  preferredCurrencySymbol: string = '$';
  preferredCurrency: string = 'USD';

  creditCardNumber: string = '';
  securityCode: string = '';

  currentYear: number = new Date().getFullYear();

  months: number[] = Array.from({ length: 12 }, (_, i) => i + 1);
  years: number[] = Array.from({ length: 6 }, (_, i) => i + this.currentYear);
  expirationDateMonth: number = 0;
  expirationDateYear: number = 0;

  errorDetected: boolean = false;
  isErrorMessageShown: boolean = false;

  /**
   * On start of the component, the cart service should be called to get the cart needed for the checkout process.
   *
   * Additionally, the currency service should be called to get the preferred currency and the preferred currency symbol.
   *
   * And finally, there's a check to see if the client was on the third page of the checkout process
   * and if he chose the payment method 'Credit Card'. If so, the values
   * he entered are loaded from the local storage.
   */
  ngOnInit(): void {
    var changedDiscountValue = false;

    this.cartService
      .getCart()
      .pipe(debounceTime(300))
      .subscribe((cart) => {
        if (!cart) {
          this.route.navigate(['/home']);
          return;
        }

        var checkoutItems = JSON.parse(localStorage.getItem('checkoutItems')!);
        this.cart = cart;

        const clientInfo$ = this.clientService.getClient();
        const preferredCurrencySymbol$ =
          this.currencyService.getPreferredCurrencySymbol();

        if (this.authService.isLoggedIn()) {
          combineLatest([clientInfo$, preferredCurrencySymbol$])
            .pipe(take(1))
            .subscribe(([clientInfo, preferredCurrencySymbol]) => {
              this.cart.clientInfo = clientInfo;
              this.preferredCurrency = clientInfo.preferredCurrency;
              this.preferredCurrencySymbol = preferredCurrencySymbol;

              if (!changedDiscountValue) {
                changedDiscountValue = true;

                // Based on the preferred currency, get the exchange rate for the currencies
                // to calculate what the applied discount should be
                if (localStorage.getItem('exchangeRateForCurrencies')) {
                  const exchangeRateForCurrencies = JSON.parse(
                    localStorage.getItem('exchangeRateForCurrencies')!
                  );

                  if (exchangeRateForCurrencies[this.preferredCurrency]) {
                    this.currencyPer10Points =
                      exchangeRateForCurrencies[this.preferredCurrency].rate;

                    if (this.currencyPer10Points != 1) {
                      if (typeof this.currencyPer10Points != 'number') {
                        this.currencyPer10Points = parseFloat(
                          this.currencyPer10Points
                        );

                        this.currencyPer10Points = parseFloat(
                          this.currencyPer10Points.toFixed(2)
                        );

                        this.cart.pointsDiscountValue =
                          this.currencyPer10Points;
                      }

                      if (checkoutItems) {
                        this.cart.paymentMethod = checkoutItems.paymentMethod;
                        this.changeCheckoutPage(checkoutItems.checkoutPage);
                        this.gotValueFromLocalStorage = true;
                        this.appliedDiscount = checkoutItems.appliedDiscount;
                        this.loadPaymentDetailValues();
                      }
                    }
                  }
                }
              }
            });
        } // Closing bracket for if (this.authService.isLoggedIn())
      }); // Closing bracket for this.cartService.getCart().subscribe
  }

  /**
   * Getter for the appliedDiscount variable.
   */
  get appliedDiscount(): boolean {
    return this._appliedDiscount;
  }

  /**
   * Setter for the appliedDiscount variable.
   */
  set appliedDiscount(value: boolean) {
    this._appliedDiscount = value;
    this.calculateTotalPrice();
  }

  /**
   * Helper method that calculates the distance of the line between the circles.
   * This ensures that when the user resizes the window, the line between the circles
   * will always be the correct length.
   */
  private calculateLineWidth() {
    const circles = this.el.nativeElement.querySelectorAll('.circle');

    // Loop through each circle and calculate the distance to the next circle
    for (let i = 0; i < circles.length - 1; i++) {
      const currentCircle = circles[i];
      const nextCircle = circles[i + 1];

      const currentCircleRect = currentCircle.getBoundingClientRect();
      const nextCircleRect = nextCircle.getBoundingClientRect();

      // Calculate the distance between the center of the right side of one circle
      // and the left side of the next circle
      const distance = nextCircleRect.left - currentCircleRect.right;

      // Update the CSS variable with the calculated distance
      document.documentElement.style.setProperty(
        '--line-width',
        `${distance}px`
      );
    }
  }

  /**
   * Helper function that verifies if the payment can go through.
   *
   * This function verifies the data that is sent when going from the third page to the fourth page.
   * And if it returns false, an alert is shown to the user.
   *
   * @returns True if the payment went through, false otherwise.
   */
  private verifyIfPaymentCanGoThrough(): boolean {
    const paymentMethodChosen = this.cart.paymentMethod;

    switch (paymentMethodChosen) {
      case 'Credit Card':
        let errorMessage = '';
        const creditCardNumber = this.creditCardNumber.replace(/\D/g, '');
        const securityCode = this.securityCode;
        const expirationDateMonth = this.expirationDateMonth;
        const expirationDateYear = this.expirationDateYear;

        let expirationDateYearString = expirationDateYear.toString();
        let expirationDateMonthString = expirationDateMonth
          .toString()
          .padStart(2, '0');

        let isCreditCardNumberValid = creditCardNumber.length == 16;
        let isSecurityCodeValid = securityCode.length == 3;

        let isExpirationDateMonthValid =
          expirationDateMonthString.length == 2 &&
          expirationDateMonth >= 1 &&
          expirationDateMonth <= 12;

        let isExpirationDateYearValid =
          expirationDateYearString.length == 4 &&
          expirationDateYear >= this.currentYear &&
          expirationDateYear <= this.currentYear + 5;

        if (!isCreditCardNumberValid) {
          errorMessage += `Invalid credit card number (16 digits). --> Your credit card number ${creditCardNumber}\n`;
        }

        if (!isSecurityCodeValid) {
          errorMessage += `Invalid security code (3 digits). - Your security code ${securityCode}\n`;
        }

        if (!isExpirationDateMonthValid) {
          errorMessage += `Invalid expiration date month. - ${expirationDateMonthString}\n`;
        }

        if (!isExpirationDateYearValid) {
          errorMessage += `Invalid expiration date year. - ${expirationDateYearString}\n`;
        }

        if (errorMessage == '') {
          return true;
        } else {
          if (!this.isErrorMessageShown) {
            let errorAlert = document.getElementById('errorAlert')!;
            let errorMessages = errorMessage.split('\n');

            for (let msg of errorMessages) {
              if (msg != '') {
                let textNode = document.createTextNode(msg);
                errorAlert.appendChild(document.createElement('br'));
                errorAlert.appendChild(textNode);
              }
            }

            this.isErrorMessageShown = true;

            setTimeout(() => {
              errorAlert.innerHTML = '';
              this.isErrorMessageShown = false;
            }, 5000);
          }
        }
        break;
      case 'MBWay':
        return true;
      case 'Paypal':
        return true;
      case 'Bank Transfer':
        return true;
      default:
        break;
    }

    return false;
  }

  /**
   * Function that stores the values of the checkout process in the local storage.
   */
  private storeValuesInLocalStorage(): void {
    let checkoutItems = {
      appliedDiscount: this.appliedDiscount,
      securityCode: this.securityCode,
      creditCardNumber: this.creditCardNumber,
      expirationDateMonth: this.expirationDateMonth.toString(),
      expirationDateYear: this.expirationDateYear.toString(),
      checkoutPage: this.currentPage,
      paymentMethod: this.cart.paymentMethod,
    };

    localStorage.setItem('checkoutItems', JSON.stringify(checkoutItems));
  }

  /**
   * Function that loads the values of the checkout process from the local storage.
   */
  private loadPaymentDetailValues(): void {
    let checkoutItems = JSON.parse(localStorage.getItem('checkoutItems')!);

    this.securityCode = checkoutItems.securityCode;
    this.creditCardNumber = checkoutItems.creditCardNumber;
    this.expirationDateMonth = +checkoutItems.expirationDateMonth;
    this.expirationDateYear = +checkoutItems.expirationDateYear;
  }

  /**
   * Call the calculateLineWidth() method after the window has been resized.
   */
  private onWindowResize(): void {
    this.calculateLineWidth();
  }

  /**
   * Function that calculates the total price of the cart based on whether the user has applied a discount or not.
   */
  private calculateTotalPrice(): void {
    if (this.cart.clientInfo) {
      if (this.cart.clientInfo.currentPoints) {
        let discount = parseFloat(
          (
            this.currencyPer10Points *
            Math.floor(this.cart.clientInfo.currentPoints / 10)
          ).toFixed(2)
        );

        if (this.appliedDiscount) {
          this.cart.totalValueWithDiscount = parseFloat(
            (this.cart.totalValueWithDiscount - discount).toFixed(2)
          );

          this.cart.pointsUsed = this.cart.clientInfo.currentPoints;

          if (this.gotValueFromLocalStorage) {
            this.gotValueFromLocalStorage = false;
          }

        } else {
          if (this.gotValueFromLocalStorage) {
            this.gotValueFromLocalStorage = false;
          } else {
            this.cart.totalValueWithDiscount = parseFloat(
              (this.cart.totalValueWithDiscount + discount).toFixed(2)
            );

            this.cart.pointsUsed = 0;
          }
        }
      }
    }
  }

  /**
   * Call the calculateLineWidth() method after the view has been initialized.
   */
  ngAfterViewInit(): void {
    this.calculateLineWidth();
  }

  /**
   * Function that is called when the user either refreshes the page or closes the tab
   * on the third page of the checkout process while the payment method is 'Credit Card'.
   */
  @HostListener('window:beforeunload')
  unloadHandler() {
    if (this.currentPage == 3) {
      this.storeValuesInLocalStorage();
    }
  }

  /**
   * Function that changes the checkout page.
   * @param page The page to change to.
   */
  changeCheckoutPage(page: number) {
    let checkoutItems = JSON.parse(localStorage.getItem('checkoutItems')!);
    switch (page) {
      case 1:
        this.page1 = true;
        this.page2 = false;
        this.page3 = false;
        this.page4 = false;

        this.activeCircle1 = true;
        this.activeCircle2 = false;

        this.currentPage = 1;

        if (checkoutItems) {
          checkoutItems.checkoutPage = 1;
          localStorage.setItem('checkoutItems', JSON.stringify(checkoutItems));
        }

        break;
      case 2:
        this.page1 = false;
        this.page2 = true;
        this.page3 = false;
        this.page4 = false;

        this.activeCircle1 = true;
        this.activeCircle2 = true;
        this.activeCircle3 = false;

        this.currentPage = 2;
        if (checkoutItems) {
          checkoutItems.checkoutPage = 2;
          localStorage.setItem('checkoutItems', JSON.stringify(checkoutItems));
        }

        break;
      case 3:
        this.page1 = false;
        this.page2 = false;
        this.page3 = true;
        this.page4 = false;

        this.activeCircle1 = true;
        this.activeCircle2 = true;
        this.activeCircle3 = true;
        this.activeCircle4 = false;

        this.currentPage = 3;
        if (checkoutItems) {
          checkoutItems.checkoutPage = 3;
          localStorage.setItem('checkoutItems', JSON.stringify(checkoutItems));
        }
        break;
      case 4:
        const verifyIfPaymentMethodIsSelected = this.cart.paymentMethod;
        const verifyIfPaymentCanGoThrough = this.verifyIfPaymentCanGoThrough();

        if (verifyIfPaymentMethodIsSelected) {
          if (verifyIfPaymentCanGoThrough) {
            if (this.cart.clientInfo) {
              this.checkoutService
                .registerSale(
                  this.cart,
                  {
                    creditCardNumber: this.creditCardNumber,
                    securityCode: this.securityCode,
                    expirationDateMonth: this.expirationDateMonth,
                    expirationDateYear: this.expirationDateYear,
                  },
                  this.cart.clientInfo.id
                )
                .subscribe((response: any) => {
                  if (response.success) {
                    this.cartService.clearCart();
                    this.cartService.getCart().subscribe((cart) => {
                      this.cart = cart;
                    });

                    localStorage.removeItem('checkoutItems');
                  } else {
                    this.errorDetected = true;
                  }
                });
            } else {
            }

            this.page1 = false;
            this.page2 = false;
            this.page3 = false;
            this.page4 = true;

            this.activeCircle1 = true;
            this.activeCircle2 = true;
            this.activeCircle3 = true;
            this.activeCircle4 = true;

            this.currentPage = 4;
            if (checkoutItems) {
              checkoutItems.checkoutPage = 3;
              localStorage.setItem(
                'checkoutItems',
                JSON.stringify(checkoutItems)
              );
            }
          } else {
            this.errorDetected = true;
          }
        }

        break;
      default:
        break;
    }
  }

  /**
   * Function that generates a random number with the specified length on some of the payment method options
   * for numbers like IBAN, MBWay, etc.
   * @param length The length of the random number to generate.
   * @returns A random number as a string with the specified length.
   */
  generateRandomNumber(length: number): string {
    const randomNumber = Math.floor(
      Math.random() * Math.pow(10, length)
    ).toString();
    return randomNumber;
  }

  /**
   * Function that formats the security code of the credit card
   * to ensure that the user can only enter numbers.
   */
  formatSecurityCode(): void {
    setTimeout(() => {
      const cleanedValue = this.securityCode.replace(/\D/g, '');

      this.securityCode = cleanedValue;

      this.cdr.detectChanges();
    }, 0);
  }

  /**
   * Function that formats the credit card number to ensure that the user can only enter numbers
   * and adds the ' ● ' in the correct places.
   */
  formatCreditCardNumber(): void {
    setTimeout(() => {
      let cleanedValue = this.creditCardNumber.replace(/\D/g, '');

      if (cleanedValue.length == 0) {
        this.creditCardNumber = '';
      } else {
        // Check if the user has entered a number in place of the ' ● '
        const parts = this.creditCardNumber.split(' ● ');
        parts.forEach((part, index) => {
          if (part.length > 4) {
            // The user has entered a number in place of the ' ● ', so replace the number with ' ● '
            parts[index] = part.slice(0, 4) + ' ● ';
            cleanedValue = parts.join('');
          } else if (part.length == 4 && index < 3) {
            // The user has entered a full group of 4 numbers, so add the ' ● ' at the end
            parts[index] = part + ' ● ';
            cleanedValue = parts.join('');
          }
        });

        var formattedValue = cleanedValue;

        if (formattedValue.length <= 25) {
          this.creditCardNumber = formattedValue;
        }
      }

      this.cdr.detectChanges();
    }, 0);
  }

  /**
   * Function that formats the credit card number to ensure that the user can only enter numbers
   */
  updatePaymentMethod(): void {
    let checkoutItems = JSON.parse(localStorage.getItem('checkoutItems')!);

    if (checkoutItems) {
      checkoutItems.paymentMethod = this.cart.paymentMethod;
      localStorage.setItem('checkoutItems', JSON.stringify(checkoutItems));
    }
  }
}
