import { Pipe, PipeTransform } from '@angular/core';
import { SearchBarService } from 'src/app/services/searchBar/search-bar.service';

@Pipe({
  name: 'searchBarFilter',
})
export class SearchBarFilterPipe implements PipeTransform {
  constructor(private searchBarService: SearchBarService) {}

  /**
   * Function to filter the products based on the search text
   * @param items products to filter
   * @param searchText the text to search for
   * @returns the filtered products
   */
  transform(items: any[], searchText: string): any[] {
    const filterOptions: string[] = this.searchBarService.getSelectedFilters();
    const results: any = new Array();
    if (!items) {
      return [];
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLowerCase();

    var isProduct = items[0].hasOwnProperty('productId');

    items.forEach((it: any) => {
      if (isProduct) {
        filterOptions.forEach((filter) => {
          var filterValue = it[filter];

          let isText = typeof filterValue === 'string';
          let isNumber = typeof filterValue === 'number';

          if (isText) {
            filterValue = filterValue.toLowerCase();
            if (filterValue.includes(searchText)) {
              results.push(it);
            }
          } else if (isNumber) {
            if (filterValue > Number(0)) {
              results.push(it);
            }
          }
        });
      }
    });

    return results;
  }
}
