import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/Product';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product/product.service';
import { CurrencyService } from 'src/app/services/currencyService/currency.service';

@Component({
  selector: 'app-exclusive-products',
  templateUrl: './exclusive-products.component.html',
  styleUrls: ['./exclusive-products.component.css'],
})
export class ExclusiveProductsComponent implements OnInit {
  // Array of products that will be displayed
  productArray: Product[] = [];

  // Preferred currency symbol 
  // Default is $ since currency API uses USD as the base currency 
  // and we are using the free version
  preferredCurrencySymbol: string = '$';

  constructor(private router: Router, private productService: ProductService, private currencyService: CurrencyService) {
    // Use the product service to get the products
    this.productService.getProducts().subscribe((products) => {
      this.productArray = products;
    });

    // Use the currency service to get the preferred currency symbol
    this.currencyService.getPreferredCurrencySymbol().subscribe((preferredCurrencySymbol: string) => {
      this.preferredCurrencySymbol = preferredCurrencySymbol;
    });
  }

  ngOnInit(): void {
    // Filter the products to only show the ones that have stock as 1 --> This means that they are exclusive
    this.productArray = this.productArray.filter(
      (product) => product.stock == 1
    );
  }

  navigateTo(url: string, object?: any) {
    // Get the current url and store it in local storage
    // Afterwards, navigate to the url that is passed in
    if (object) {
      localStorage.setItem('previousUrl', url + '/' + object);
      this.router.navigate(['/' + url + '/' + object]);
    } else {
      localStorage.setItem('previousUrl', url);
      this.router.navigate(['/' + url]);
    }
  }
}
