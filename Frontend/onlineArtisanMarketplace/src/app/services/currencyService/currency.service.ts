import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ClientRestService } from '../clientRest/client-rest.service';
import { AuthService } from '../auth/auth.service';

const httpOptions = {
  headers: new HttpHeaders({
    accept: 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {
  // Create a new BehaviorSubject
  private preferredCurrency = new BehaviorSubject<string>('USD'); // Default value is 'USD'
  private preferredCurrencySymbol = new BehaviorSubject<string>('$'); // Default value is '$'

  constructor(
    private http: HttpClient,
    private clientService: ClientRestService,
    private authService: AuthService
  ) {
    if (this.authService.isLoggedIn()) {
      this.clientService.getClient().subscribe((client) => {
        if (client) {
          this.setPreferredCurrency(client.preferredCurrency);
        }
      });
    }
  }

  /**
   * Function to fetch the exchange rate from the openexchangerates API
   * @param endpoint The endpoint to make the request to
   * @param price The price of the product
   * @param preferredCurrency The currency that the user wants to see
   * @returns the price of the product in the preferred currency
   */
  private fetchExchangeRate(
    endpoint: string,
    price: number,
    preferredCurrency: string
  ): Observable<number> {
    return this.http.get<number>(endpoint).pipe(
      map((response: any) => {
        // Store the exchange rate for the currency in localStorage
        const targetRate = response.rates[preferredCurrency];
        let clientPreferredCurrency =
          preferredCurrency == 'EUR'
            ? '€'
            : preferredCurrency == 'BRL'
            ? 'R$'
            : preferredCurrency == 'GBP'
            ? '£'
            : '$';
        let exchangeRateForCurrencies = JSON.parse(
          localStorage.getItem('exchangeRateForCurrencies') || '{}'
        );
        exchangeRateForCurrencies[preferredCurrency] = {
          rate: targetRate.toString(),
          expiry: new Date().getTime() + 7 * 24 * 60 * 60 * 1000, // 7 days in milliseconds
          currencySymbol: clientPreferredCurrency,
        };
        localStorage.setItem(
          'exchangeRateForCurrencies',
          JSON.stringify(exchangeRateForCurrencies)
        );
        this.setPreferredCurrencySymbol(clientPreferredCurrency);

        return price * targetRate;
      }),
      catchError((error) => {
        return of(parseFloat(price.toFixed(2)));
      })
    );
  }

  /**
   * Function to get the currency symbol
   * @returns the currency symbol
   */
  public getPreferredCurrencySymbol() {
    return this.preferredCurrencySymbol.asObservable();
  }

  /**
   * Function to set the currency symbol
   * @param currencySymbol the currency symbol to set
   */
  public setPreferredCurrencySymbol(currencySymbol: string) {
    this.preferredCurrencySymbol.next(currencySymbol);
  }

  // Function to update the preferred currency
  public setPreferredCurrency(currency: string) {
    this.preferredCurrency.next(currency);

  }

  // Function to get the current preferred currency
  public getPreferredCurrency() {
    return this.preferredCurrency.asObservable();
  }

  /**
   * Function to change currency. When the component starts, it will call this function to change the currency
   * A lot of the logic will be changed once there's a backend since we will only need to make one API call and receive the target currency
   * @param price price of the products being displayed
   * @param preferredCurrency the currency that the user wants to see
   * @returns the price of the product in the preferred currency
   */
  public changeCurrencyForProducts(
    price: number,
    preferredCurrency: string
  ): Observable<number> {
    var API_KEY = '47973482c35246df9afd38896af7a775';

    const endpoint = `https://openexchangerates.org/api/latest.json?app_id=${API_KEY}&symbols=${preferredCurrency}&prettyprint=false&show_alternative=false`;

    let exchangeRateForCurrencies = JSON.parse(
      localStorage.getItem('exchangeRateForCurrencies') || '{}'
    );
    var exchangeRateForCurrency = exchangeRateForCurrencies[preferredCurrency];

    if (exchangeRateForCurrency) {
      var exchangeRateForCurrencySymbol =
        exchangeRateForCurrency.currencySymbol;
      if (new Date().getTime() > exchangeRateForCurrency.expiry) {
        // Data has expired, delete it
        delete exchangeRateForCurrencies[preferredCurrency];
        localStorage.setItem(
          'exchangeRateForCurrencies',
          JSON.stringify(exchangeRateForCurrencies)
        );

        // Make a request to the API
        return this.fetchExchangeRate(endpoint, price, preferredCurrency);
      } else {
        // Data has not expired, use the rate
        this.setPreferredCurrencySymbol(exchangeRateForCurrencySymbol);
        const targetRate = parseFloat(exchangeRateForCurrency.rate);
        return of(parseFloat((price * targetRate).toFixed(2)));
      }
    } else {
      // No data in localStorage, make a request to the API
      if (preferredCurrency != 'USD') {
        return this.fetchExchangeRate(endpoint, price, preferredCurrency);
      } else {
        return of(parseFloat(price.toFixed(2)));
      }
    }
  }
}
