import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cart } from 'src/app/models/Cart';
import { Package } from 'src/app/models/Package';

const endpoint = 'https://localhost:7204/api/package/';

@Injectable({
  providedIn: 'root',
})
export class CheckoutService {
  constructor(private http: HttpClient) {}

  /**
   * Function to register a sale
   * @param cart Cart with the products to be sold
   * @param paymentInformation Payment information of the client
   */
  public registerSale(cart: Cart, paymentInformation: any, clientId: number) {
    let totalProductsNumber = 0;

    cart.products.forEach((product) => {
      totalProductsNumber += product.quantityBought;
    });

    const packageObj: Package = new Package(
      0,
      cart.products,
      totalProductsNumber,
      cart.totalValue,
      cart.totalValueWithDiscount,
      cart.pointsUsed,
      cart.paymentMethod,
      new Date(),
      clientId,
    );

    // Remove anything that isn't a number from the credit card number
    paymentInformation.creditCardNumber =
      paymentInformation.creditCardNumber.replace(/\D/g, '');

    const objectToSend = {
      package: packageObj,
      paymentMethod: paymentInformation,
    };

    const currentUser = localStorage.getItem('currentUser')
      ? JSON.parse(localStorage.getItem('currentUser')!)
      : null;

    const token = currentUser ? currentUser.token : null;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
      }),
    };

    console.log(objectToSend);

    return this.http.post<any>(
      endpoint + 'registerSale',
      JSON.stringify(objectToSend),
      httpOptions
    );
  }

  /**
   * Function to get purchase history for the current user
   */
  public getPurchaseHistory(): Observable<Package[]> {
    const currentUser = localStorage.getItem('currentUser')
      ? JSON.parse(localStorage.getItem('currentUser')!)
      : null;

    const token = currentUser ? currentUser.token : null;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
      }),
    };

    return this.http.get<Package[]>(endpoint + 'getPackagesFromLoggedInUser', httpOptions);
  }
}
