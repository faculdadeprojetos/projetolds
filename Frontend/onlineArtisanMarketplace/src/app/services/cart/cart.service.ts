import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cart } from 'src/app/models/Cart';
import { CartProduct } from 'src/app/models/CartProduct';
import { Client } from 'src/app/models/Client';
import { Product } from 'src/app/models/Product';
import { ProductService } from '../product/product.service';
@Injectable({
  providedIn: 'root',
})
export class CartService {
  public cart: BehaviorSubject<Cart> = new BehaviorSubject<Cart>(new Cart([]));

  /**
   * Constructor for the cart service
   *
   * Gets the cart from the local storage if it exists
   */
  constructor(private productService: ProductService) {
    var tempCart = localStorage.getItem('cart');
    if (tempCart != null) {
      this.updateCartWithChangedProductPrices(JSON.parse(tempCart));
    }
  }

  private updateCartWithChangedProductPrices(cart: Cart) {
    this.productService.getProducts().subscribe((products) => {
      var updatedProducts = cart.products.map((cartProduct) => {
        var product = products.find(
          (product) => product.id == cartProduct.product.id
        );
        if (product != undefined) {
          return new CartProduct(
            cartProduct.id,
            product,
            cartProduct.quantityBought
          );
        } else {
          return cartProduct;
        }
      });

      var newTotalValue = updatedProducts.reduce((total, cartProduct) => {
        return (
          total + cartProduct.product.individualPrice * cartProduct.quantityBought
        );
      }, 0);

      var updatedCart = new Cart(updatedProducts, parseFloat(newTotalValue.toFixed(2)));

      this.cart.next(updatedCart);
    });
  }

  /**
   * Adds a product to the cart
   *
   * @param cartProduct  The card product to be added to the cart - Contains the product and the quantity of the product
   */
  addToCart(cartProduct: CartProduct): void {
    const currentCart = this.cart.getValue();
    // Search if the cartProduct id is equal to any of the current cartProduct ids in the cart
    if (
      !currentCart.products.some((currentCartProduct) => {
        return currentCartProduct.id === cartProduct.id;
      })
    ) {
      const updatedProducts = [...currentCart.products, cartProduct];

      const updatedTotalValue = currentCart.totalValue + cartProduct.totalValue;

      const updatedCart = new Cart(
        updatedProducts,
        parseFloat(updatedTotalValue.toFixed(2))
      );
      this.cart.next(updatedCart);
      localStorage.setItem('cart', JSON.stringify(updatedCart));
    }
  }

  /**
   * Removes a product from the cart
   * @param cartProduct The cart product to be removed from the cart
   */
  removeFromCart(cartProduct: CartProduct) {
    const currentCart = this.cart.getValue();

    const updatedProducts = currentCart.products.filter(
      (currentCartProduct) => {
        return currentCartProduct.product.id !== cartProduct.product.id;
      }
    );

    const updatedTotalValue = currentCart.totalValue - cartProduct.totalValue;

    const updatedCart = new Cart(
      updatedProducts,
      parseFloat(updatedTotalValue.toFixed(2))
    );
    this.cart.next(updatedCart);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  }

  /**
   * Updates the quantity of a product in the cart
   * @param quantity The new quantity of the product
   * @param productId The id of the product to be updated
   */
  updateQuantity(quantity: number, productId: number) {
    const currentCart = this.cart.getValue();
    const updatedProducts = currentCart.products.map((currentCartProduct) => {
      if (currentCartProduct.product.id == productId) {
        // Update the total value of the product
        return new CartProduct(
          currentCartProduct.id,
          currentCartProduct.product,
          quantity
        );
      } else {
        // Return the current cart product if the id is not equal to the productId
        return currentCartProduct;
      }
    });

    // Calculate the new totalValue
    const newTotalValue = updatedProducts.reduce((total, cartProduct) => {
      return (
        total + cartProduct.product.individualPrice * cartProduct.quantityBought
      );
    }, 0);

    const newCart = new Cart(
      updatedProducts,
      parseFloat(newTotalValue.toFixed(2))
    );

    this.cart.next(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
  }

  /**
   *
   * @param productId product id of the product to be updated
   * @param price new price of the product
   */
  updateProductPrice(productId: number, price: number) {
    const currentCart = this.cart.getValue();
    const updatedProducts = currentCart.products.map((currentCartProduct) => {
      if (currentCartProduct.product.id == productId) {
        // Update the total value of the product
        let newProduct = new Product(
          currentCartProduct.product.id,
          currentCartProduct.product.stock,
          currentCartProduct.product.name,
          currentCartProduct.product.category,
          price,
          currentCartProduct.product.height,
          currentCartProduct.product.width,
          currentCartProduct.product.weight,
          currentCartProduct.product.typeOfMaterialMade,
          currentCartProduct.product.washingInstructions,
          currentCartProduct.product.sellerId,
          currentCartProduct.product.imagePath
        );

        return new CartProduct(
          currentCartProduct.id,
          newProduct,
          currentCartProduct.quantityBought
        );
      } else {
        // Return the current cart product if the id is not equal to the productId
        return currentCartProduct;
      }
    });

    // Calculate the new totalValue
    var newTotalValue = updatedProducts.reduce((total, cartProduct) => {
      return (
        total + cartProduct.product.individualPrice * cartProduct.quantityBought
      );
    }, 0);

    newTotalValue = parseFloat(newTotalValue.toFixed(2));

    const newCart = new Cart(updatedProducts, newTotalValue);

    this.cart.next(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
  }

  /**
   * Gets the current cart as an observable
   * @returns The current cart as an observable
   */
  getCart(): Observable<Cart> {
    return this.cart.asObservable();
  }

  public clearCart(): void {
    this.cart.next(new Cart([]));
    localStorage.setItem('cart', JSON.stringify(new Cart([])));
  }

  /**
   * Sets the current cart
   * @param pointsDiscountValue The points discount value to be set
   */
  setPointsDiscountValue(pointsDiscountValue: number): void {
    const currentCart = this.cart.getValue();
    currentCart.setPointsDiscountValue(pointsDiscountValue);
    this.cart.next(currentCart);
  }

  /**
   * Sets the current cart
   * @param totalValueWithDiscount The total value with discount to be set
   */
  public setTotalValueWithDiscount(totalValueWithDiscount: number): void {
    const currentCart = this.cart.getValue();
    currentCart.setTotalValueWithDiscount(totalValueWithDiscount);
    this.cart.next(currentCart);
  }

  /**
   * Sets the current cart
   * @param paymentMethod The payment method to be set
   */
  public setPaymentMethod(paymentMethod: string): void {
    const currentCart = this.cart.getValue();
    currentCart.setPaymentMethod(paymentMethod);
    this.cart.next(currentCart);
  }

  /**
   * Sets the current cart
   * @param client The client info to be set
   */
  public setClientInfo(clientInfo: Client): void {
    const currentCart = this.cart.getValue();
    currentCart.setClientInfo(clientInfo);
    this.cart.next(currentCart);
  }
}
