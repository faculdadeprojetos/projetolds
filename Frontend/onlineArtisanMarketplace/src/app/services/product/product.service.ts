import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { Product } from 'src/app/models/Product';

const endpoint = 'https://localhost:7204/api/product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  
  products = new BehaviorSubject<Product[]>([]);

  /**
   * Constructor for the ProductService
   *
   * Makes a request to the backend to get all products
   *
   * @param http The HttpClient to make requests to the backend
   */
  constructor(private http: HttpClient) {}

  /**
   * Function to get all products
   * @returns All products
   */
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(endpoint);
  }

  /**
   * Function to get a product by its id
   * @param id The id of the product to get
   * @returns The product with the given id
   */
  getProductById(id: number): Product | undefined {
    var tempProduct = this.products
      .getValue()
      .find((product) => product.id == id);
    return tempProduct;
  }

  /**
   * Function to add a product or various products to the list of products
   * @param product The product or products to add to the list of products
   */
  addProducts(...product: Product[]) {
    this.products.next([...this.products.getValue(), ...product]);
  }
  
  updateProducts(updateProducts: Product[]) {
    // Reset the products in the BehaviorSubject
    this.products.next(updateProducts);
  }
}
