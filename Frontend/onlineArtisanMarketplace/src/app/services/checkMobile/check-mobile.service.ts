import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CheckMobileService {

  public isMobile = new BehaviorSubject<boolean>(false);

  constructor() { }

  /**
   * Function to check if the user is on a mobile device
   * @returns the value of the BehaviorSubject
   */
  public checkIfIsMobile() {
    return this.isMobile;
  }
}
