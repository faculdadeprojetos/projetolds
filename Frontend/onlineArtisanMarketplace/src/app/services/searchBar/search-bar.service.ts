import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchBarService {
  public searchText = new BehaviorSubject<string>('');
  public selectedFilters = new BehaviorSubject<string[]>([]);

  constructor() { }

  /**
   * Function to set the current search text
   * @returns The current selected filters
   */
  public getSelectedFilters(): string[] {
    return this.selectedFilters.getValue();
  }
}
