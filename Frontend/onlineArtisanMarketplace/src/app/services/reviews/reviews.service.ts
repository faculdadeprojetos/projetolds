import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Review } from 'src/app/models/Review';

const endpoint = 'https://localhost:7204/api/review';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(private http: HttpClient) { }

  /**
   * Method to get all reviews for a specific product
   * @param productId id of the product to get reviews for
   * @returns a list of reviews for the product
   */
  getReviewsForProduct(productId: number) {
    return this.http.get<Review[]>(`${endpoint}/getReviewsForProduct/${productId}`);
  }

  /**
   * Function to create a review
   * @returns A copy of the review created
   */
  createReview(review: Review) {
    const currentUser = localStorage.getItem('currentUser')
      ? JSON.parse(localStorage.getItem('currentUser')!)
      : null;

    const token = currentUser ? currentUser.token : null;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
      }),
    };

    return this.http.post<Review>(endpoint, review, httpOptions);
  }

  /**
   * Method to edit a review
   * @param review New review to replace the old one
   * @param id Id of the review to edit
   * @returns A copy of the review edited
   */
  editReview(review: Review, id: number) {
    const currentUser = localStorage.getItem('currentUser')
      ? JSON.parse(localStorage.getItem('currentUser')!)
      : null;

    const token = currentUser ? currentUser.token : null;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
      }),
    };

    return this.http.put<Review>(`${endpoint}/${id}`, review, httpOptions);
  }
}
