import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpContext,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../../models/Client';
import { AuthService } from '../auth/auth.service';
import { FormClient } from '../../models/ClientForm';

const endpoint = 'https://localhost:7204/api/user';

@Injectable({
  providedIn: 'root',
})
export class ClientRestService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  /**
   * Get a Client
   * @returns
   */
  getClient(): Observable<Client> {
    const currentUser = localStorage.getItem('currentUser')
      ? JSON.parse(localStorage.getItem('currentUser')!)
      : null;

    const token = currentUser ? currentUser.token : null;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
      }),
    };

    return this.http.get<Client>(endpoint + '/getUser', httpOptions);
  }

  /*
   * Registration of a client
   */
  createClient(client: FormClient): Observable<Client> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.http.post<Client>(endpoint + '/register', client, httpOptions);
  }

  /*
   * Edit of a client
   */
  editClient(client: FormClient): Observable<Client> {
    const currentUser = localStorage.getItem('currentUser')
      ? JSON.parse(localStorage.getItem('currentUser')!)
      : null;

    const token = currentUser ? currentUser.token : null;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        ...(token ? { Authorization: `Bearer ${token}` } : {}),
      }),
    };

    console.log(httpOptions);
    return this.http.put<Client>(
      endpoint + `/updateClient`,
      client,
      httpOptions
    );
  }

  sendTooManyFailedAttemptsEmail(email: String) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    const body = {
      email: email,
    };

    return this.http.post(
      endpoint + '/multipleFailedLoginAttemptsSendEmail',
      body,
      httpOptions
    );
  }
}
