export class FormClient {
    constructor(
      public name?: string,
      public contact?: number,
      public address?: string,
      public country?: string,
      public postalCode?: string,
      public nif?: number,
      public email?: string,
      public password?: string,
      public preferredCurrency?: string
    ) {}
  }
