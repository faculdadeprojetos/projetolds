import { CartProduct } from './CartProduct';
import { Product } from './Product';

export class Package {
  id: number;
  products: CartProduct[];
  totalProductQuantity: number;
  totalValue: number;
  totalValueWithDiscount: number;
  date: Date;
  userId: number;
  pointsUsed: number;
  paymentMethod: string;
  showProducts: boolean | undefined;

  constructor(
    id: number,
    products: CartProduct[],
    totalProductsNumber: number,
    totalValue: number,
    totalValueWithDiscount: number,
    pointsUsed: number,
    paymentMethod: string,
    purchaseDate: Date,
    userId: number,
  ) {
    this.id = id;
    this.products = products;
    this.totalProductQuantity = totalProductsNumber;
    this.totalValue = totalValue;
    this.totalValueWithDiscount = totalValueWithDiscount;
    this.pointsUsed = pointsUsed;
    this.paymentMethod = paymentMethod;
    this.date = purchaseDate;
    this.userId = userId;
  }
}
