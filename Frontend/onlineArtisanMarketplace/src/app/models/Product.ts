export class Product {
  id: number;
  stock: number;
  name: string;
  category: string;
  individualPrice: number;
  height: number;
  width: number;
  weight: number;
  typeOfMaterialMade: string;
  washingInstructions: string;
  sellerId: number;
  imagePath: string;

  constructor(
    id: number,
    stock: number,
    name: string,
    category: string,
    individualPrice: number,
    height: number,
    width: number,
    weight: number,
    typeOfMaterialMade: string,
    washingInstructions: string,
    sellerId: number,
    imagePath: string
  ) {
    this.id = id;
    this.name = name;
    this.stock = stock;
    this.category = category;
    this.individualPrice = individualPrice;
    this.height = height;
    this.width = width;
    this.typeOfMaterialMade = typeOfMaterialMade;
    this.washingInstructions = washingInstructions;
    this.sellerId = sellerId;
    this.imagePath = imagePath;
    this.weight = weight;
  }
}
