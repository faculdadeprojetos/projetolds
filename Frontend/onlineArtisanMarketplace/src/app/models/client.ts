export class Client {
  id: number;
  name: string;
  contact: number;
  email: string;
  password: string;
  address: string;
  country: string;
  postalCode: string;
  nif: number;
  preferredCurrency: string;
  totalPurchasesAmount: number;
  totalPurchasesValue: number;
  currentPoints: number;
  totalSpentPoints: number;

  constructor(
    id: number,
    name: string,
    contact: number,
    email: string,
    password: string,
    address: string,
    country: string,
    postalCode: string,
    nif: number,
    preferredCurrency: string,
    totalPurchasesAmount: number,
    totalPurchasesValue: number,
    currentPoints: number,
    totalSpentPoints: number
  ) {
    this.id = id;
    this.name = name;
    this.contact = contact;
    this.email = email;
    this.password = password;
    this.address = address;
    this.country = country;
    this.postalCode = postalCode;
    this.nif = nif;
    this.preferredCurrency = preferredCurrency;
    this.totalPurchasesAmount = totalPurchasesAmount;
    this.totalPurchasesValue = totalPurchasesValue;
    this.currentPoints = currentPoints;
    this.totalSpentPoints = totalSpentPoints;
  }
}
