import { Product } from "./Product";

export class CartProduct {
    id: number;
    product: Product;
    quantityBought: number;
    totalValue: number;

    constructor(id: number, product: Product, quantityBought?: number) {
        this.id = id;
        this.product = product;
        this.quantityBought = quantityBought || 1;
        this.totalValue = this.product.individualPrice * this.quantityBought;
    }
}