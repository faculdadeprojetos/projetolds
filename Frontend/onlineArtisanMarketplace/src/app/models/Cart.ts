import { CartProduct } from "./CartProduct";
import { Client } from "./Client";

export class Cart {
    products: CartProduct[];
    totalValue: number;
    pointsDiscountValue: number;
    totalValueWithDiscount: number;
    pointsUsed: number;
    paymentMethod: string;
    clientInfo!: Client;

    constructor(products: CartProduct[], totalValue?: number) {
        this.products = products;
        this.totalValue = totalValue || 0;
        this.pointsDiscountValue = 0;
        this.totalValueWithDiscount = this.totalValue;
        this.paymentMethod = '';
        this.pointsUsed = 0;
    }

    setTotalValue(totalValue: number) : void {
        this.totalValue += totalValue;
    }

    setPointsDiscountValue(pointsDiscountValue: number) : void {
        this.pointsDiscountValue = pointsDiscountValue;
    }

    setTotalValueWithDiscount(totalValueWithDiscount: number) : void {
        this.totalValueWithDiscount = totalValueWithDiscount;
    }

    setPaymentMethod(paymentMethod: string) : void {
        this.paymentMethod = paymentMethod;
    }

    setClientInfo(clientInfo: Client) : void {
        this.clientInfo = clientInfo;
    }

    getTotalValue() : number {
        return this.totalValue;
    }
}