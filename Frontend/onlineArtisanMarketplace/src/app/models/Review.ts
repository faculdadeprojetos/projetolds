export class Review {
  id: number;
  clientId: number | undefined; 
  productId: number;
  comment: string;
  rating: number;

  constructor(
    id: number,
    productId: number,
    reviewText: string,
    reviewRating: number,
    clientId?: number
  ) {
    this.id = id;
    this.clientId = clientId;
    this.productId = productId;
    this.comment = reviewText;
    this.rating = reviewRating;
  }
}
