﻿using Artisani.Data;
using Artisani.Models;
using Artisani.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.AccessControl;
using System.Security.Principal;
using static System.Net.Mime.MediaTypeNames;

namespace Artisani.Controllers
{
    /// <summary>
    /// Controller for managing products in the API.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        private readonly ArtisaniContext dbContext;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger<ProductController> _logger;
        private readonly IUserService _userService;

        public ProductController(ArtisaniContext context, IWebHostEnvironment webHostEnvironment, ILogger<ProductController> logger, IUserService userService)
        {
            dbContext = context;
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
            _userService = userService;
        }


        /*
         * <summary> Gets all products saved in the database </sumary>
         * <returns> ActionResult with a list of all existing products</returns> 
         */
        //GET :api/products
        [HttpGet]
        public IActionResult GetProducts()
        {
            if (dbContext.Products == null)
            {
                return NotFound();
            }

            var products = dbContext.Products.ToList();

            // Manuallly create a list of products to return
            List<Product> productsToReturn = new();
            foreach (var product in products)
            {
                productsToReturn.Add(new Product
                {
                    Id = product.Id,
                    Stock = product.Stock,
                    Name = product.Name,
                    Category = product.Category,
                    IndividualPrice = product.IndividualPrice,
                    Height = product.Height,
                    Width = product.Width,
                    Weight = product.Weight,
                    TypeOfMaterialMade = product.TypeOfMaterialMade,
                    WashingInstructions = product.WashingInstructions,
                    ImagePath = product.ImagePath
                });
            }

            return Ok(productsToReturn);

        }

        [HttpGet("getProductsFromSeller"), Authorize]
        public IActionResult GetProductFromSeller()
        {
            if (dbContext.Products == null)
            {
                return NotFound();
            }

            int sellerId = _userService.GetIdentityId();

            if (sellerId < 0)
            {
                return NotFound();
            }

            var products = dbContext.Products.Where(p => p.SellerId == sellerId).ToList();

            if (products == null || products.Count == 0)
            {
                return NotFound();
            }

            // Manuallly create a list of products to return
            List<Product> productsToReturn = new();
            foreach (var product in products)
            {
                productsToReturn.Add(new Product
                {
                    Id = product.Id,
                    Stock = product.Stock,
                    Name = product.Name,
                    Category = product.Category,
                    IndividualPrice = product.IndividualPrice,
                    Height = product.Height,
                    Width = product.Width,
                    Weight = product.Weight,
                    TypeOfMaterialMade = product.TypeOfMaterialMade,
                    WashingInstructions = product.WashingInstructions,
                    ImagePath = product.ImagePath
                });
            }

            return Ok(productsToReturn);
        }


        /*
         * <summary> Obtain a specific product using its unique identification number (ID) </sumary>
         * <param name="id"> Product ID to be obtained </param>
         * <returns> AAn ActionResult containing the newly created product.An ActionResult containing the newly created product.n ActionResult containing the product corresponding to the given ID. </returns> 
         */
        //GET:api/products/2
        [HttpGet("{id}")]
        public ActionResult<Product> GetProduct(int id)
        {
            if (dbContext.Products == null)
            {
                return NotFound();
            }

            var product = dbContext.Products.SingleOrDefault(p => p.Id == id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }



        /// <summary>
        /// Gets a list of products based on name.
        /// </summary>
        /// <param name="name">The name of the product to search for.</param>
        /// <returns>An ActionResult containing the list of products matching the given name</returns>
        [HttpGet("byname/{name}")]
        public ActionResult<IEnumerable<Product>>GetProductsByName(string name)
        {
            if (dbContext.Products == null)
            {
                return NotFound();
            }

            var products = dbContext.Products.Where(p => p.Name == name).ToList();

            if (products == null || products.Count == 0)
            {
                return NotFound();
            }

            return Ok(products);
        }




        /*
         * <summary> Create a new product and save it in the database  </sumary>
         * <param name="product"> New product to be created </param>
         * <returns> An ActionResult containing the newly created product </returns> 
         */
        //POST:api/products
        [HttpPost("create"),Authorize]
        public ActionResult<Product> CreateProduct(Product product)
        {

            var sellerId = _userService.GetIdentityId();
            if (sellerId < 0)
            {
                return Unauthorized();

            }
            var products = dbContext.Products;

            if (products == null)
            {
                return NotFound();

            }
           

            for (int i = 0; i < products.ToList().Count; i++)
            {
                if (products.ToList()[i].Id == product.Id)
                {
                    return BadRequest("This product already exists");
                }

                if (products.ToList()[i].Name == product.Name)
                {
                    return BadRequest("This product name already exists");
                }
            }
           

            // Obtenha a string base64 da imagem
            string base64 = product.ImagePath;

            // Converta a string base64 em bytes
            byte[] imageBytes = Convert.FromBase64String(base64);

            // Crie um nome de arquivo exclusivo
            string fileName = $"{Guid.NewGuid()}.png";

            // Obtenha o caminho físico da pasta onde deseja salvar as imagens
            string imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "./Imagem", fileName);
           

            try
            {
               

                // Check if the directory exists
                if (!Directory.Exists(Path.GetDirectoryName(imagePath)))
                {
                    // If not, create it
                    Directory.CreateDirectory(Path.GetDirectoryName(imagePath));
                }


                // Set the access control to allow write



                System.IO.File.WriteAllBytes(imagePath, imageBytes);


                // Your code to save the product...
            }
            catch (Exception ex)
            {
                // Log the exception...
                return StatusCode(500, "Internal server error");
            }
            
            // Atualize o caminho relativo no objeto Product
            product.ImagePath = $"/Imagem/{fileName}";

            product.Id = products.ToList().Count + 1;
            product.SellerId = sellerId;
            product.IndividualPrice = Math.Round(product.IndividualPrice, 2);

            dbContext.Products.Add(product);
            dbContext.SaveChanges();

            return Ok( product);
        }
    



    /*
     * <summary> Updates a product by ID </sumary>
     * <param name="product"> The Product object with the new information </param>
     * <param name="id"> The ID of the product to update  </param>
     * <returns> An IActionResult indicating the result of the operation </returns> 
     */
    //PUT: api/products/2
    [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, Product product)
        {
            if (!product.Id.Equals(id)) {
                return BadRequest();
            }

            var existingProductFromDb = dbContext.Products.Find(id);

            if (existingProductFromDb == null)
            {
                return NotFound();
            }

            if (existingProductFromDb.Id != product.Id)
            {
                return BadRequest();
            }

            // Update the product
            existingProductFromDb.Stock = product.Stock;
            existingProductFromDb.Name = product.Name;
            existingProductFromDb.Category = product.Category;
            existingProductFromDb.IndividualPrice = product.IndividualPrice;
            existingProductFromDb.Height = product.Height;
            existingProductFromDb.Width = product.Width;
            existingProductFromDb.Weight = product.Weight;
            existingProductFromDb.TypeOfMaterialMade = product.TypeOfMaterialMade;
            existingProductFromDb.WashingInstructions = product.WashingInstructions;
            existingProductFromDb.ImagePath = product.ImagePath;

            dbContext.SaveChanges();
            return NoContent();
        }



        /*
        * <summary> Deletes a product by ID </sumary>
        * <param name="id"> The ID of the product to delete </param>
        * <returns> An IActionResult indicating the result of the operation  </returns> 
        */
        //DELETE: api/products/2
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            if(dbContext.Products == null)
            {
                return NotFound();
            }

            var product = dbContext.Products.SingleOrDefault(p => p.Id == id);

            if ( product == null )
            {
                return NotFound();
            }

            dbContext.Products.Remove(product);
            dbContext.SaveChanges();
            return NoContent();
        }
    }
}
