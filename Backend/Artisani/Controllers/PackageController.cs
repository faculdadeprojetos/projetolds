﻿using System.Net;
using System.Net.Mail;
using Artisani.Data;
using Artisani.Models;
using Artisani.Models.Dtos;
using Artisani.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Artisani.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageController : ControllerBase
    {
        private readonly ArtisaniContext dbContext;
        private readonly IUserService _userService;
        private readonly ILogger<PackageController> _logger;

        public PackageController(
            ArtisaniContext context,
            ILogger<PackageController> logger,
            IUserService userService
        )
        {
            dbContext = context;
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        public IActionResult GetPackages()
        {
            if (dbContext == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Packages == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Products == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Get userId from the token and get only the packages that have the same userId

            var packagesFromDb = dbContext.Packages.ToList();
            var productsFromDb = dbContext.Products.ToList();

            if (packagesFromDb == null)
            {
                return NotFound();
            }

            if (productsFromDb == null)
            {
                return NotFound();
            }

            // Send all the packages but change the product ids to the actual products
            List<PackageDto> packagesToSend = new();

            packagesFromDb.ForEach(p =>
            {
                var quantityOfProductsBoughtInPackage = p.ProductQuantities
                    .Split(",")
                    .Select(int.Parse);

                var idsOfProductsInPackage = p.ProductIds.Split(",").Select(int.Parse);

                var products = productsFromDb.Where(p => idsOfProductsInPackage.Contains(p.Id));

                List<CartProductDto> cartProducts = new();

                for (int i = 0; i < products.Count(); i++)
                {
                    CartProductDto cartProduct =
                        new()
                        {
                            Id = i,
                            QuantityBought = quantityOfProductsBoughtInPackage.ElementAt(i),
                            Product = products.ElementAt(i),
                            TotalValue = (
                                products.ElementAt(i).IndividualPrice
                                * quantityOfProductsBoughtInPackage.ElementAt(i)
                            )
                        };

                    cartProducts.Add(cartProduct);
                }

                PackageDto package =
                    new()
                    {
                        Id = p.Id,
                        UserId = p.UserId,
                        Products = cartProducts,
                        TotalProductsNumber = p.TotalProductQuantity,
                        TotalValue = p.TotalValue,
                        TotalValueWithDiscount = p.TotalValueWithDiscount,
                        PointsUsed = p.PointsUsed,
                        Date = p.Date,
                        PaymentMethod = p.PaymentMethod,
                    };

                packagesToSend.Add(package);
            });

            return Ok(packagesToSend);
        }

        [HttpGet("getPackagesFromLoggedInUser"), Authorize]
        public IActionResult GetPackagesFromLoggedInUser()
        {
            if (dbContext == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Packages == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Products == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Get userId from the token and get only the packages that have the same userId
            int userId = _userService.GetIdentityId();

            if (userId < 0)
            {
                return Unauthorized();
            }

            var packagesFromDb = dbContext.Packages.ToList();
            var productsFromDb = dbContext.Products.ToList();

            if (packagesFromDb == null)
            {
                return NotFound();
            }

            if (productsFromDb == null)
            {
                return NotFound();
            }

            // Send all the packages but change the product ids to the actual products
            List<PackageDto> packagesToSend = new();

            packagesFromDb.ForEach(p =>
            {
                // If the userId in this package is different from the userId in the token, skip this package
                if (p.UserId != userId)
                {
                    return;
                }

                var quantityOfProductsBoughtInPackage = p.ProductQuantities
                    .Split(",")
                    .Select(int.Parse);

                var idsOfProductsInPackage = p.ProductIds.Split(",").Select(int.Parse);

                var products = productsFromDb.Where(p => idsOfProductsInPackage.Contains(p.Id));

                List<CartProductDto> cartProducts = new();

                for (int i = 0; i < products.Count(); i++)
                {
                    CartProductDto cartProduct =
                        new()
                        {
                            Id = i,
                            QuantityBought = quantityOfProductsBoughtInPackage.ElementAt(i),
                            Product = products.ElementAt(i),
                            TotalValue = (
                                products.ElementAt(i).IndividualPrice
                                * quantityOfProductsBoughtInPackage.ElementAt(i)
                            )
                        };

                    cartProducts.Add(cartProduct);
                }

                PackageDto package =
                    new()
                    {
                        Id = p.Id,
                        UserId = p.UserId,
                        Products = cartProducts,
                        TotalProductsNumber = p.TotalProductQuantity,
                        TotalValue = p.TotalValue,
                        TotalValueWithDiscount = p.TotalValueWithDiscount,
                        PointsUsed = p.PointsUsed,
                        Date = p.Date,
                        PaymentMethod = p.PaymentMethod,
                    };

                packagesToSend.Add(package);
            });

            return Ok(packagesToSend);
        }

        /// <summary>
        /// Verifies the content of the package to see if it's valid and updates the stock of the products in the package.
        /// </summary>
        /// <param name="package">Package to be verified.</param>
        /// <returns>A boolean that indicates if the package is valid or not.</returns>
        private bool VerifyAndUpdateProductsInPackage(Package package)
        {
            // Verify content of the array of products section
            if (package.Products == null)
            {
                return false;
            }

            //_logger.LogInformation($"Package: {package.Products.Count}");

            foreach (CartProductDto product in package.Products)
            {
                //_logger.LogInformation($"Product: {product}");

                // Verify if the product exists in the package
                if (product == null)
                {
                    return false;
                }

                //_logger.LogInformation($"Quantity Bought: {product.QuantityBought}");

                // Verify if the product has stock
                if (product.QuantityBought == 0)
                {
                    return false;
                }

                // Compare the id of the product with the id of the product in the database to see if it exists
                Product productFromDb = dbContext.Products.First(p => p.Id == product.Product.Id);

                //_logger.LogInformation($"ProductDB: {productFromDb}");

                // If the product doesn't exist in the database, return a 404
                if (productFromDb == null)
                {
                    return false;
                }

                //_logger.LogInformation($"Stock: {productFromDb.Stock}");

                // Finally, verify if the product has enough stock to be sold
                if (productFromDb.Stock < product.QuantityBought)
                {
                    return false;
                }

                // Update the stock of the product in the database by
                // subtracting the stock of the product in the package from the stock of the product in the database
                productFromDb.Stock -= product.QuantityBought;

                dbContext.Products.Update(productFromDb);
            }

            return true;
        }

        /// <summary>
        /// Gets the monetization policy from the database.
        /// </summary>
        /// <returns>The monetization policy object or null if it doesn't exist.</returns>
        private MonetizationPolicy? GetMonetizationPolicy()
        {
            try
            {
                return dbContext.MonetizationPolicy.First();
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        /// <summary>
        /// Updates the user points and returns a boolean that indicates if the operation was successful or not.
        /// </summary>
        /// <param name="package">The package that was bought.</param>
        /// <param name="pointsPerDollar">The amount of points that the user will receive per dollar spent.</param>
        /// <returns>A boolean that indicates if the operation was successful or not.</returns>
        private bool UpdateUserPoints(Package package, int pointsPerDollar)
        {
            // Points Section of the User
            User user = dbContext.Users.First(u => u.Id == package.UserId);

            if (user == null)
            {
                return false;
            }
            else
            {
                // Remove points from the client
                user.CurrentPoints -= package.PointsUsed;

                // If the client has less than 0 points, set the points to 0
                // It's done in this order (subtract first and add later) to prevent the client not gaining points from a purchase
                // If they spent more points than they gained from the purchase
                if (user.CurrentPoints < 0)
                {
                    user.CurrentPoints = 0;
                }

                // Add points from the money spent on the order
                user.CurrentPoints += (int)Math.Floor(package.TotalValue * pointsPerDollar);

                // Add points to the total spent points of the client
                user.TotalSpentPoints += package.PointsUsed;

                dbContext.Users.Update(user);
            }

            return true;
        }

        /// <summary>
        /// Updates the total value earned from sales and the total products sold of a seller.
        /// </summary>
        /// <param name="totalValueWithDiscount"> The total value of the package with the discount applied -
        /// May be the same as the total value if no discount was applied
        /// or the user didn't have enough points to start discounting money.</param>
        /// <param name="totalQuantityBought"> The total quantity of products bought in the package.</param>
        /// <param name="key"> The id of the seller.</param>
        /// <returns> A boolean that indicates if the operation was successful or not.</returns>
        private bool UpdateSellerTotalEarnedMoneyAndTotalSoldProducts(
            decimal totalValueWithDiscount,
            int totalQuantityBought,
            int key
        )
        {
            _logger.LogInformation($"Reached function to update seller total earned money and total sold products");

            if (totalValueWithDiscount < 0) 
            {
                return false;
            }

            _logger.LogInformation($"Value with discount not less than 0");

            if (totalQuantityBought < 0)
            {
                return false;
            }

            _logger.LogInformation($"Quantity bought not less than 0");

            if (key < 0)
            {
                return false;
            }

            _logger.LogInformation($"Key not less than 0");

            var seller = dbContext.Sellers.First(s => s.Id == key);

            if (seller == null)
            {
                return false;
            }

            _logger.LogInformation($"Seller not null");

            seller.TotalValueEarnedFromSales += totalValueWithDiscount;
            seller.TotalProductsSold += totalQuantityBought;

            dbContext.Sellers.Update(seller);

            return true;
        }

        /// <summary>
        /// Updates the user money and returns a boolean that indicates if the operation was successful or not.
        /// </summary>
        /// <param name="package">Package that was bought.</param>
        /// <returns>A boolean that indicates if the operation was successful or not.</returns>
        private bool UpdateUserMoney(Package package)
        {
            // Money Section of the User
            User user = dbContext.Users.First(u => u.Id == package.UserId);

            if (user == null)
            {
                return false;
            }
            else
            {
                // Add the total number of stock of the products in the package to the total purchases amount of the user
                user.TotalPurchasesAmount += package.TotalProductQuantity;

                // By default, in the website, the total value with discount is the same as the total value when discount is not applied
                // but since requests can be made to this endpoint, we need to verify if the total value with discount is 0
                user.TotalPurchasesValue +=
                    package.TotalValueWithDiscount == 0
                        ? package.TotalValue
                        : package.TotalValueWithDiscount;

                dbContext.Users.Update(user);
            }

            return true;
        }

        /// <summary>
        /// Creates a seller package for each seller in the package.
        /// </summary>
        /// <param name="package"></param>
        private bool CreateSellerPackages(Package package, decimal dollarToDiscoutPer10Points)
        {
            // Seller package section - Map the products that are sold by a seller in a dictionary
            // and for each key, add the value (total products made by a specific seller) into
            // the array of a new seller package object and associate the key of the dictionary to the sellerId property
            // of the seller package object.

            // Create a dictionary to store the seller id and the total products sold by that seller
            Dictionary<int, List<CartProductDto>> sellerIdAndTotalProductsSold = new();

            // Loop through the products of the package
            foreach (CartProductDto product in package.Products)
            {
                var productFromDb = dbContext.Products.First(p => p.Id == product.Product.Id);

                if (
                    sellerIdAndTotalProductsSold.TryGetValue(
                        productFromDb.SellerId,
                        out List<CartProductDto>? value
                    )
                )
                {
                    // If the seller id already exists in the dictionary, append the product to the array of products
                    sellerIdAndTotalProductsSold[productFromDb.SellerId] = value
                        .Append(product)
                        .ToList();
                }
                else
                {
                    // If the seller id doesn't exist in the dictionary, add the seller id as a key and the product as a value
                    sellerIdAndTotalProductsSold.Add(
                        productFromDb.SellerId,
                        new List<CartProductDto> { product }
                    );
                }
            }

            _logger.LogInformation($"Reached here");

            // Loop through the dictionary and create a new seller package object for each key and value
            foreach (var keyValuePair in sellerIdAndTotalProductsSold)
            {
                var productFromDb = dbContext
                    .Products
                    .First(p => p.Id == keyValuePair.Value.First().Product.Id);

                decimal totalValue = Math.Round(
                    keyValuePair.Value.Sum(p => productFromDb.IndividualPrice),
                    2
                );
                decimal totalValueWithDiscount = totalValue;

                int totalProductQuantity = keyValuePair.Value.Sum(p => p.QuantityBought);

                // Apply the discount to the total value
                int usedPoints = (int)Math.Floor((float)package.PointsUsed / 10);

                if (usedPoints > 0)
                {
                    totalValueWithDiscount = Math.Round(
                        totalValue - (usedPoints * dollarToDiscoutPer10Points),
                        2
                    );

                    if (totalValueWithDiscount < 0)
                    {
                        totalValueWithDiscount = 0;
                    }
                } else
                {
                    totalValueWithDiscount = totalValue;
                }

                // Make a list with the ids of the products sold by this seller, if they are already exist in the list, don't add them
                List<int> listOfIdsOfProductsSoldByThisSeller = keyValuePair
                    .Value
                    .Select(p => p.Product.Id)
                    .ToList();

                List<int> listOfQuantitiesOfProductsSoldByThisSeller = keyValuePair.
                    Value.
                    Select(p => p.QuantityBought).
                    ToList();

                string idsOfProductsSoldByThisSeller = string.Join(
                    ",",
                    listOfIdsOfProductsSoldByThisSeller
                );

                string quantitiesOfProductsSoldByThisSeller = string.Join(
                    ",",
                    listOfQuantitiesOfProductsSoldByThisSeller
                );

                _logger.LogInformation($"Reached here 2");

                // Create a new seller package object
                SellerPackage sellerPackage =
                    new()
                    {
                        SellerId = keyValuePair.Key,
                        OriginalPackageId = package.Id,
                        // For each products in the array of products of the dictionary, add the stock of each product to the total product quantity property
                        TotalProductQuantity = totalProductQuantity,
                        // For each products in the array of products of the dictionary, add the individual price of each product to the total value property
                        TotalValue = totalValue,
                        Date = package.Date,
                        ClientId = package.UserId,
                        // Associate the products from the original package sold by this seller
                        // with the OriginalPackageProductsFromThisSeller property in SellerPackage
                        OriginalPackageProductsFromThisSeller = idsOfProductsSoldByThisSeller,
                        OriginalPackageProductQuantitiesFromThisSeller = quantitiesOfProductsSoldByThisSeller,
                        TotalValueWithDiscount = totalValueWithDiscount,
                    };

                _logger.LogInformation($"Reached here 3");

                if (
                    !UpdateSellerTotalEarnedMoneyAndTotalSoldProducts(
                        totalValueWithDiscount,
                        totalProductQuantity,
                        keyValuePair.Key
                    )
                )
                {
                    return false;
                }

                _logger.LogInformation($"Reached here 4");

                sellerPackage.Id = dbContext.SellerPackages.Count() + 1;
                sellerPackage.State = "Pending";

                // Add the seller package object to the database
                dbContext.SellerPackages.Add(sellerPackage);
            }

            return true;
        }

        /// <summary>
        /// After everything has been created and updated, end the register sale process by updating the package information.
        /// </summary>
        /// <param name="packageDto">Package that was bought.</param>
        /// <param name="dollarToDiscoutPer10Points">Discount applied per 10 points.</param>
        private void UpdatePackageInformation(
            SaleDto packageDto,
            decimal dollarToDiscoutPer10Points
        )
        {
            var idOfProductsInPackage = packageDto.Package.Products.Select(p => p.Product.Id);

            var quantityOfProductsInPackage = packageDto
                .Package
                .Products
                .Select(p => p.QuantityBought);

            _logger.LogInformation($"Products Quantity: {quantityOfProductsInPackage}");

            string idsOfProductsSoldByThisSeller = string.Join(",", idOfProductsInPackage);

            string quantitiesOfProductsSoldByThisSeller = string.Join(
                ",",
                quantityOfProductsInPackage
            );

            _logger.LogInformation($"{quantitiesOfProductsSoldByThisSeller}");

            // Map the id of the products in the array of products from the package to the product ids list property of the package
            packageDto.Package.ProductIds = idsOfProductsSoldByThisSeller;
            packageDto.Package.ProductQuantities = quantitiesOfProductsSoldByThisSeller;

            // For the products in this list, get their individualPrice and sum them up to get the total value of the package
            // This is done because the total value in the package may be different from the total value of the products
            // due to the client maybe being from a different country than the USA and the price of the products being converted
            // Example: The client is from Portugal and the price of the products is in dollars. If the price of the products
            // that is sent in euros, the total value of the package would be less than the registered value of the products in the database
            // due to the conversion rate between dollars and euros which isn't what we want.
            // We want the total price of the package to be the same as the total price of the products in the database.

            List<Product> productsFromDb = dbContext
                .Products
                .Where(p => idOfProductsInPackage.Contains(p.Id))
                .ToList();

            decimal totalValue = Math.Round(productsFromDb.Sum(p => p.IndividualPrice), 2);
            decimal totalValueWithDiscount = totalValue;

            // Apply the discount to the total value
            int usedPoints = (int)Math.Floor((float)packageDto.Package.PointsUsed / 10);

            if (usedPoints > 0)
            {
                totalValueWithDiscount = Math.Round(
                    totalValue - (usedPoints * dollarToDiscoutPer10Points),
                    2
                );
            }

            packageDto.Package.TotalValue = totalValue;
            packageDto.Package.TotalValueWithDiscount = totalValueWithDiscount;

            // Add the package to the database
            dbContext.Packages.Add(packageDto.Package);
        }

        private bool SendEmailToClient(Package package)
        {
            // Send email to the client with the package information
            var clientId = package.UserId;

            if (clientId < 0)
            {
                return false;
            }

            var client = dbContext.Users.First(u => u.Id == package.UserId);

            if (client == null)
            {
                return false;
            }

            var clientEmail = client.Email;

            if (clientEmail == null)
            {
                return false;
            }

            string fromEmail = "artisanimarketplace@gmail.com";
            string fromPassword = "urof vhpv xtrs lhja";

            string smtpServer = "smtp.gmail.com";
            int smtpPort = 587;

            MailMessage mail =
                new(
                    fromEmail,
                    clientEmail,
                    "Artisani - Order Confirmation",
                    $"Hello {client.Name},\n\n"
                        + $"Thank you for your purchase!\n\n"
                        + $"Here is the information about your order:\n\n"
                        + $"Cost of your purchase: {package.TotalValueWithDiscount}\n"
                        + $"Points used: {package.PointsUsed}\n"
                        + $"Remaining points: {client.CurrentPoints}\n\n"
                        + $"If you have any questions, please contact us at "
                        + $"artisanimarketplace_support@gmail.com"
                );

            SmtpClient clientSmtp = new(smtpServer, smtpPort)
            {
                Credentials = new NetworkCredential(fromEmail, fromPassword),
                EnableSsl = true
            };

            try
            {
                clientSmtp.Send(mail);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        [HttpPost("registerSale"), Authorize]
        public ActionResult<Package> RegisterPackage([FromBody] SaleDto packageDto)
        {
            // Verify if the package is null and if the package and payment method properties are null
            if (packageDto == null)
            {
                return BadRequest();
            }

            _logger.LogInformation($"Package: {packageDto.Package.Products}");

            if (packageDto.Package == null)
            {
                return BadRequest();
            }

            _logger.LogInformation($"1");

            if (packageDto.PaymentMethod == null)
            {
                return BadRequest();
            }

            _logger.LogInformation($"2");

            // Check if the package collection is valid
            if (dbContext.Packages == null)
            {
                return NotFound();
            }

            _logger.LogInformation($"3");

            // Verify content of the array of products section and update the stock of the products in the database
            if (!VerifyAndUpdateProductsInPackage(packageDto.Package))
            {
                return BadRequest();
            }

            _logger.LogInformation($"4");

            // Monetization Policy section
            MonetizationPolicy? CurrentMonetizationPolicy = GetMonetizationPolicy();
            int pointsPerDollar =
                CurrentMonetizationPolicy != null ? CurrentMonetizationPolicy.PointsPerDollar : 2;
            decimal dollarToDiscoutPer10Points = Math.Round(
                CurrentMonetizationPolicy != null
                    ? CurrentMonetizationPolicy.DollarToDiscoutPer10Points
                    : 1,
                2
            );

            // Give the package an id
            int Id = dbContext.Packages.Count() + 1;

            packageDto.Package.Id = Id;

            // Update the user points
            if (!UpdateUserPoints(packageDto.Package, pointsPerDollar))
            {
                return BadRequest();
            }

            _logger.LogInformation($"5");

            // Update the user money
            if (!UpdateUserMoney(packageDto.Package))
            {
                return BadRequest();
            }

            _logger.LogInformation($"6");

            // Create the seller package
            if (!CreateSellerPackages(packageDto.Package, dollarToDiscoutPer10Points))
            {
                return BadRequest();
            }

            // Based on the payment method, call the different services to contact the payment apps like MBWay, PayPal, etc.
            // and register the payment after the payment is confirmed

            // Note: The payment method feature is not implemented in this project due to it being a university project
            // but the data needed to implement it will still be sent through the body of the request

            // For MBWay and Credit Card, the card holder name and phone number are properties sent in the body of the package

            // Update the package information
            UpdatePackageInformation(packageDto, dollarToDiscoutPer10Points);

            _logger.LogInformation($"7");

            // Send email to the client with the package information
            if (!SendEmailToClient(packageDto.Package))
            {
                return BadRequest();
            }

            // Save all of the changes made in this function to the database
            dbContext.SaveChanges();

            // Return the package that was sent through the body
            return Ok();
        }
    }
}
