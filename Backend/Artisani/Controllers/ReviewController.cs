﻿using Artisani.Data;
using Artisani.Models;
using Artisani.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Artisani.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly ArtisaniContext dbContext;
        private readonly IUserService _userService;
        private readonly ILogger<ReviewController> _logger;

        public ReviewController(ArtisaniContext context, IUserService userService, ILogger<ReviewController> logger)
        {
            dbContext = context;
            _userService = userService;
            _logger = logger;
        }

        /// <summary>
        /// Function that returns all reviews from the database
        /// </summary>
        /// <returns>All reviews from the database</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Review>> GetReviews()
        {
            if (dbContext.Reviews == null)
            {
                return NotFound();
            }

            return Ok(dbContext.Reviews.ToList());

        }

        /// <summary>
        /// Function that returns all the reviews from a product with a specific id
        /// </summary>
        /// <param name="id">Id of the product</param>
        /// <returns>All reviews from a product with a specific id</returns>
        [HttpGet("getReviewsForProduct/{id}")]
        public IActionResult GetReviewsFromProductWithId(int id)
        {
            if (id == 0)
            {
                return BadRequest("Invalid product id");
            }

            if (dbContext == null)
            {
                return NotFound("Reviews not found");
            }

            if (dbContext.Reviews == null)
            {
                return NotFound("Reviews not found");
            }

            if (dbContext.Products == null)
            {
                return NotFound("Products with this id not found");
            }

            var productFromDb = dbContext.Products.Find(id);

            if (productFromDb == null)
            {
                return NotFound("Product with this id not found");
            }

            var reviews = dbContext.Reviews.Where(r => r.ProductId == id).ToList();

            if (reviews == null)
            {
                return NotFound("Reviews not found");
            }

            return Ok(reviews);
        }   

        /// <summary>
        /// Function to create a review
        /// </summary>
        /// <param name="review"> Review object that contains all the information about the review</param>
        /// <returns>A copy of the review that was created</returns>
        [HttpPost]
        public IActionResult CreateReview([FromBody] Review review)
        {
            var clientId = _userService.GetIdentityId();

            _logger.LogInformation($"Review is {review}");

            // User is not logged in
            if (clientId < 0)
            {
                return Unauthorized();
            }

            //Checks if all review camps are null
            if (review == null)
            {
                return BadRequest("Invalid review data");
            }

            var reviewProductId = review.ProductId;

            //Checks if product exists
            if (reviewProductId == 0)
            {
                return BadRequest("Invalid product id");
            }

            var productFromDb = dbContext.Products.Find(reviewProductId);

            if (productFromDb == null)
            {
                return NotFound("Product not found");
            }

            var userFromDb = dbContext.Users.Find(clientId);

            if (userFromDb == null) {
                return NotFound("User not found");
            }

            review.Id = dbContext.Reviews.Count() + 1;
            review.ClientId = clientId;

            // If everything is ok, add review to database
            dbContext.Reviews.Add(review);
            dbContext.SaveChanges();

            return Ok(review);
        }

        [HttpPut("{id}"), Authorize]
        public IActionResult EditReview(int id, [FromBody] Review updatedReview)
        {
            var clientId = _userService.GetIdentityId();

            if (clientId < 0) 
            {
                return Unauthorized();
            }
            
            // Verify if the review exists
            var existingReview = dbContext.Reviews.Find(id);

            // If review doesnt exist return not found
            if (existingReview == null)
            {
                return NotFound();
            }

            // If the client id is different from the one that is trying to update the review return forbidden
            if (clientId != existingReview.ClientId || clientId != updatedReview.ClientId)
            {
                return Forbid();
            }

            // If the owner of the review isnt the same as the one that is trying to update he shouldnt be able to change
            if (existingReview.ClientId != updatedReview.ClientId)
            {
                return Forbid(); // Return 403, no authorization
            }

            // Update the review
            existingReview.Rating = updatedReview.Rating;
            existingReview.Comment = updatedReview.Comment;

            dbContext.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteReview(int id, int clientId)
        {
            // Find review based on id
            var existingReview =  dbContext.Reviews.Find(id);

            // Checks if review exist
            if (existingReview == null)
            {
                return NotFound(); 
            }

            // Checks if clientId is the same as the owner of the review
            if (existingReview.ClientId != clientId)
            {
                return Forbid();
            }

            // Delete the review and update the database
            dbContext.Reviews.Remove(existingReview);
            dbContext.SaveChanges();

            return NoContent();
        }

    }
}