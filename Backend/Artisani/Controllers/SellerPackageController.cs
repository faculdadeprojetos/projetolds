﻿using System.Net;
using Artisani.Data;
using Artisani.Models;
using Artisani.Models.Dtos;
using Artisani.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Any;

namespace Artisani.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerPackageController : Controller
    {
        private readonly ArtisaniContext dbContext;
        private readonly IUserService _userService;
        private readonly ILogger<SellerPackageController> _logger;

        public SellerPackageController(
            ArtisaniContext dbContext,
            IUserService userService,
            ILogger<SellerPackageController> logger
        )
        {
            this.dbContext = dbContext;
            _userService = userService;
            _logger = logger;
        }

        /// <summary>
        /// Function that returns all the seller packages
        /// </summary>
        /// <returns>All the seller packages</returns>
        [HttpGet]
        public IActionResult GetSellerPackages()
        {
            if (dbContext == null)
            {
                return NotFound();
            }

            if (dbContext.SellerPackages == null)
            {
                return NotFound();
            }

            if (dbContext.Products == null)
            {
                return NotFound();
            }

            // Get sellerId from the token and get only the seller packages that have the same sellerId
            // int sellerId = _userService.GetIdentityId();

            var sellerPackagesFromDb = dbContext.SellerPackages.ToList();
            var productsFromDb = dbContext.Products.ToList();

            if (sellerPackagesFromDb == null)
            {
                return NotFound();
            }

            if (productsFromDb == null)
            {
                return NotFound();
            }

            // Send all the seller packages but change the product ids to the actual products
            List<SellerPackageDto> sellerPackagesToSend = new();

            sellerPackagesFromDb.ForEach(p =>
            {
                var idsOfProductsSold = p.OriginalPackageProductsFromThisSeller
                    .Split(",")
                    .Select(int.Parse);

                var quantitiesOfProductsSold = p.OriginalPackageProductQuantitiesFromThisSeller
                    .Split(",")
                    .Select(int.Parse);

                var products = productsFromDb.Where(p => idsOfProductsSold.Contains(p.Id));

                List<CartProductDto> cartProductDtos = new();

                for (int i = 0; i < products.Count(); i++)
                {
                    CartProductDto cartProductDto =
                        new()
                        {
                            Id = i,
                            QuantityBought = quantitiesOfProductsSold.ElementAt(i),
                            Product = products.ElementAt(i),
                            TotalValue = (
                                products.ElementAt(i).IndividualPrice
                                * quantitiesOfProductsSold.ElementAt(i)
                            )
                        };

                    cartProductDtos.Add(cartProductDto);
                }

                SellerPackageDto sellerPackage =
                    new()
                    {
                        Id = p.Id,
                        TotalProductQuantity = p.TotalProductQuantity,
                        TotalValue = p.TotalValue,
                        TotalValueWithDiscount = p.TotalValueWithDiscount,
                        Date = p.Date,
                        ClientId = p.ClientId,
                        State = p.State,
                        Products = cartProductDtos
                    };

                sellerPackagesToSend.Add(sellerPackage);
            });

            return Ok(sellerPackagesToSend);
        }

        /// <summary>
        /// Function that returns all the seller packages from a specific seller
        /// </summary>
        /// <returns>All the seller packages from a specific seller</returns>
        [HttpGet("getSellerPackageFromLoggedInSeller"), Authorize]
        public IActionResult GetSellerPackagesFromLoggedInSeller()
        {
            if (dbContext == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Packages == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Products == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Get userId from the token and get only the packages that have the same userId
            int sellerId = _userService.GetIdentityId();

            if (sellerId < 0)
            {
                return Unauthorized();
            }

            var sellerPackagesFromDb = dbContext.SellerPackages.ToList();
            var productsFromDb = dbContext.Products.ToList();

            if (sellerPackagesFromDb == null)
            {
                return NotFound();
            }

            if (productsFromDb == null)
            {
                return NotFound();
            }

            // Send all the packages but change the product ids to the actual products
            List<SellerPackageDto> packagesToSend = new();

            sellerPackagesFromDb.ForEach(p =>
            {
                // If the userId in this package is different from the userId in the token, skip this package
                if (p.SellerId != sellerId)
                {
                    return;
                }

                var quantityOfProductsBoughtInPackage =
                    p.OriginalPackageProductQuantitiesFromThisSeller.Split(",").Select(int.Parse);

                var idsOfProductsInPackage = p.OriginalPackageProductsFromThisSeller
                    .Split(",")
                    .Select(int.Parse);

                var products = productsFromDb.Where(p => idsOfProductsInPackage.Contains(p.Id));

                List<CartProductDto> cartProducts = new();

                for (int i = 0; i < products.Count(); i++)
                {
                    CartProductDto cartProduct =
                        new()
                        {
                            Id = i,
                            QuantityBought = quantityOfProductsBoughtInPackage.ElementAt(i),
                            Product = products.ElementAt(i),
                            TotalValue = (
                                products.ElementAt(i).IndividualPrice
                                * quantityOfProductsBoughtInPackage.ElementAt(i)
                            )
                        };

                    cartProducts.Add(cartProduct);
                }

                SellerPackageDto sellerPackage =
                    new()
                    {
                        Id = p.Id,
                        TotalProductQuantity = p.TotalProductQuantity,
                        TotalValue = p.TotalValue,
                        TotalValueWithDiscount = p.TotalValueWithDiscount,
                        Date = p.Date,
                        ClientId = p.ClientId,
                        State = p.State,
                        Products = cartProducts
                    };

                packagesToSend.Add(sellerPackage);
            });

            return Ok(packagesToSend);
        }

        /// <summary>
        /// Function that returns a specific seller package from a specific seller
        /// </summary>
        /// <param name="id">Id of the seller package</param>
        /// <returns>A specific seller package from a specific seller</returns>
        [HttpGet("getSellerPackageFromLoggedInSeller/{id}"), Authorize]
        public IActionResult GetSellerPackageFromLoggedInUser(int id)
        {
            if (dbContext == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Packages == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.Products == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Get userId from the token and get only the packages that have the same userId
            int sellerId = _userService.GetIdentityId();

            if (sellerId < 0)
            {
                return Unauthorized();
            }

            var sellerPackageFromDb = dbContext
                .SellerPackages
                .Where(p => p.Id == id)
                .FirstOrDefault();
            var productsFromDb = dbContext.Products.ToList();

            if (sellerPackageFromDb == null)
            {
                return NotFound();
            }

            if (productsFromDb == null)
            {
                return NotFound();
            }

            // Send all the packages but change the product ids to the actual products
            SellerPackageDto packageToSend = new();

            // If the userId in this package is different from the userId in the token, skip this package
            if (sellerPackageFromDb.SellerId != sellerId)
            {
                return Unauthorized();
            }

            var quantityOfProductsBoughtInPackage = sellerPackageFromDb
                .OriginalPackageProductQuantitiesFromThisSeller
                .Split(",")
                .Select(int.Parse);

            var idsOfProductsInPackage = sellerPackageFromDb
                .OriginalPackageProductsFromThisSeller
                .Split(",")
                .Select(int.Parse);

            var products = productsFromDb.Where(p => idsOfProductsInPackage.Contains(p.Id));

            List<CartProductDto> cartProducts = new();

            for (int i = 0; i < products.Count(); i++)
            {
                CartProductDto cartProduct =
                    new()
                    {
                        Id = i,
                        QuantityBought = quantityOfProductsBoughtInPackage.ElementAt(i),
                        Product = products.ElementAt(i),
                        TotalValue = (
                            products.ElementAt(i).IndividualPrice
                            * quantityOfProductsBoughtInPackage.ElementAt(i)
                        )
                    };

                cartProducts.Add(cartProduct);
            }

            packageToSend =
                new()
                {
                    Id = sellerPackageFromDb.Id,
                    TotalProductQuantity = sellerPackageFromDb.TotalProductQuantity,
                    TotalValue = sellerPackageFromDb.TotalValue,
                    TotalValueWithDiscount = sellerPackageFromDb.TotalValueWithDiscount,
                    Date = sellerPackageFromDb.Date,
                    ClientId = sellerPackageFromDb.ClientId,
                    Products = cartProducts,
                    State = sellerPackageFromDb.State
                };

            return Ok(packageToSend);
        }

        /// <summary>
        /// Function that updates the state of a seller package
        /// </summary>
        /// <param name="id">Id of the seller package</param>
        /// <param name="state">New state of the seller package</param>
        /// <returns>Whether the update was successful or not</returns>
        [HttpPut("updateSellerPackageState/{id}"), Authorize]
        public IActionResult UpdateSellerPackageState(int id, [FromBody] string state)
        {
            if (dbContext == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (dbContext.SellerPackages == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var sellerPackageFromDb = dbContext
                .SellerPackages
                .Where(p => p.Id == id)
                .FirstOrDefault();

            if (sellerPackageFromDb == null)
            {
                return NotFound();
            }

            if (state == null)
            {
                return BadRequest();
            }

            if (state != "Pendente" && state != "Confirmada" && state != "Recusada" && state != "Enviada" && state != "Em Processamento")
            {
                return BadRequest();
            }

            sellerPackageFromDb.State = state;

            dbContext.SaveChanges();

            return NoContent();
        }
    }
}
