﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Artisani.Data;
using Artisani.Models;
using Artisani.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;



namespace Artisani.Controllers
{
    /// <summary>
    /// Controller for managing sellers in the API.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        // Database context for interacting with the seller data
        private readonly ArtisaniContext dbContext;
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;
        private readonly ILogger<SellerController> _logger;


        /// <summary>
        /// Constructor for SellerController, initializes the controller with a database context.
        /// </summary>
        /// <param name="context">The database context for sellers.</param>
        public SellerController(ArtisaniContext context, IConfiguration configuration, IUserService userService, ILogger<SellerController> logger)
        {
            dbContext = context;
            _configuration = configuration;
            _userService = userService;
            _logger = logger;
        }

        /// <summary>
        /// Function that returns a token for a seller if the seller's data is valid.
        /// </summary>
        /// <param name="seller">Seller with only email and password fields filled.</param>
        /// <returns>A jwt token for the seller if the seller's data is valid,
        /// or a NotFound response if the collection of sellers is null,
        /// or a BadRequest response if the sellers's data is invalid,
        /// or an Unauthorized response if the sellers's password is incorrect
        /// or if the token could not be generated.</returns>
        [HttpPost("login")]
        public IActionResult ValidateIdentity(SellerLogin seller)
        {
            _logger.LogInformation("Validating seller identity");

            if (dbContext.Sellers == null)
            {
                return NotFound();
            }

            // Verify if the seller's data is valid
            if (seller.Email == null || seller.Password == null)
            {
                return BadRequest();
            }

            var sellerWithSameEmail = dbContext.Sellers.SingleOrDefault(s => s.Email == seller.Email);

            if (sellerWithSameEmail == null)
            {
                return NotFound();
            }

            // Use BCrypt to verify if the hashed password in the database matches the password received in the request after it has been hashed
            if (!BCrypt.Net.BCrypt.Verify(seller.Password, sellerWithSameEmail.Password))
            {
                return Unauthorized();
            }

            // Create a jwt token for this user and return it
            var token = _userService.GenerateJwtToken(sellerWithSameEmail.Id, sellerWithSameEmail.Email);

            if (token == null)
            {
                return Unauthorized();
            }

            // Change this to return a jwt token instead after implementing it
            return Ok(new { token });

        }

        /// <summary>
        /// Retrieves a list of all sellers.
        /// </summary>
        /// <returns>An ActionResult containing a list of sellers if available, NotFound otherwise.</returns>
        //GET: api/sellers
        [HttpGet]
        public IActionResult GetSellers()
        {
            if (dbContext.Sellers == null)
            {
                return NotFound();
            }

            return Ok(dbContext.Sellers.ToList());
        }


        /// <summary>
        /// Retrieves a specific seller by ID.
        /// </summary>
        /// <param name="id">The ID of the seller to retrieve.</param>
        /// <returns>An ActionResult containing the requested seller if found, NotFound otherwise.</returns>
        ///GET: api/sellers/1
        [HttpGet("getSeller"), Authorize]
        public ActionResult<Seller> GetSeller ()
        {
            _logger.LogInformation("Getting seller");

            if (dbContext.Sellers == null)
            {
                return NotFound();
            }

            var sellerId = _userService.GetIdentityId();

            if (sellerId < 0)
            {
                return NotFound();
            }

            var sellerFromDb = dbContext.Sellers.SingleOrDefault(s => s.Id == sellerId);

            if (sellerFromDb == null)
            {
                return NotFound();
            }

            return Ok(sellerFromDb);
        }

        /// <summary>
        /// Creates a new seller.
        /// </summary>
        /// <param name="seller">The Seller object representing the new seller.</param>
        /// <returns>
        /// ActionResult with the newly created seller if successful, BadRequest if the seller already exists,
        /// or NotFound if the input seller is null.
        /// </returns>
        ///POST: api/sellers
        [HttpPost("register")]
        public ActionResult<Seller> CreateSeller(Seller seller)
        {
            var sellers = dbContext.Sellers;

            if (sellers == null)
            {
                return NotFound();
            }

            for (int i = 0; i < sellers.ToList().Count; i++)
            {
                if (sellers.ToList()[i].Id == seller.Id)
                {
                    return BadRequest("This seller already exists");
                }

                if (sellers.ToList()[i].Email == seller.Email)
                {
                    return BadRequest("There is already a seller registered with this email");
                }

                if (sellers.ToList()[i].Nif == seller.Nif)
                {
                    return BadRequest("There is already a seller with the same NIF");
                }

                if (sellers.ToList()[i].Nib == seller.Nib)
                {
                    return BadRequest("There is already a seller with the same NIB");
                }
            }

            seller.Id = sellers.ToList().Count + 1;

            seller.Password = BCrypt.Net.BCrypt.HashPassword(seller.Password);

            dbContext.Sellers.Add(seller);
            dbContext.SaveChanges();

            return CreatedAtAction(nameof(CreateSeller), new { id = seller.Id }, seller);
        }


        /// <summary>
        /// Updates an existing seller by ID.
        /// </summary>
        /// <param name="id">The ID of the seller to update.</param>
        /// <param name="seller">The updated Seller object.</param>
        /// <returns>
        /// IActionResult indicating success (NoContent) or BadRequest if the provided seller ID does not match the route parameter ID.
        /// </returns>
        //PUT: api/sellers/1
        [HttpPut("{id}")]
        public IActionResult UpdateSeller(int id, Seller seller)
        {
            // Check if the user received in the request body is null or if userId is different from user.Id
            if (seller == null || id != seller.Id)
            {
                // If the condition is true, return a 403 - Forbid response
                return Forbid();
            }

            _logger.LogInformation($"Updating seller: {seller}");

            // Check if the user with the specified userId exists in the database
            var sellerExists = dbContext.Sellers.FirstOrDefault(s => s.Id == id);

            if (sellerExists == null)
            {
                // If the user does not exist, return a 404 - NotFound response indicating that the resource was not found
                return NotFound();
            }

            // Update the existing user with the values from the user received in the request
            sellerExists.Address = seller.Address;
            sellerExists.Contact = seller.Contact;
            sellerExists.Country = seller.Country;
            sellerExists.Email = seller.Email;
            sellerExists.Name = seller.Name;
            sellerExists.Nif = seller.Nif;
            sellerExists.PostalCode = seller.PostalCode;

            // Change the user's password if the password received in the request is different from the one in the database
            seller.Password = BCrypt.Net.BCrypt.HashPassword(seller.Password);

            if (seller.Password != null && seller.Password != sellerExists.Password)
            {
                sellerExists.Password = seller.Password;
            }
            else
            {
                sellerExists.Password = sellerExists.Password;
            }

            // Save the changes to the database
            dbContext.SaveChanges();

            // Return a 204 - NoContent response indicating that the operation was successful, but there is no content to return
            return NoContent();
        }



        /// <summary>
        /// Deletes a seller by ID.
        /// </summary>
        /// <param name="id">The ID of the seller to delete.</param>
        /// <returns>
        /// IActionResult indicating success (NoContent) or NotFound if the seller is not found in the database.
        /// </returns>
        //DELETE: api/sellers/1
        [HttpDelete("{id}")]
        public IActionResult DeleteSeller(int id)
        {
            if(dbContext.Sellers == null)
            {
                return NotFound();
            }

            var seller = dbContext.Sellers.SingleOrDefault(s => s.Id == id);

            if ( seller == null)
            {
                return NotFound();
            }

            dbContext.Sellers.Remove(seller);
            dbContext.SaveChanges();
            return NoContent();
        }
    }
}
