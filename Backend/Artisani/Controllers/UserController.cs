﻿using Artisani.Data;
using Artisani.Models;
using Artisani.Models.Dtos;
using Artisani.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace Artisani.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ArtisaniContext dbContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        private static readonly TimeSpan _tokenLifeTime = TimeSpan.FromHours(3);

        public UserController(ArtisaniContext context, IConfiguration configuration, ILogger<UserController> logger, IUserService userService)
        {
            dbContext = context;
            _configuration = configuration;
            _logger = logger;
            _userService = userService;
        }

        /// <summary>
        /// Function that verifies if an email is valid.
        /// </summary>
        /// <param name="email">Email to be verified.</param>
        /// <returns>Whether the email is valid or not.</returns>
        private static bool IsValidEmail(string email)
        {
            if (email == null)
            {
                return false;
            }

            if (email == "")
            {
                return false;
            }

            try
            {
                var addr = new System.Net.Mail.MailAddress(email);

                if (addr.Address == null)
                {
                    return false;
                }

                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Function that returns all the users in the database.
        /// </summary>
        /// <returns>A list of all the users in the database.</returns>
        [HttpGet]
        public ActionResult<IEnumerable<User>> GetUsers()
        {
            if (dbContext.Users == null)
            {
                return NotFound();
            }

            return Ok(dbContext.Users.ToList());
        }

        /// <summary>
        /// Function that returns a token for a user if the user's data is valid.
        /// </summary>
        /// <param name="user">User with only email and password fields filled.</param>
        /// <returns>A jwt token for the user if the user's data is valid,
        /// or a NotFound response if the collection of users is null,
        /// or a BadRequest response if the user's data is invalid,
        /// or an Unauthorized response if the user's password is incorrect
        /// or if the token could not be generated.</returns>
        [HttpPost("login")]
        public IActionResult ValidateIdentity(UserLogin user)
        {
            if (dbContext.Users == null)
            {
                return NotFound();
            }

            // Verify if the user's data is valid
            if (user.Email == null || user.Password == null)
            {
                return BadRequest();
            }

            var userWithSameEmail = dbContext.Users.SingleOrDefault(u => u.Email == user.Email);

            if (userWithSameEmail == null)
            {
                return NotFound();
            }

            // Use BCrypt to verify if the hashed password in the database matches the password received in the request after it has been hashed
            if (!BCrypt.Net.BCrypt.Verify(user.Password, userWithSameEmail.Password))
            {
                return Unauthorized();
            }

            // Create a jwt token for this user and return it
            var token = _userService.GenerateJwtToken(userWithSameEmail.Id, userWithSameEmail.Email);

            if (token == null)
            {
                return Unauthorized();
            }

            // Change this to return a jwt token instead after implementing it
            return Ok(new { token });

        }

        /// <summary>
        /// Registers a new user in the database.
        /// </summary>
        /// <param name="user">User to be registered.</param>
        /// <returns>A copy of the user that was registered.</returns>
        [HttpPost("register")]
        public IActionResult Register([FromBody] User user)
        {
            if (dbContext.Users == null)
            {
                return NotFound();
            }

            // Verify if the user's data is valid
            if (user.Name == null || user.Country == null || user.Address == null || user.PostalCode == null || user.Email == null || user.Password == null)
            {
                return BadRequest();
            }

            // Verify is the previous data instead has default values --> ""
            if (user.Name == "" || user.Country == "" || user.Address == "" || user.PostalCode == "" || user.Email == "" || user.Password == "")
            {
                return BadRequest();
            }

            // Verify if the user's email is already in use
            var userWithSameEmail = dbContext.Users.SingleOrDefault(u => u.Email == user.Email);

            if (userWithSameEmail != null)
            {
                return Conflict();
            }

            // Set the user's id to the number of users in the database + 1
            user.Id = dbContext.Users.Count() + 1;

            // Encrypt the user's password
            // with a hashing function provided by Bcrypt
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);

            dbContext.Users.Add(user);
            dbContext.SaveChanges();

            return CreatedAtAction(nameof(GetUsers), new { id = user.Id }, user);
        }

        /// <summary>
        /// Function that sends an email to the user with the email received in the request body.
        /// </summary>
        /// <param name="email">Email of the user to send the email to.</param>
        /// <returns>Whether the operation was successful or not.</returns>
        [HttpPost("multipleFailedLoginAttemptsSendEmail")]
        public IActionResult MultipleFailedLoginAttemptsSendEmail([FromBody] EmailDto email)
        {
            _logger.LogInformation($"Sending email to user with email {email}");

            if (dbContext.Users == null)
            {
                return NotFound();
            }

            if (email == null)
            {
                return BadRequest();
            }

            if (email.Email == null)
            {
                return BadRequest();
            }

            if (!IsValidEmail(email.Email))
            {
                return BadRequest();
            }

            // Verify if the user's email is already in use
            var userWithSameEmail = dbContext.Users.SingleOrDefault(u => u.Email == email.Email);

            if (userWithSameEmail == null)
            {
                return NotFound();
            }

            // Send email to user with a link to reset password
            string fromEmail = "artisanimarketplace@gmail.com";
            string fromPassword = "urof vhpv xtrs lhja";

            string smtpServer = "smtp.gmail.com";
            int smtpPort = 587;

            MailMessage mail =
                new(
                    fromEmail,
                    userWithSameEmail.Email,
                    "Artisani - Reset Password",
                    "Hello, " + userWithSameEmail.Name + "! \n\n" +
                    "We have detected multiple failed login attempts on your account. \n" +
                    "If you don't remember your password and want to reset it, please click on the following link: \n" +
                    "https://localhost:4200/client/resetPassword \n\n" +
                    "If you didn't request a password reset, please ignore this email. \n\n" +
                    "Best regards, \n" +
                    "Artisani Marketplace"
                );

            SmtpClient clientSmtp = new(smtpServer, smtpPort)
            {
                Credentials = new NetworkCredential(fromEmail, fromPassword),
                EnableSsl = true
            };

            try
            {
                clientSmtp.Send(mail);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Function that returns a user with the id that was received in the header of the GET request.
        /// </summary>
        /// <returns>A user with the id that was received in the header of the GET request.</returns>
        [HttpGet("getUser"), Authorize]
        public IActionResult GetUser()
        {
            int userId = _userService.GetIdentityId();

            // Verify if the collection of users is null
            if (dbContext.Users == null)
            {
                return NotFound();
            }

            var user = dbContext.Users.FirstOrDefault(u => u.Id == userId);

            // Verify if the user exists
            if (user == null)
            {
                return NotFound();
            }

            // Return the user if it exists
            return Ok(user);

        }

        /// <summary>
        /// Function that edits a user in the database.
        /// </summary>
        /// <param name="user">The new user info of the user to be edited.</param>
        /// <param name="userId">The id of the user to be edited.</param>
        /// <returns>A NoContent response if the user was edited successfully, 
        /// a NotFound response if the user was not found in the database, 
        /// or a Forbid response if the user received in the request body is null 
        /// or if userId is different from id property of the user.</returns>
        [HttpPut("{id}")]
        public IActionResult EditUser(int id, [FromBody] User user)
        {
            // Check if the user received in the request body is null or if userId is different from user.Id
            if (user == null || id != user.Id)
            {
                // If the condition is true, return a 403 - Forbid response
                return Forbid();
            }

            // Check if the user with the specified userId exists in the database
            var userExists = dbContext.Users.FirstOrDefault(u => u.Id == id);

            if (userExists == null)
            {
                // If the user does not exist, return a 404 - NotFound response indicating that the resource was not found
                return NotFound();
            }

            // Update the existing user with the values from the user received in the request
            userExists.Address = user.Address;
            userExists.Contact = user.Contact;
            userExists.Country = user.Country;
            userExists.Email = user.Email;
            userExists.Name = user.Name;
            userExists.Nif = user.Nif;
            userExists.PostalCode = user.PostalCode;

            // Change the user's password if the password received in the request is different from the one in the database
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            
            if (user.Password != null && user.Password != userExists.Password)
            {
                userExists.Password = user.Password;
            } 
            else
            {
                userExists.Password = userExists.Password;
            }

            // Save the changes to the database
            dbContext.SaveChanges();

            // Return a 204 - NoContent response indicating that the operation was successful, but there is no content to return
            return NoContent();
        }

        /// <summary>
        /// Deletes a user in the database.
        /// </summary>
        /// <param name="id">Id of the user to be deleted.</param>
        /// <returns>
        /// Returns NoContent if the user was deleted successfully, 
        /// NotFound if the user was not found in the database or if the collection of users is null.
        /// </returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            if (dbContext.Users == null)
            {
                return NotFound();
            }

            var user = dbContext.Users.SingleOrDefault(u => u.Id == id);

            if (user == null)
            {
                return NotFound();
            }

            dbContext.Users.Remove(user);
            dbContext.SaveChanges();

            return NoContent();
        }
    }
}