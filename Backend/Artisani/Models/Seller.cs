﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Numerics;
using ThirdParty.Json.LitJson;

namespace Artisani.Models
{
    /// <summary>
    /// The seller model.
    /// </summary>
    public class Seller
    {
        /// <summary>
        /// The unique identifier for the seller.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the seller.
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// The seller's contact number.
        /// </summary>
        public int Contact { get; set; }

        /// <summary>
        /// The seller's NIF.
        /// </summary>
        public int Nif { get; set; }

        /// <summary>
        /// The seller's NIB.
        /// </summary>
        public int Nib { get; set; }

        /// <summary>
        /// The seller's country.
        /// </summary>
        public string Country { get; set; } = null!;

        /// <summary>
        /// The seller's address - street, door number
        /// </summary>
        public string Address { get; set; } = null!;

        /// <summary>
        /// The seller's city's postal code.
        /// </summary>
        public string PostalCode { get; set; } = null!;

        /// <summary>
        /// The seller's email address.
        /// </summary>
        public string Email { get; set; } = null!;

        /// <summary>
        /// The seller's password.
        /// </summary>
        public string? Password { get; set; } = null!;

        /// <summary>
        /// The total number of products sold by the seller.
        /// </summary>
        public int TotalProductsSold { get; set; } = 0;

        /// <summary>
        /// The total value of the products sold by the seller.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal TotalValueEarnedFromSales { get; set; } = 0;
    }
}
