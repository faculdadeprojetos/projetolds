﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Artisani.Models
{
    /// <summary>
    /// The user model.
    /// </summary>
    public class User
    {
        /// <summary>
        /// The unique identifier for the user.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the user.
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// The user's contact number.
        /// </summary>
        public int Contact { get; set; }

        /// <summary>
        /// The user's NIF.
        /// </summary>
        public int Nif { get; set; }

        /// <summary>
        /// The user's country.
        /// </summary>
        public string Country { get; set; } = null!;

        /// <summary>
        /// The user's address - street, door number
        /// </summary>
        public string Address { get; set; } = null!;

        /// <summary>
        /// The user's city's postal code.
        /// </summary>
        public string PostalCode { get; set; } = null!;

        /// <summary>
        /// The user's email address.
        /// </summary>
        public string Email { get; set; } = null!;

        /// <summary>
        /// The user's password.
        /// </summary>
        public string? Password { get; set; } = null!;

        /// <summary>
        /// The total number of products purchased by the user.
        /// </summary>
        public int TotalPurchasesAmount { get; set; } = 0;

        /// <summary>
        /// The total value of products purchased by the user.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal TotalPurchasesValue { get; set; } = 0;

        /// <summary>
        /// The current number of points the user has - Relates to the loyalty program.
        /// </summary>
        public int CurrentPoints { get; set; } = 0;

        /// <summary>
        /// The total number of points the user has spent - Relates to the loyalty program.
        /// </summary>
        public int TotalSpentPoints { get; set; } = 0;

        /// <summary>
        /// The user's preferred currency
        /// By default, it's set to USD since currency API supports USD as a base currency.
        /// More info on openexchangerates.org - https://openexchangerates.org/ --> Free Plan
        /// </summary>
        public string PreferredCurrency { get; set; } = "USD";
    }
}
