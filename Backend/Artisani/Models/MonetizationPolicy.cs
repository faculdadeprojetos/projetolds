﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Artisani.Models
{
    public class MonetizationPolicy
    {
        /// <summary>
        /// The unique identifier for the monetization policy.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Amount of dollars that can be saved per 10 points.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal DollarToDiscoutPer10Points { get; set; }

        /// <summary>
        /// Amount of points that can be earned per dollar spent in the store.
        /// </summary>
        public int PointsPerDollar { get; set; }
    }
}
