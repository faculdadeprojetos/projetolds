﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Artisani.Models
{
    public class Product
    {
        /// <summary>
        /// Unique identifier for the product.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The total number of products available to be purchased of this type of product.
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// The name of the product.
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// The category of the product.
        /// </summary>
        public string Category { get; set; } = null!;

        /// <summary>
        /// The price of each individual product.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal IndividualPrice { get; set; } 

        /// <summary>
        /// The height of each individual product.
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// The width of each individual product.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// The weight of each individual product.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal Weight { get; set; }

        /// <summary>
        /// The material the product is made of - ceramic, wood, etc.
        /// </summary>
        public string TypeOfMaterialMade { get; set; } = null!;

        /// <summary>
        /// The instructions for washing the product.
        /// </summary>
        public string WashingInstructions { get; set; } = null!;

        /// <summary>
        /// The id of the seller that registered the product in the database.
        /// It is a foreign key.
        /// </summary>
        public int SellerId { get; set; }

        /// <summary>
        /// The id of the package that contains this product - Should be null if the product is not in a package.
        /// For example when it's first registered in the database.
        /// </summary>
        public int? PackageId { get; set; }

        /// <summary>
        /// The id of the seller package that contains this product - Should be null if the product is not in a seller package.
        /// For example when it's first registered in the database.
        /// </summary>
        public int? SellerPackageId { get; set; }

        /// <summary>
        /// The path to the image of the product - Sellers can upload images of their products
        /// for the users to see in the online store.
        /// </summary>
        public String ImagePath { get; set; } = null!;
    }
}
