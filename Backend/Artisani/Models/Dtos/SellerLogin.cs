﻿using System;
namespace Artisani.Models
{
	public class SellerLogin
	{
        /// <summary>
        /// Email address of the seller that is trying to login.
        /// </summary>
        public string Email { get; set; } = null!;

        /// <summary>
        /// Password of the seller that is trying to login.
        /// </summary>
        public string Password { get; set; } = null!;
    }
}

