﻿namespace Artisani.Models.Dtos
{
    public class SellerPackageDto
    {
        public int Id { get; set; }
        public int TotalProductQuantity { get; set; }
        public decimal TotalValue { get; set; }
        public decimal TotalValueWithDiscount { get; set; }
        public DateTime Date { get; set; }
        public int ClientId { get; set; }
        public List<CartProductDto> Products { get; set; } = null!;

        public string State { get; set; } = null!;
    }
}
