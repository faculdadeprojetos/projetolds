﻿namespace Artisani.Models.Dtos
{
    public class CartProductDto
    {
        /// <summary>
        /// Sent from the client to the server so we should keep it the property the same as this is a DTO.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Number of products of this type that the user wants to buy.
        /// </summary>
        public int QuantityBought { get; set; }

        /// <summary>
        /// Id of the product that the user wants to buy.
        /// </summary>
        public Product Product { get; set; } = null!;

        /// <summary>
        /// Total value of all the products of this type that the user wants to buy.
        /// </summary>
        public decimal TotalValue { get; set; }
    }
}
