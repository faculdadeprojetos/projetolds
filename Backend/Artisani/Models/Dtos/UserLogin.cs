﻿namespace Artisani.Models.Dtos
{
    public class UserLogin
    {
        /// <summary>
        /// Email address of the user that is trying to login.
        /// </summary>
        public string Email { get; set; } = null!;

        /// <summary>
        /// Password of the user that is trying to login.
        /// </summary>
        public string Password { get; set; } = null!;
    }
}
