﻿namespace Artisani.Models.Dtos
{
    public class PackageDto
    {
        public int Id { get; set; }

        public List<CartProductDto> Products { get; set; } = null!;

        public int TotalProductsNumber { get; set; }

        public decimal TotalValue { get; set; }

        public decimal TotalValueWithDiscount { get; set; }

        public DateTime Date { get; set; }

        public int UserId { get; set; }

        public int PointsUsed { get; set; }

        public string PaymentMethod { get; set; } = string.Empty;
    }
}
