﻿namespace Artisani.Models.Dtos
{
    public class SaleDto
    {
        /// <summary>
        /// Package sent from the client to the server.
        /// </summary>
        public Package Package { get; set; } = null!;

        /// <summary>
        /// Payment method information sent from the client to the server.
        /// </summary>
        public PaymentMethodDto PaymentMethod { get; set; } = null!;
    }
}
