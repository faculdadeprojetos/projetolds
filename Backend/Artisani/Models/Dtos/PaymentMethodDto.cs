﻿namespace Artisani.Models.Dtos
{
    public class PaymentMethodDto
    {
        /// <summary>
        /// Security code of the credit card.
        /// </summary>
        public string SecurityCode { get; set; } = null!;

        /// <summary>
        /// Credit card number.
        /// </summary>
        public string CreditCardNumber { get; set; } = null!;

        /// <summary>
        /// Expiration date of the credit card - month.
        /// </summary>
        public int ExpirationDateMonth { get; set; }

        /// <summary>
        /// Expiration date of the credit card - year.
        /// </summary>
        public int ExpirationDateYear { get; set; }
    }
}
