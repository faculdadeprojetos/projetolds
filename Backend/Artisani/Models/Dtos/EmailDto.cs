﻿namespace Artisani.Models.Dtos
{
    public class EmailDto
    {
        /// <summary>
        /// Email address
        /// </summary>
        public string? Email { get; set; }
    }
}
