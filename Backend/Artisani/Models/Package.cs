﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Artisani.Models.Dtos;

namespace Artisani.Models
{
    public class Package
    {
        /// <summary>
        /// The unique identifier for the package.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The total products in the package.
        /// </summary>
        public List<CartProductDto> Products { get; set; } = null!;

        /// <summary>
        /// The ids of the products in the package.
        /// </summary>
        public string? ProductIds { get; set; }

        /// <summary>
        /// The quantities of the products in the package.
        /// </summary>
        public string? ProductQuantities { get; set; }

        /// <summary>
        /// The total number of all the products in the package.
        /// A product might have more than one quantity being purchased at once.
        /// Important to know when calculating whether or not the package should be accepted
        /// in the database as the sale is being processed in the controller.
        /// </summary>
        public int TotalProductQuantity { get; set; }

        /// <summary>
        /// The total value of all the products in the package.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal TotalValue { get; set; }

        /// <summary>
        /// The total value of all the products in the package with the discount applied.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal TotalValueWithDiscount { get; set; }

        /// <summary>
        /// The date the package was created.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The id of the user that purchased the package.
        /// Foreign key of the User model.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// The points that were used to purchase the package - Will be 0 if no discount was applied.
        /// </summary>
        public int PointsUsed { get; set; }

        /// <summary>
        /// Payment method used to purchase the package.
        /// </summary>
        public string PaymentMethod { get; set; } = null!;
    }
}
