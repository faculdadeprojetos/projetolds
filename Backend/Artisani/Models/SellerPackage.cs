﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Artisani.Models
{
    public class SellerPackage
    {
        /// <summary>
        /// Id of this package that contains only products from one seller.
        /// </summary>
        public int Id { get; set; }

        public string OriginalPackageProductsFromThisSeller { get; set; } = null!;

        /// <summary>
        /// The quantities of the products in the package.
        /// </summary>
        public string OriginalPackageProductQuantitiesFromThisSeller { get; set; } = null!;

        /// <summary>
        /// The total amount of products in the package.
        /// </summary>
        public int TotalProductQuantity { get; set; }

        /// <summary>
        /// The total value of all the products in the package.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal TotalValue { get; set; }

        /// <summary>
        /// The total value of all the products in the package after applying the discount.
        /// </summary>
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal TotalValueWithDiscount { get; set; }

        /// <summary>
        /// The date the package was created.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The id of the user that purchased the package.
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// The id of the original package that contains products from multiple sellers.
        /// The products in this package are a subset of the products in the original package
        /// where the SellerId of the product is equal to the Id of the seller that is looking at this seller package.
        /// </summary>
        public int OriginalPackageId { get; set; }

        /// <summary>
        /// The id of the seller that is looking at this seller package.
        /// </summary>
        public int SellerId { get; set; }

        public string State { get; set; } = null!;
    }
}
