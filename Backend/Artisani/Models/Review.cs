﻿namespace Artisani.Models
{
    public class Review
    {
        /// <summary>
        /// Unique identifier for the review.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of the client that made the review.
        /// Important since the client will be displayed in the review section of the product page.
        /// </summary>
        public int? ClientId { get; set; }

        /// <summary>
        /// Id of the product that was reviewed.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// The comment the client made about the product.
        /// </summary>
        public string Comment { get; set; } = null!;

        /// <summary>
        /// The rating the client gave the product - 1 to 5, accepts intervals of 0.5.
        /// </summary>
        public float Rating { get; set; }
    }
}
