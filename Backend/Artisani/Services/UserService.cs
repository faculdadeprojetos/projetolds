﻿using Artisani.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Artisani.Services
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly TimeSpan _tokenLifeTime = TimeSpan.FromMinutes(30);
        private readonly ILogger<UserService> _logger;

        public UserService(IHttpContextAccessor httpContextAccessor, IConfiguration configuration, ILogger<UserService> logger)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _logger = logger;
        }

        /// <summary>
        /// Function that generates a jwt token for the user that is trying to login.
        /// </summary>
        /// <param name="identityId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public string? GenerateJwtToken(int identityId, string email)
        {
            _logger.LogInformation($"Generating jwt token for user with id {identityId} and email {email}");
            string keyString = _configuration["JwtSettings:Key"]!;

            if (keyString != null)
            {
                List<Claim> claims = new()
                {
                    new (JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new (JwtRegisteredClaimNames.Sub, email),
                    new (JwtRegisteredClaimNames.Email, email),
                    new ("userid", identityId.ToString()),
                    new (ClaimTypes.Name, identityId.ToString())
                };

                var tokenKey = Encoding.UTF8.GetBytes(keyString);

                var credentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.Add(_tokenLifeTime),
                    SigningCredentials = credentials,
                    Issuer = _configuration["JwtSettings:Issuer"],
                    Audience = _configuration["JwtSettings:Audience"]
                };

                var tokenHandler = new JwtSecurityTokenHandler();

                var token = tokenHandler.CreateToken(tokenDescriptor);

                var jwt = tokenHandler.WriteToken(token);

                return jwt;
            }

            return null;
        }

        /// <summary>
        /// Function that returns the user id of the user that is currently logged in.
        /// </summary>
        /// <returns></returns>
        public int GetIdentityId()
        {
            var userIdString = string.Empty;

            if (_httpContextAccessor.HttpContext is not null)
            {
                userIdString = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
                _logger.LogInformation($"User id is {userIdString}");

                if (userIdString == null)
                {
                    return -1;
                }
            }

            return int.Parse(userIdString);

        }
    }
}
