﻿namespace Artisani.Services
{
    public interface IUserService
    {
        int GetIdentityId();

        string? GenerateJwtToken(int identityId, string email);
    }
}
