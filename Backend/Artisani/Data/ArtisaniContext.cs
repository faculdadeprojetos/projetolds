﻿using System.Collections;
using Artisani.Models;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;

namespace Artisani.Data
{
    public class ArtisaniContext : DbContext
    {
        public ArtisaniContext(DbContextOptions<ArtisaniContext> options)
            : base(options) { }

        public DbSet<User> Users { get; set; }

        public DbSet<Package> Packages { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public DbSet<Seller> Sellers { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<SellerPackage> SellerPackages { get; set; }

        public DbSet<MonetizationPolicy> MonetizationPolicy { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // The review model has a foreign key to the client model and to the product model.
            modelBuilder.Entity<Review>().HasOne<User>().WithMany().HasForeignKey(r => r.ClientId);

            modelBuilder
                .Entity<Review>()
                .HasOne<Product>()
                .WithMany()
                .HasForeignKey(r => r.ProductId);

            // The package model has a foreign key to the client model.
            modelBuilder.Entity<Package>().HasOne<User>().WithMany().HasForeignKey(p => p.UserId);

            // The seller model has a an array of pending purchases. Each pending purchase has a key to the seller package model.
            modelBuilder
                .Entity<SellerPackage>()
                .HasOne<Seller>()
                .WithMany()
                .HasForeignKey(sp => sp.SellerId);

            // The seller package model is made out of a full package. The seller package model has a foreign key to the package model.
            modelBuilder
                .Entity<SellerPackage>()
                .HasOne<Package>()
                .WithMany()
                .HasForeignKey(sp => sp.OriginalPackageId);

            // The product model has a foreign key to the seller model.
            modelBuilder
                .Entity<Product>()
                .HasOne<Seller>()
                .WithMany()
                .HasForeignKey(p => p.SellerId);

            // Force all decimal values to be rounded to 2 decimal places and be stored as strings in the database.
            // When reading from the database, the string will be converted to a decimal with 2 decimal places.
            modelBuilder
                .Entity<Product>()
                .Property(p => p.IndividualPrice)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            modelBuilder
                .Entity<MonetizationPolicy>()
                .Property(mp => mp.DollarToDiscoutPer10Points)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));
            modelBuilder
                .Entity<SellerPackage>()
                .Property(sp => sp.TotalValue)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            modelBuilder
                .Entity<SellerPackage>()
                .Property(sp => sp.TotalValueWithDiscount)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            modelBuilder
                .Entity<Package>()
                .Property(p => p.TotalValue)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            modelBuilder
                .Entity<Package>()
                .Property(p => p.TotalValueWithDiscount)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            modelBuilder
                .Entity<Seller>()
                .Property(s => s.TotalValueEarnedFromSales)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            modelBuilder
                .Entity<User>()
                .Property(u => u.TotalPurchasesValue)
                .HasConversion(v => v.ToString(), v => Math.Round(decimal.Parse(v), 2));

            // Force entityframework to not add the list of products in the package entity to the database
            // since we're going to add instead a list of product ids.
            modelBuilder.Entity<Package>().Ignore(p => p.Products);
        }
    }
}
