# Hermes Study Group

## Starting

To run the server-side application, try running
`dotnet run Backend\Artisani\Program.cs`

To run the webpage of the online store, try running
`cd Frontend\onlineArtisanMarketplace`
`npm install`
`npm start`

To run the mobile application, simply click on it's icon and the app will open